
if [ "$1" = "" ]; then
    ls -l services
    ls -l cluster.yaml
    exit 0
fi

unlink services || true
unlink cluster.yaml || true
ln -s services-$1 services
ln -s cluster-$1.yaml cluster.yaml
