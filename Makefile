.SHELLFLAGS := -euxc

lint:
	docker run \
		--cap-drop ALL \
		--rm \
		--security-opt no-new-privileges \
		--volume "$${PWD:?}:$${PWD:?}:ro" \
		--workdir "$${PWD:?}" \
		docker.io/golangci/golangci-lint:v1.64.7-alpine@sha256:6d0bd5c5f6918a481a1cb7d6fddd959657631287b9654e8a02e41290a7931b14 \
		golangci-lint run --timeout=10m ./...
