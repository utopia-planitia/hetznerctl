.SHELLFLAGS := -euxc

lint:
	docker run \
		--cap-drop ALL \
		--rm \
		--security-opt no-new-privileges \
		--volume "$${PWD:?}:$${PWD:?}:ro" \
		--workdir "$${PWD:?}" \
		docker.io/golangci/golangci-lint:v1.64.6-alpine@sha256:65e406735929c1c52f71e5b5524644627fb03538fc32c2119e193512ad78adbb \
		golangci-lint run --timeout=10m ./...
