#!/usr/bin/env bats

@test "hairpin" {
  run kubectl apply -f hairpin.yaml
  [ $status -eq 0 ]
  sleep 1

  run timeout 120 sh -c "until kubectl wait pods -l app=test-hairpin --for=condition=ready; do sleep 1; done"
  [ $status -eq 0 ]

  run timeout 120 sh -c "until kubectl exec deployment/test-hairpin -- curl --fail --max-time 5 http://test-hairpin/; do sleep 1; done"
  [ $status -eq 0 ]
}

teardown () {
  kubectl delete -f hairpin.yaml --ignore-not-found=true --wait=true

# from "load test_helper"
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}
