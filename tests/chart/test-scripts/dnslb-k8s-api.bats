#!/usr/bin/env bats

setup () {
  kubectl get no --no-headers | awk '{ print $1 }' | xargs kubectl uncordon
  kubectl -n kube-system patch cronjobs apiserver-dns-cleanup -p '{"spec" : {"suspend" : true }}'

  until [ `kubectl -n kube-system get po -l app=loadbalancer-apiserver --no-headers | wc -l` = "3" ]; do sleep 5; done

  export CF_API_KEY={{ .Values.cloudflare.key }}
  export CF_API_EMAIL={{ .Values.cloudflare.email }}
}

teardown () {
  kubectl -n kube-system patch cronjobs apiserver-dns-cleanup -p '{"spec" : {"suspend" : false }}'

  kubectl uncordon ${NODE}
  kubectl -n kube-system delete --ignore-not-found=true job/cleanup-apiserver-dns-now

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "normal life cycle" {
  # ip registered at cloudflare when pod started
  NODE=`kubectl -n kube-system get po -o wide -l app=loadbalancer-apiserver --no-headers | awk '{print $7}' | head -n 1`
  POD=`kubectl -n kube-system get po -l app=loadbalancer-apiserver -o wide | grep ${NODE} | awk '{print $1}'`
  run kubectl -n kube-system wait --timeout=120s --for=condition=Ready pod/${POD}
  [ $status -eq 0 ]

  NODE_IP=`kubectl -n kube-system exec ${POD} -c dnslb /dnslb ipv4`
  sleep 10
  run sh -c "flarectl dns list --zone {{ .Values.cloudflare.zone }} --name {{ .Values.cloudflare.api_subdomain }}.{{ .Values.cloudflare.zone }} | grep '${NODE_IP}'"
  [ $status -eq 0 ]

  # ip gets unregistered during pod deletion
  kubectl cordon ${NODE}
  kubectl -n kube-system delete pod/${POD}
  sleep 10
  run sh -c "flarectl dns list --zone {{ .Values.cloudflare.zone }} --name {{ .Values.cloudflare.api_subdomain }}.{{ .Values.cloudflare.zone }} | grep '${NODE_IP}'"
  [ $status -eq 1 ]
}
