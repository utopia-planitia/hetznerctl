#!/usr/bin/env bats

cleanup() {
  kubectl delete job/local-path-test-read job/local-path-test-write --ignore-not-found=true
  kubectl delete po -l job-name=local-path-test-read
  kubectl delete po -l job-name=local-path-test-write
  kubectl delete -f local-path-volume.yml --ignore-not-found=true
  kubectl delete -f local-path-volume-read.yml --ignore-not-found=true
  kubectl delete -f local-path-volume-write.yml --ignore-not-found=true
  kubectl get pv | grep Released | grep local-path | awk '{ print $1 }' | xargs -r -n 1 kubectl delete --ignore-not-found=true pv
}

setup() {
  cleanup
}

teardown() {
  cleanup

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "write to and read from persistent local volume" {
  run kubectl apply -f local-path-volume.yml
  [ $status -eq 0 ]

  run kubectl apply -f local-path-volume-write.yml
  [ $status -eq 0 ]
  sleep 1

  run kubectl wait --for=condition=complete --timeout=60s job/local-path-test-write
  [ $status -eq 0 ]

  run kubectl apply -f local-path-volume-read.yml
  [ $status -eq 0 ]
  sleep 1

  run kubectl wait --for=condition=complete --timeout=60s job/local-path-test-read
  [ $status -eq 0 ]
}
