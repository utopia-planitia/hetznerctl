#!/usr/bin/env bats

set +x

teardown () {
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

{{ range .Values.nodes }}
{{ if ne .role `worker` }}

@test "verify access to kubernetes API {{ .name }}" {
  run kubectl config set-cluster testing --server=https://{{ .network.public_ip }}:6443/ --certificate-authority=/run/secrets/kubernetes.io/serviceaccount/ca.crt
  [ $status -eq 0 ]
  run kubectl config set-context testing --cluster=testing
  [ $status -eq 0 ]
  run kubectl config set-credentials testing --token=$(cat /run/secrets/kubernetes.io/serviceaccount/token)
  [ $status -eq 0 ]
  run kubectl config set-context testing --user=testing
  [ $status -eq 0 ]
  run kubectl config use-context testing
  [ $status -eq 0 ]

  run kubectl version
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 2 ]
  [[ "${lines[0]}" =~ ^Client\ Version:\ .* ]]
  [[ "${lines[1]}" =~ ^Server\ Version:\ .* ]]
}

{{ end }}
{{ end }}
