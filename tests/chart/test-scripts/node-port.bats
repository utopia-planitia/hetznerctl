#!/usr/bin/env bats

@test "use node port" {
  run kubectl apply -f node-port.yaml
  [ $status -eq 0 ]
  sleep 1

  run timeout 120 sh -c "until kubectl wait pods -l app=test-node-port --for=condition=ready; do sleep 1; done"
  [ $status -eq 0 ]

  PORT=`kubectl get svc test-node-port -o go-template='{{`{{(index .spec.ports 0).nodePort}}`}}'`

  echo $PORT

  {{ range .Values.nodes }}
  run curl -v \
    --connect-timeout 5 \
    --max-time 5 \
    --retry 12 \
    --retry-delay 0 \
    --retry-max-time 60 --retry-connrefused \
    http://{{ .network.public_ip }}:${PORT}/
  [ $status -eq 0 ]
  {{ end }}
}

teardown () {
  kubectl delete -f node-port.yaml --ignore-not-found=true

# from "load test_helper"
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}
