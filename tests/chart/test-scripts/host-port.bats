#!/usr/bin/env bats

cleanup () {
  kubectl delete -f host-port.yaml --ignore-not-found=true
}

setup () {
  cleanup
}

teardown () {
  cleanup

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "use host port" {
  skip "disabled until https://github.com/cilium/cilium/issues/14287 is fixed"

  kubectl apply -f host-port.yaml
  sleep 1
  run kubectl wait pods --timeout=180s -l app=host-port --for=condition=ready
  [ $status -eq 0 ]

  PORT=32123
  {{ range $index, $node := .Values.nodes }}
  run curl -v --connect-timeout 5 --max-time 5 --retry 12 --retry-delay 0 --retry-max-time 60 --retry-connrefused http://{{ $node.network.private_ip }}:${PORT}/
  [ $status -eq 0 ]
  {{ end }}
}
