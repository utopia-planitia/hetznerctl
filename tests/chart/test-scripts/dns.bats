#!/usr/bin/env bats

@test "check google-public-dns-a-google-com ip" {
  nslookup google-public-dns-a.google.com | grep "Address: 8.8.8.8"
}

@test "check dns service ip" {
  nslookup google.de | grep "Address:	10.96.0.10#53"
}

@test "check kubernetes-default-svc-cluster-local ip" {
  nslookup kubernetes.default.svc.cluster.local | grep "Address: 10.96.0.1"
}

@test "check kubernetes-default-svc ip" {
  nslookup kubernetes.default.svc | grep "Address: 10.96.0.1"
}

@test "check kubernetes-default ip" {
  nslookup kubernetes.default | grep "Address: 10.96.0.1"
}

teardown () {
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}
