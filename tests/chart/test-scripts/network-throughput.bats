#!/usr/bin/env bats

teardown () {
  kubectl delete -f network-throughput.yaml --ignore-not-found=true

# from "load test_helper"
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "check transfer time" {
  kubectl apply -f network-throughput.yaml
  run kubectl wait --for=condition=ready --timeout=120s pod/network-throughput-transmit
  [ $status -eq 0 ]
  run kubectl wait --for=condition=ready --timeout=120s pod/network-throughput-receive
  [ $status -eq 0 ]

  run kubectl exec network-throughput-transmit -- apk --update add curl
  [ $status -eq 0 ]

#  kubectl exec transmit -- dd if=/dev/urandom of=/file bs=1M count=100
  run kubectl exec network-throughput-transmit -- timeout -t 60 sh -c "until curl --fail --max-time 5 --connect-timeout 5 -o file http://speed.hetzner.de/100MB.bin; do sleep 1; done"
  [ $status -eq 0 ]

  run kubectl exec network-throughput-transmit -- timeout -t 60 sh -c "until curl --fail --max-time 1 --connect-timeout 1 http://network-throughput-receive/; do sleep 1; done"
  [ $status -eq 0 ]

  positive=0
  for i in {1..10}; do
    echo "run #${i}"
    # timeout -t 2 ensures at least 66,5MB/s
    if kubectl exec network-throughput-transmit -- timeout -t 2 curl -T file --fail http://network-throughput-receive/file; then
      let "positive=positive+1"
    fi
  done

  [ $positive -ge 9 ] # 9 or 10 out of 10
}
