#!/bin/bash
set -euo pipefail

sleep 2

curl --silent --fail http://artscene.textfiles.com/asciiart/unicorn | grep 'O   O'
