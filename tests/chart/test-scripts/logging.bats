#!/usr/bin/env bats

cleanup () {
  kubectl delete -f logging.yaml --ignore-not-found=true
  kubectl delete po -l job-name=hello-world
}

setup() {
  cleanup
}

teardown () {
  cleanup

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "check logging hello, world" {
  kubectl apply -f logging.yaml
  sleep 1

  run kubectl wait --for=condition=complete --timeout=60s job/logging
  [ $status -eq 0 ]

  run kubectl logs `kubectl get pod --selector=job-name=logging --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 1 ]
  [ "${lines[0]}" = "Hello, World." ]
}
