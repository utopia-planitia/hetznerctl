#!/usr/bin/env bats

cleanup() {
  kubectl delete -f dns-fallback-dig-google.com.yml --ignore-not-found=true
  kubectl delete po -l job-name=dig-google-com

  kubectl scale deployment coredns --replicas=2
  kubectl patch daemonset node-local-dns --type json -p='[{"op": "remove", "path": "/spec/template/spec/nodeSelector/non-existing"}]' || true
}

setup () {
  cleanup
}

teardown () {
  cleanup

  kubectl wait --for=condition=available deployment/coredns
  sleep 3

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "check google.com dns" {
  kubectl apply -f dns-fallback-dig-google.com.yml
  run kubectl wait --for=condition=complete --timeout=60s job/dig-google-com
  [ $status -eq 0 ]
  run kubectl logs `kubectl get pod -l=job-name=dig-google-com --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
}

@test "check google.com dns without coredns" {
  kubectl scale deployment coredns --replicas=0
  kubectl delete po -l k8s-app=kube-dns

  sleep 2

  kubectl apply -f dns-fallback-dig-google.com.yml
  run kubectl wait --for=condition=complete --timeout=60s job/dig-google-com
  [ $status -eq 0 ]
  run kubectl logs `kubectl get pod -l=job-name=dig-google-com --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
}

@test "check google.com dns without node local dns cache" {
  kubectl patch daemonset node-local-dns -p '{"spec": {"template": {"spec": {"nodeSelector": {"non-existing": "true"}}}}}'
  kubectl delete po -l k8s-app=node-local-dns

  sleep 2

  kubectl apply -f dns-fallback-dig-google.com.yml
  run kubectl wait --for=condition=complete --timeout=60s job/dig-google-com
  [ $status -eq 0 ]
  run kubectl logs `kubectl get pod -l=job-name=dig-google-com --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
}


@test "check google.com dns fails without any dns" {
  kubectl patch daemonset node-local-dns -p '{"spec": {"template": {"spec": {"nodeSelector": {"non-existing": "true"}}}}}'
  kubectl scale deployment coredns --replicas=0
  kubectl delete po -l k8s-app=node-local-dns
  kubectl delete po -l k8s-app=kube-dns

  sleep 2

  kubectl apply -f dns-fallback-dig-google.com.yml
  run kubectl wait --for=condition=failed --timeout=240s job/dig-google-com
  [ $status -eq 0 ]
  run kubectl logs `kubectl get pod -l=job-name=dig-google-com --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
}
