#!/usr/bin/env bats

cleanup() {
  kubectl delete -f anonymous-request-pod.yml -f anonymous-request-host.yml --ignore-not-found=true
  kubectl delete po -l job-name=anonymous-request-host
  kubectl delete po -l job-name=anonymous-request-pod
}

setup () {
  cleanup
}

teardown () {
  cleanup

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "check anonymous request to apiserver (pod network)" {
  kubectl apply -f anonymous-request-pod.yml
  run kubectl wait --for=condition=complete --timeout=60s job/anonymous-request-pod
  [ $status -eq 0 ]

  run diff anonymous-request.golden <(kubectl logs `kubectl get pod --selector=job-name=anonymous-request-pod --output=jsonpath={.items..metadata.name}`)
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 0 ]
}

@test "check anonymous request to apiserver (host network)" {
  kubectl apply -f anonymous-request-host.yml
  run kubectl wait --for=condition=complete --timeout=60s job/anonymous-request-host
  [ $status -eq 0 ]

  run diff anonymous-request.golden <(kubectl logs `kubectl get pod --selector=job-name=anonymous-request-host --output=jsonpath={.items..metadata.name}`)
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 0 ]
}
