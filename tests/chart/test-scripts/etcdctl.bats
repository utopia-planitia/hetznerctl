#!/usr/bin/env bats

teardown () {
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

{{ range .Values.nodes }}
{{ if ne .role `worker` }}

@test "verify etcdctl {{ .name }}" {
  run kubectl -n kube-system exec -c etcd etcd-{{ .name }} -- etcdctl member list
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 3 ]
}

{{ end }}
{{ end }}
