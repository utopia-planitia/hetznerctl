releases:
  - name: hetznerctl-tests
    namespace: kube-system
    chart: ./chart
    labels:
      phase: only-testing
    values:
      - nodes: {{ toJson .Values.nodes }}
        cloudflare:
          key: "{{ .Values.cloudflare.key }}"
          email: "{{ .Values.cloudflare.email }}"
          zone: "{{ .Values.cloudflare.zone }}"
          api_subdomain: "{{ .Values.cloudflare.subdomains.api }}"
