# Remove a node

1. Rebalance Ceph

   1. Set OSD out

      ``` bash
      kubectl -n rook-ceph exec -ti deployment/rook-ceph-tools -- ceph status
      kubectl -n rook-ceph exec -ti deployment/rook-ceph-tools -- ceph osd tree
      kubectl -n rook-ceph exec -ti deployment/rook-ceph-tools -- ceph osd out osd.[OSD_ID]
      ```

   2. To watch the progress

      ``` bash
      kubectl -n rook-ceph exec -ti deployment/rook-ceph-tools -- watch ceph status
      ```

2. Drain the node

   ``` bash
   kubectl drain <node-name>
   ```

3. Shut down the node
   
   1. Run 
      ```
      hetznerctl --limit [NODE_NAME] rescuemode enable
      hetznerctl --limit [NODE_NAME] reboot hw
      ```
      to boot node into rescue mode.

      After this no data is deleted but the node appears gone.

   2. Check alerting and monitoring to verify all services are still running.

4. Replace Cassandra Node

   1. Mark node to be replaced

      ```
      kubectl -n cassandra edit cassandradatacenters.cassandra.datastax.com dc1
      ```

      Add Pod name to `spec.replaceNodes` as an list item.

   2. Deleted old Pod, Volumeclaim, and Volume

      ```
      kubectl -n cassandra delete pvc/[VOLUME_CLAIM_NAME] pod/[POD_NAME] pv/[VOLUME_NAME]
      ```

   3. Watch replacement happening

      ```
      CASSANDRA_SUPER_USERNAME=$( kubectl -n cassandra get secret k8ssandra-superuser -o jsonpath="{.data.username}" | base64 --decode )
      CASSANDRA_SUPER_PASSWORD=$( kubectl -n cassandra get secret k8ssandra-superuser -o jsonpath="{.data.password}" | base64 --decode )
      
      kubectl --namespace cassandra exec statefulset/k8ssandra-dc1-default-sts --container cassandra -- nodetool -u ${CASSANDRA_SUPER_USERNAME} -pw ${CASSANDRA_SUPER_PASSWORD} --host ::FFFF:127.0.0.1 status

      kubectl --namespace cassandra exec statefulset/k8ssandra-dc1-default-sts --container cassandra -- nodetool -u ${CASSANDRA_SUPER_USERNAME} -pw ${CASSANDRA_SUPER_PASSWORD} --host ::FFFF:127.0.0.1 netstats
      ```

5. Remove OSD

   ```
   kubectl -n rook-ceph exec -ti deployment/rook-ceph-tools -- ceph osd purge osd.[OSD_ID] --yes-i-really-mean-it
   kubectl -n rook-ceph delete deployment rook-ceph-osd-[OSD_ID]
   kubectl -n rook-ceph delete job rook-ceph-osd-prepare-[OSD_NODE_NAME]
   ```

6. Remove etcd member

   When removing a master node, the node needs to be removed from the etcd cluster.
   ``` bash
   kubectl -n kube-system exec -ti etcd-[RUNNING_NODE] -- etcdctl member list
   kubectl -n kube-system exec -ti etcd-[RUNNING_NODE] -- etcdctl member remove [ETCD_ID]
   ```

7. Delete the node from kubernetes

   1. remove instance from kubernetes state

      ``` bash
      kubectl delete node <node-name>
      ```

   2. remove node from `cluster.yaml`
      
      Make sure to define at least one of the control plane nodes as primary. Hetznerctl uses this node to run kubectl.

8. Remove node from vswitch
