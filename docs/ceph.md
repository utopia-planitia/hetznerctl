# Ceph

enter rook ceph toool container

``` bash
kubectl -n rook-ceph exec -ti  deployment/rook-ceph-tools -- bash
```

## Clean up crash reports

Ceph collects information about crashed processes.

This shows up in alerting until acknowledged.

To list and show the reports run

``` bash
ceph crash ls
ceph crash info <id>
```

To archive a single report or all of them run

``` bash
ceph crash archive <id>
ceph crash archive-all
```

## Clean up wrong flags

From https://swamireddy.wordpress.com/2019/12/09/ceph-setting-flags-per-osd/

List the flags with

```
ceph health detail
```

Remove flags with

```
ceph osd rm-<flag>  <osd_id|host_name>
```

## Speed up osd repair

From https://www.suse.com/support/kb/doc/?id=000019693 and https://www.thomas-krenn.com/en/wiki/Ceph_-_increase_maximum_recovery_%26_backfilling_speed.

### Increase parallel backfill jobs

``` bash
ceph tell 'osd.*' injectargs --osd-max-backfills=3 --osd-recovery-max-active=9
```

### Undo

``` bash
ceph tell 'osd.*' injectargs --osd-max-backfills=1 --osd-recovery-max-active=0
```
