# Documentation

How to [create](cluster-setup.md) a Cluster.

How to [add](add-node.md) a node.

How to [remove](remove-node.md) a node.

## Tips
 - [Ceph](ceph.md)
 - [Flatcar](flatcar.md)
