# Add a node

## Reusing a node

### Uncordon

If re-adding a re-setup node with the same name, make sure to run
```
kubectl uncordon <node-name>
```

Reusing the same node name keeps the uncordened state and will reschedule all work onto the returned node.

## Adding a worker node

1. add node config to cluster.yaml
   - IPv4 / IPv6 / Gateway / MAC

2. change admin password in hetzner robot to ensure only one cluster claims node at a time

3. configure vswitch

4. apply the change

   ``` bash
   hetznerctl --limit=<node-name> os all
   hetznerctl --limit=<node-name> k8s workers
   ```

## Adding a control plane node

1. add node config to cluster.yaml

2. apply the change

   ``` bash
   hetznerctl --limit=<node-name> os all
   hetznerctl --limit=<node-name> k8s masters
   ```
