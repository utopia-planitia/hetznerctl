# Flatcar

[Collecting crash logs on Flatcar Container Linux
](https://www.flatcar.org/docs/latest/setup/debug/collecting-crash-logs/)

# Containerd

Reset (delete) containerd local state

``` bash
systemctl stop kubelet
crictl pods -q | xargs -L 1 crictl stopp
crictl pods -q | xargs -L 1 crictl rmp
systemctl stop containerd
rm -rf /var/lib/containerd/*
systemctl start containerd
systemctl start kubelet
```
