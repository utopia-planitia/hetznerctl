# Cluster setup

1. Create a default cluster.yaml

   ``` bash
   hetznerctl cfg tpl > cluster.yaml
   ```

2. Configure the nodes in cluster.yaml
   - IPv4 / IPv6 / Gateway / MAC
   - By default a minimum of 3 control plane nodes is expected

3. create and configure vswitch

4. Configure access to Cloudflare \
   Cloudflare, in conjunction with ingress sidecar containers, is used to run a DNS load balancer.

5. Add services
   1. Add repo to services/\<service name>\
      Services are deployed via helmfile.\
      Each service contains one ore many subfolders, each with one helmfile.
   2. Configure service in cluster.yaml\
      The cluser configuration (cluster.yaml) is passed to each call of helmfile via --state-values-file.\
      The helmfile.yaml maps the configuration to the helm chart values.

6. Install the operating system

   ``` bash
   hetznerctl os all
   ```

7. Install Kubernetes

   ``` bash
   hetznerctl k8s all
   ```

8. Install the services

   ``` bash
   hetznerctl svc all
   ```

9. Wait for all pods to start \
   This executes `status.sh` on the primary master.

   ``` bash
   watch hetznerctl status k8s
   ```

10. Run tests (optional)\
   Tests can be executed to verify a setup works as expected.

   ``` bash
   hetznerctl tests all
   ```
