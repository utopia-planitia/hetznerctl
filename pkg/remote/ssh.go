package remote

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"al.essio.dev/pkg/shellescape"
	"github.com/hnakamur/go-scp"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
)

// Remote connects to the server via ssh.
type Remote struct {
	sshClient *ssh.Client
	addr      string
	cfg       *ssh.ClientConfig
	Sudo      bool
	Debug     bool
	Name      string
}

// Log prints messages prefixed with the servers name.
func (r Remote) Log(f string, a ...interface{}) {
	log.Printf("%s: %s", r.Name, fmt.Sprintf(f, a...))
}

// NewRemoteUsernamePassword creates a new Remove based on a username password login.
func NewRemoteUsernamePassword(name, addr, username, password string) (Remote, error) {
	return newRemote(name, addr, username, ssh.Password(password))
}

// NewRemoteAuthSocket creates a new Remove based using SSH_AUTH_SOCK to authorize.
func NewRemoteAuthSocket(name, addr, username string) (Remote, error) {
	sshAgent, err := net.Dial("unix", os.Getenv("SSH_AUTH_SOCK"))
	if err != nil {
		return Remote{}, fmt.Errorf("configure ssh client with SSH_AUTH_SOCK: %v", err)
	}

	auth := ssh.PublicKeysCallback(agent.NewClient(sshAgent).Signers)

	return newRemote(name, addr, username, auth)
}

func newRemote(name, addr, username string, auth ssh.AuthMethod) (Remote, error) {
	cfg := &ssh.ClientConfig{
		// use known_hosts instead https://skarlso.github.io/2019/02/17/go-ssh-with-host-key-verification/
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		User:            username,
		Auth:            []ssh.AuthMethod{auth},
		Timeout:         10 * time.Second,
	}

	client, err := ssh.Dial("tcp", addr, cfg)
	if err != nil {
		return Remote{}, fmt.Errorf("dialing to ssh: %s", err)
	}

	remote := Remote{
		sshClient: client,
		addr:      addr,
		cfg:       cfg,
		Sudo:      username != "root",
		Name:      name,
	}

	return remote, nil
}

// Close disconnects the ssh connection.
func (r Remote) Close() {
	err := r.sshClient.Close()
	if err != nil {
		r.Log("closing ssh connection: %v", err)
	}
}

// Run executes a command on the remote server and returns true if the file changed.
func (r Remote) RunChanges(cmd, file string) (bool, error) {
	originalMd5, err := r.Md5(file)
	if err != nil {
		return false, err
	}

	err = r.Run(cmd)
	if err != nil {
		return false, err
	}

	newMd5, err := r.Md5(file)
	if err != nil {
		return false, err
	}

	changed := originalMd5 != newMd5

	return changed, nil
}

// Run executes a command on the remote server and returns the output.
func (r Remote) Run(cmd string) error {
	stdIn := &bytes.Buffer{}
	stdOut := &bytes.Buffer{}
	stdErr := &bytes.Buffer{}

	stdIn.WriteString(cmd)

	shell := r.sudoPrefix(r.terminal())

	err := r.Execute(shell, stdIn, stdOut, stdErr)
	if containsDeprecation(stdErr.String(), stdOut.String()) {
		log.Printf("deprecation found:\nstderr: %s\nstdout: %s", stdErr.String(), stdOut.String())
	}
	if err != nil {
		return fmt.Errorf("execution failed: cmd \"%s\": %s: stderr: %s: stdout: %s", cmd, err, stdErr.String(), stdOut.String())
	}

	return nil
}

func containsDeprecation(stdErr, stdOut string) bool {
	lowerCaseStdErr := strings.ToLower(stdErr)
	lowerCaseStdOut := strings.ToLower(stdOut)
	// deprecat is used a shared prefix of deprecated and deprecation
	return strings.Contains(lowerCaseStdErr, "deprecat") && strings.Contains(lowerCaseStdOut, "deprecat")
}

// Run executes a command on the remote server and returns the output.
func (r Remote) RunFetchStdout(cmd string) (string, error) {
	stdIn := &bytes.Buffer{}
	stdOut := &bytes.Buffer{}
	stdErr := &bytes.Buffer{}

	stdIn.WriteString(cmd)

	shell := r.sudoPrefix(r.terminal())

	err := r.Execute(shell, stdIn, stdOut, stdErr)
	if err != nil {
		return stdOut.String(), fmt.Errorf("execution failed: cmd \"%s\": %s: %s", cmd, err, stdErr.String())
	}

	return stdOut.String(), nil
}

// RunStdout executes a command on the remote server and pritns the output to stderr & stdout.
func (r Remote) RunStdout(cmd string) error {
	stdIn := &bytes.Buffer{}
	stdOut := os.Stdout
	stdErr := os.Stderr

	stdIn.WriteString(cmd)

	shell := r.sudoPrefix(r.terminal())

	err := r.Execute(shell, stdIn, stdOut, stdErr)
	if err != nil {
		return fmt.Errorf("execution failed, cmd %s: %s", cmd, err)
	}

	return nil
}

// Execute runs a command on the remote server.
func (r Remote) Execute(cmd string, stdIn io.Reader, stdOut, stdErr io.Writer) error {
	retries := 0

OPEN_SESSION:
	session, err := r.sshClient.NewSession()

	if err != nil && strings.HasSuffix(err.Error(), "use of closed network connection") {
		log.Printf("failed to open ssh session: %v", err)

	RECONNECT:
		retries++

		if retries >= 20 {
			return fmt.Errorf("all retries failed to open ssh session: %s", err)
		}

		log.Printf("wait 10 seconds before retry")
		time.Sleep(10 * time.Second)

		r.sshClient.Conn.Close()

		r.sshClient.Conn, err = ssh.Dial("tcp", r.addr, r.cfg)
		if err != nil {
			log.Printf("failed to open ssh connection: %v", err)
			goto RECONNECT
		}

		goto OPEN_SESSION
	}

	if err != nil {
		return fmt.Errorf("create ssh session: %s", err)
	}
	defer session.Close()

	session.Stdin = stdIn
	session.Stdout = stdOut
	session.Stderr = stdErr

	err = session.Run(r.sudoPrefix(cmd))
	if err != nil {
		return err
	}

	return nil
}

// ExecuteStdin runs a command on the remote server.
func (r Remote) ExecuteStdin(stdIn io.Reader, stdOut, stdErr io.Writer) error {
	shell := r.sudoPrefix(r.terminal())
	return r.Execute(shell, stdIn, stdOut, stdErr)
}

func (r Remote) sudoPrefix(cmd string) string {
	if !r.Sudo {
		return cmd
	}

	return fmt.Sprintf("sudo -u root -i %s", cmd)
}

func (r Remote) terminal() string {
	printCommands := ""
	if r.Debug {
		printCommands = "x"
	}

	return fmt.Sprintf("bash -eu%so pipefail", printCommands)
}

// ReceiveFile download a file.
func (r Remote) ReceiveFile(path string, w io.Writer) error {
	_, err := r.scp().Receive(path, w)
	if err != nil {
		return fmt.Errorf("fail to receive file: %s", err)
	}

	return nil
}

func (r Remote) scp() *scp.SCP {
	scpClient := scp.NewSCP(r.sshClient)

	if r.Sudo {
		scpClient.SCPCommand = "sudo scp"
	}

	return scpClient
}

// WriteFile uploads a file.
func (r Remote) WriteFile(content []byte, path string, mode uint) error {
	info := scp.NewFileInfo(path, int64(len(content)), os.FileMode(mode), time.Now(), time.Now())

	err := r.scp().Send(info, io.NopCloser(bytes.NewReader(content)), path)
	if err != nil {
		return fmt.Errorf("fail to send file: %s", err)
	}

	return nil
}

// UploadFile uploads a file.
func (r Remote) UploadFile(src, dest string) error {
	err := r.MkDir(filepath.Dir(dest))
	if err != nil {
		return fmt.Errorf("ensure destination exists: %s", err)
	}

	err = r.scp().SendFile(src, dest)
	if err != nil {
		return fmt.Errorf("fail to send file: %s", err)
	}

	return nil
}

// UploadFolder uploads the content of a folder recursively.
func (r Remote) UploadFolderViaTar(src, dest string) error {
	err := r.MkDir(dest)
	if err != nil {
		return fmt.Errorf("ensure destination exists: %s", err)
	}

	fileList, err := listFiles(src)
	if err != nil {
		return fmt.Errorf("list files: %v", err)
	}

	tarOut, err := createTarball(fileList)
	if err != nil {
		return fmt.Errorf("create tarball: %v", err)
	}

	extractCmd := fmt.Sprintf("tar -xzf - --directory=%s --strip-components=2", dest)

	err = r.Execute(extractCmd, tarOut, os.Stdout, os.Stderr)
	if err != nil {
		return fmt.Errorf("fail to send folder: %v", err)
	}

	return nil
}

func listFiles(src string) ([]string, error) {
	files := []string{}

	// fix to deref symlinks
	src = src + string(os.PathSeparator)

	err := filepath.Walk(src, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.Mode()&os.ModeSymlink == os.ModeSymlink {
			return nil
		}

		if info.IsDir() && info.Name() == ".git" {
			return filepath.SkipDir
		}

		if info.IsDir() {
			return nil
		}

		files = append(files, path)

		return nil
	})

	if err != nil {
		return []string{}, fmt.Errorf("couldn't list files: %v", err)
	}

	return files, nil
}

func createTarball(filePaths []string) (*bytes.Buffer, error) {
	buf := &bytes.Buffer{}

	gzipWriter := gzip.NewWriter(buf)
	defer gzipWriter.Close()

	tarWriter := tar.NewWriter(gzipWriter)
	defer tarWriter.Close()

	for _, filePath := range filePaths {
		err := addFileToTarWriter(filePath, tarWriter)
		if err != nil {
			return &bytes.Buffer{}, fmt.Errorf("could not add file '%s': %v", filePath, err)
		}
	}

	return buf, nil
}

func addFileToTarWriter(filePath string, tarWriter *tar.Writer) error {
	stat, err := os.Stat(filePath)
	if err != nil {
		return err
	}

	header := &tar.Header{
		Name:    filePath,
		Size:    stat.Size(),
		Mode:    int64(stat.Mode()),
		ModTime: stat.ModTime(),
	}

	err = tarWriter.WriteHeader(header)
	if err != nil {
		return err
	}

	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(tarWriter, file)
	if err != nil {
		return err
	}

	return nil
}

// UploadFolder uploads the content of a folder recursively.
func (r Remote) UploadFolder(src, dest string) error {
	err := r.MkDir(filepath.Dir(dest))
	if err != nil {
		return fmt.Errorf("ensure destination exists: %s", err)
	}

	err = r.scp().SendDir(src, dest, acceptAny)
	if err != nil {
		return fmt.Errorf("fail to send folder: %s", err)
	}

	return nil
}

// DownloadFolder downloads the content of a folder recursively.
func (r Remote) DownloadFolder(src, dest string) error {
	err := os.RemoveAll(dest)
	if err != nil {
		return fmt.Errorf("remove destination: %s", err)
	}

	err = r.scp().ReceiveDir(src, dest, acceptAny)
	if err != nil {
		return fmt.Errorf("fail to send folder: %s", err)
	}

	return nil
}

func acceptAny(parentDir string, info os.FileInfo) (bool, error) {
	return true, nil
}

// FilesMatch calculates and compares the hashes of two files.
func (r Remote) FilesMatch(path, otherPath string) (bool, error) {
	hash, err := r.Md5(path)
	if err != nil {
		return false, fmt.Errorf("calculate path hash: %v", err)
	}

	otherHash, err := r.Md5(otherPath)
	if err != nil {
		return false, fmt.Errorf("calculate path otherPath: %v", err)
	}

	return hash == otherHash, nil
}

func (r Remote) Md5(path string) (string, error) {
	out, err := r.RunFetchStdout(fmt.Sprintf("md5sum '%v'", path))
	if err != nil {
		return "", fmt.Errorf("md5sum %s: %v", path, err)
	}

	return out[0:32], nil
}

func (r Remote) Sha256(path string) (string, error) {
	out, err := r.RunFetchStdout(fmt.Sprintf("sha256sum '%v'", path))
	if err != nil {
		return "", fmt.Errorf("sha256sum %s: %v", path, err)
	}

	return out[0:64], nil
}

func (r Remote) PrepareGracefulReboot() error {
	exists, err := r.FileExists("/etc/systemd/system/kubelet.service")
	if err != nil {
		return fmt.Errorf("kubelet service: %v", err)
	}

	if exists {
		err := r.StopService("kubelet")
		if err != nil {
			return fmt.Errorf("shutdown kubelet: %v", err)
		}
	}

	exists, err = r.FileExists("/opt/bin/kubectl")
	if err != nil {
		return fmt.Errorf("/opt/bin/kubectl cli tool: %v", err)
	}

	if exists {
		err = r.DeletePods()
		if err != nil {
			return fmt.Errorf("delete pods: %v", err)
		}
	}

	r.Log("stopping containers")

	err = r.Run("for p in $(crictl pods -q); do crictl stopp $p; done")
	if err != nil {
		return fmt.Errorf("stopping containers: %v", err)
	}

	r.Log("removing containers")

	err = r.Run("for p in $(crictl pods -q); do crictl rmp $p; done")
	if err != nil {
		return fmt.Errorf("removing containers: %v", err)
	}

	err = r.StopService("containerd")
	if err != nil {
		return fmt.Errorf("shutdown containerd: %v", err)
	}

	err = r.SyncDisks()
	if err != nil {
		return fmt.Errorf("sync disks: %v", err)
	}

	return nil
}

// DeletePods deletes all pods running on a node.
func (r Remote) DeletePods() error {
	cmd := fmt.Sprintf(
		"kubectl delete pods --all-namespaces=true --field-selector spec.nodeName=%s --grace-period=10 --timeout=10s",
		r.Name,
	)

	err := r.Run(cmd)
	if err != nil {
		r.Log("graceful pod deletion failed (using force next): %v", err)
	}

	cmd = fmt.Sprintf(
		"kubectl delete pods --all-namespaces=true --field-selector spec.nodeName=%s --force=true --grace-period=0",
		r.Name,
	)

	return r.Run(cmd)
}

// SyncDisks calls sync to flush dirty pages to disk.
func (r Remote) SyncDisks() error {
	err := r.SyncDir("/")
	if err != nil {
		return err
	}

	err = r.SyncDir("/var/lib/containerd")
	if err != nil {
		return err
	}

	err = r.SyncDir("/opt/local-path-provisioner")
	if err != nil {
		return err
	}

	r.Log("waiting for dirty pages sync")

	err = r.awaitDirtyPagesSync()
	if err != nil {
		return fmt.Errorf("await dirty pages sync: %v", err)
	}

	return nil
}

func (r Remote) awaitDirtyPagesSync() error {
	watermark := 0

	for {
		dirtyKB, err := r.localDirtyPagesMinimum()
		if err != nil {
			return fmt.Errorf("find minimum dirty pages: %v", err)
		}

		if dirtyKB <= watermark {
			return nil
		}

		r.Log("dirty pages: %d kb", dirtyKB)

		watermark += 1
		watermark *= 2

		if watermark > 100 {
			watermark = 100
		}
	}
}

func (r Remote) localDirtyPagesMinimum() (int, error) {
	oldKB, err := r.dirtyPages()
	if err != nil {
		return 0, fmt.Errorf("load dirty pages: %v", err)
	}

	if oldKB == 0 {
		return 0, nil
	}

	for {
		time.Sleep(1 * time.Second)

		currentKB, err := r.dirtyPages()
		if err != nil {
			return 0, fmt.Errorf("load dirty pages: %v", err)
		}

		if currentKB == 0 {
			return 0, nil
		}

		if currentKB > oldKB {
			return oldKB, nil
		}

		oldKB = currentKB
	}
}

func (r Remote) dirtyPages() (int, error) {
	line, err := r.RunFetchStdout("cat /proc/meminfo | grep -e Dirty")
	if err != nil {
		return 0, fmt.Errorf("read dirty pages: %v", err)
	}

	numbers := regexp.MustCompile("[0-9]+")
	kBstr := numbers.FindString(line)

	kB, err := strconv.Atoi(kBstr)
	if err != nil {
		return 0, fmt.Errorf("parse dirty pages: %v", err)
	}

	return kB, nil
}

// SyncDir calls syncs to flush a directory.
func (r Remote) SyncDir(path string) error {
	exists, err := r.DirExists(path)
	if err != nil {
		return fmt.Errorf("sync disk: %v", err)
	}

	if !exists {
		return nil
	}

	r.Log("syncing %s", path)

	err = r.Run(fmt.Sprintf("sync %s", path))
	if err != nil {
		return fmt.Errorf("sync disk: %v", err)
	}

	return nil
}

// Reboot restarts the server via the 'reboot' command.
// The disk gets sync before rebooting.
func (r Remote) Reboot() error {
	r.Log("rebooting")

	err := r.Run("nohup sh -c 'sleep 2 && reboot' &>/dev/null &")
	if err != nil {
		return fmt.Errorf("reboot command: %v", err)
	}

	return nil
}

// StopService stops a service via systemctl.
func (r Remote) StopService(svc string) error {
	r.Log("stopping %s", svc)

	err := r.Run(fmt.Sprintf("systemctl stop %v", svc))
	if err != nil {
		return fmt.Errorf("stop service %s: %v", svc, err)
	}

	return nil
}

// RestartService restarts a service via systemctl.
func (r Remote) RestartService(svc string) error {
	r.Log("restart %s", svc)

	err := r.Run("systemctl daemon-reload")
	if err != nil {
		return fmt.Errorf("restart service %s: %v", svc, err)
	}

	err = r.Run(fmt.Sprintf("systemctl restart %v", svc))
	if err != nil {
		return fmt.Errorf("restart service %s: %v", svc, err)
	}

	waitForServiceToBecomeActive := fmt.Sprintf("timeout %d sh -c 'while ! systemctl is-active --quiet \"%s\"; do sleep 1; done'", int64(time.Minute.Seconds()), svc)
	err = r.Run(waitForServiceToBecomeActive)
	if err != nil {
		return fmt.Errorf("%s did not become active", svc)
	}

	return nil
}

// AwaitReady blocks until the clusters becomes ready.
func (r Remote) AwaitReady(skipAwaitReady bool) error {
	if skipAwaitReady {
		r.Log("[SKIP] wait for 'status k8s' to become ready")
		return nil
	}

	r.Log("wait for 'status k8s' to become ready")

	cmd := fmt.Sprintf("timeout %d sh -c 'while ! status.sh; do sleep 5; done'", int64(time.Hour.Seconds()))

	err := r.Run(cmd)
	if err != nil {
		return fmt.Errorf("cluster status did not become ready")
	}

	return nil
}

// AwaitDNS blocks until cluster domain resolves to API server.
func (r Remote) AwaitDNS(domain string) error {
	r.Log("clean up old DNS entries")

	err := r.Run("kubectl -n kube-system delete job cleanup-dns --ignore-not-found")
	if err != nil {
		return fmt.Errorf("delete clean up DNS job: %v", err)
	}

	err = r.Run("kubectl -n kube-system create job cleanup-dns --from=cronjob/apiserver-dns-cleanup")
	if err != nil {
		return fmt.Errorf("create job to clean up DNS: %v", err)
	}

	time.Sleep(2 * time.Second)

	err = r.Run("kubectl -n kube-system wait --timeout=600s --for=condition=complete job/cleanup-dns")
	if err != nil {
		return fmt.Errorf("wait for DNS clean up job to finish: %v", err)
	}

	r.Log("await DNS TTL (120 seconds)")

	time.Sleep(120 * time.Second)

	r.Log("wait for cluster domain to resolve")

	cmd := fmt.Sprintf(
		"timeout %d sh -c \"while ! dig %s | grep -v ';' | sed '/^$/d' | grep %s; do sleep 5; done\"",
		int64((10 * time.Minute).Seconds()),
		domain,
		domain)

	err = r.Run(cmd)
	if err != nil {
		return fmt.Errorf("cluster domain did not resolve: %v", err)
	}

	return nil
}

// AwaitNodeReady blocks until node becomes ready.
func (r Remote) AwaitNodeReady(node string) error {
	r.Log("wait for node %s to become ready", node)

	cmd := fmt.Sprintf(
		"timeout %d sh -c \"while ! kubectl get node %s | grep ' Ready '; do sleep 5; done\"",
		int64((10 * time.Minute).Seconds()),
		node)

	err := r.Run(cmd)
	if err != nil {
		return fmt.Errorf("node did not become ready: %v", err)
	}

	return nil
}

// Touch creates a file.
func (r Remote) Touch(path string) error {
	err := r.Run(fmt.Sprintf("touch '%s'", path))
	if err != nil {
		return fmt.Errorf("touch %s: %v", path, err)
	}

	return nil
}

// Unlink removes a file.
func (r Remote) Unlink(path string) error {
	err := r.Run(fmt.Sprintf("unlink '%s'", path))
	if err != nil {
		return fmt.Errorf("unlink %s: %v", path, err)
	}

	return nil
}

// MkDir creates a directory recursively.
func (r Remote) MkDir(path string) error {
	err := r.Run(fmt.Sprintf("mkdir -p '%s'", path))
	if err != nil {
		return fmt.Errorf("create directory %s: %v", path, err)
	}

	return nil
}

// Remove removess a directory recursively.
func (r Remote) Remove(path string) error {
	err := r.Run(fmt.Sprintf("rm -fr '%s'", path))
	if err != nil {
		return fmt.Errorf("delete %s: %v", path, err)
	}

	return nil
}

// TempDir creates and returns the path to a tempdir.
func (r Remote) TempDir(suffix string) (string, error) {
	out, err := r.RunFetchStdout("mktemp --directory --suffix=-" + shellescape.Quote(suffix))
	if err != nil {
		return "", fmt.Errorf("generate random temp directory: %v", err)
	}

	path := strings.TrimSpace(out)

	return path, nil
}

// CertKey calculates a new kubeadm certificate-key
// and uploads the certs to etcd with the new key.
func (r Remote) CertKey() (string, error) {
	out, err := r.RunFetchStdout("kubeadm certs certificate-key")
	if err != nil {
		return "", fmt.Errorf("generate random cert key: %v", err)
	}

	key := strings.TrimSpace(out)

	err = r.Run(fmt.Sprintf("kubeadm init phase upload-certs --upload-certs --certificate-key=%s", key))
	if err != nil {
		return "", fmt.Errorf("upload certs with new key: %v", err)
	}

	return key, nil
}

// JoinToken creates a new join token for kubeadm.
func (r Remote) JoinToken() (string, error) {
	cmd, err := r.RunFetchStdout("kubeadm token create --ttl 10m")
	if err != nil {
		return "", fmt.Errorf("create join token: %v", err)
	}

	return strings.TrimSpace(cmd), nil
}

// CAHash calculates the hash of the ca certificate.
func (r Remote) CAHash() (string, error) {
	// https://github.com/kubernetes/kubeadm/issues/659#issuecomment-392822587
	cmd, err := r.RunFetchStdout("openssl x509 -in /etc/kubernetes/pki/ca.crt -noout -pubkey | openssl rsa -pubin -outform DER 2>/dev/null | sha256sum | cut -d' ' -f1")
	if err != nil {
		return "", fmt.Errorf("hash ca cert: %v", err)
	}

	return strings.TrimSpace(cmd), nil
}

// Drain shifts all workload from the node.
func (r Remote) Drain(node string, skipDrain bool) error {
	if skipDrain {
		r.Log("[SKIP] drain %s", node)
		return nil
	}

	r.Log("drain %s", node)

	cmd := fmt.Sprintf(
		"kubectl drain %s --delete-emptydir-data=true --force=true --ignore-daemonsets=true --timeout=%ds",
		node,
		int64(1.5*time.Hour.Seconds()),
	)
	err := r.RunStdout(cmd)
	if err == nil {
		return nil
	}

	cmd = fmt.Sprintf(
		"kubectl drain %s --delete-emptydir-data=true --force=true --ignore-daemonsets=true --timeout=%ds --disable-eviction=true --skip-wait-for-delete-timeout=%d",
		node,
		int64(time.Hour.Seconds()),
		int64((10 * time.Minute).Seconds()),
	)
	err = r.RunStdout(cmd)
	if err != nil {
		return fmt.Errorf("drain node %s: %v", node, err)
	}

	return nil
}

// Uncordon allows the pods to be scheduled on the node.
func (r Remote) Uncordon(node string, skipDrain bool) error {
	if skipDrain {
		r.Log("[SKIP] uncordon %s", node)
		return nil
	}

	r.Log("uncordon %s", node)

	err := r.Run(fmt.Sprintf("kubectl uncordon %s", node))
	if err != nil {
		return fmt.Errorf("uncordon node %s: %v", node, err)
	}

	return nil
}

// Exists checks if a kubernetes resource exists.
func (r Remote) Exists(namespace, resource string) bool {
	err := r.Run(fmt.Sprintf(`kubectl -n "%s" get "%s"`, namespace, resource))
	return err == nil
}

// FileExists verifies a file path exists.
func (r Remote) FileExists(name string) (bool, error) {
	output, err := r.RunFetchStdout(fmt.Sprintf(`test -f "%s" && echo "exists" || echo "missing"`, name))
	if err != nil {
		return false, err
	}

	output = strings.TrimSpace(output)

	switch output {
	case "exists":
		return true, nil
	case "missing":
		return false, nil
	default:
		return false, fmt.Errorf("output \"%s\" not recognized", output)
	}
}

// DirExists verifies a file path exists.
func (r Remote) DirExists(name string) (bool, error) {
	output, err := r.RunFetchStdout(fmt.Sprintf(`test -d "%s" && echo "exists" || echo "missing"`, name))
	if err != nil {
		return false, err
	}

	output = strings.TrimSpace(output)

	switch output {
	case "exists":
		return true, nil
	case "missing":
		return false, nil
	default:
		return false, fmt.Errorf("output \"%s\" not recognized", output)
	}
}

// Scale changes the replica count of a kubernetes resource.
func (r Remote) Scale(namespace, resource string, replicas int) error {
	err := r.Run(fmt.Sprintf(`kubectl -n "%s" scale --replicas=%d "%s"`, namespace, replicas, resource))
	if err != nil {
		return fmt.Errorf("scale %v to %d replicas: %v", resource, replicas, err)
	}

	return nil
}

// DisableCronjob stops new jobs from a cronjob being scheduled.
func (r Remote) DisableCronjob(namespace, resource string) error {
	err := r.Run(fmt.Sprintf(`kubectl -n "%s" patch cronjobs "%s" -p '{"spec" : {"suspend" : true }}'`, namespace, resource))
	if err != nil {
		return fmt.Errorf("suspend cronjob %s: %v", resource, err)
	}

	return nil
}

// EnableCronjob activates a given cronjob.
func (r Remote) EnableCronjob(namespace, resource string) error {
	err := r.Run(fmt.Sprintf(`kubectl -n "%s" patch cronjobs "%s" -p '{"spec" : {"suspend" : false }}'`, namespace, resource))
	if err != nil {
		return fmt.Errorf("active cronjob %s: %v", resource, err)
	}

	return nil
}

// DeleteDisk removes all data from a disk.
func (r Remote) DeleteDisk(path string) error {
	err := r.Run(fmt.Sprintf("blkdiscard %s", path))
	if err != nil {
		return fmt.Errorf("execute blkdiscard: %v", err)
	}

	err = r.Run("partprobe")
	if err != nil {
		return fmt.Errorf("partprobe: %v", err)
	}

	return nil
}

// DDDisk removes the fist 100Mi from the disk.
func (r Remote) DDDisk(path string) error {
	err := r.Run(fmt.Sprintf("dd if=/dev/zero of=%s bs=1M count=100", path))
	if err != nil {
		return fmt.Errorf("execute blkdiscard: %v", err)
	}

	err = r.Run("partprobe")
	if err != nil {
		return fmt.Errorf("partprobe: %v", err)
	}

	return nil
}
