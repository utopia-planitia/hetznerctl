package hetznerctl

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
)

// Dependencies prints out the dependencies in dot format.
// The dependencies are defined per service in dependencies.dot.
func Dependencies(w io.Writer, services []string) error {
	dot := ""

	dot += "digraph {\n"
	dot += "define(digraph,subgraph)\n"

	for _, svc := range services {
		file := filepath.Join("services", svc, "dependencies.dot")
		if !fileExists(file) {
			continue
		}

		dot += "include(" + file + ")\n"
	}

	dot += "}\n"

	_, err := w.Write([]byte(dot))
	if err != nil {
		return fmt.Errorf("write output: %v", err)
	}

	return nil
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}

	return !info.IsDir()
}
