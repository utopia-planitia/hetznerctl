package hetznerctl

import (
	"fmt"
	"log"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/utopia-planitia/hetznerctl/pkg/remote"
)

const (
	kubeadmConfigPath = "/root/kubeadm.yaml"
	kubeadmPatchDir   = "/root/kubeadm/"

	kubeletConfigPath   = "/root/kubelet.yaml"
	kubeproxyConfigPath = "/root/kubeproxy.yaml"
)

type adminitrationTask func(r remote.Remote) error

// SetupKubeadm installs kubernetes on the nodes.
// First the primary master is created, then the addons are installed,
// the backup masters join serially, and then the worker nodes join.
func (ss Servers) SetupKubeadm(cfg Config, verbose bool, helmfileSyncTries int) error {
	master, err := ss.PrimaryMaster()
	if err != nil {
		return fmt.Errorf("selecting primary master: %v", err)
	}

	masterSSH, err := master.remoteOperatingSystem()
	if err != nil {
		return fmt.Errorf("connect primary master: %v", err)
	}
	defer masterSSH.Close()

	nodeCfg := cfg.nodeConfig(master)

	err = clusterInit(nodeCfg)(masterSSH)
	if err != nil {
		return fmt.Errorf("init cluster: %v", err)
	}

	err = master.CreateUsers(cfg)
	if err != nil {
		return fmt.Errorf("create users: %v", err)
	}

	err = master.ApplyServices(cfg, []string{"addons"}, false, verbose, helmfileSyncTries, []string{"phase=setup"})
	if err != nil {
		return err
	}

	err = master.AwaitDNS(cfg.APIDomain())
	if err != nil {
		return err
	}

	err = masterSSH.AwaitNodeReady(master.Name)
	if err != nil {
		return err
	}

	masters := ss.SelectBackupMasters()

	for _, s := range masters {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return s.error("ssh connection: %v", err)
		}
		defer ssh.Close()

		nodeCfg = cfg.nodeConfig(s)

		err = masterJoin(nodeCfg, masterSSH)(ssh)
		if err != nil {
			return s.error("join master: %v", err)
		}

		err = masterSSH.AwaitNodeReady(s.Name)
		if err != nil {
			return err
		}
	}

	workers := ss.SelectWorkers()

	for _, s := range workers {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return s.error("ssh connection: %v", err)
		}
		defer ssh.Close()

		nodeCfg = cfg.nodeConfig(s)

		err = workerJoin(nodeCfg, masterSSH)(ssh)
		if err != nil {
			return fmt.Errorf("join worker: %v", err)
		}
	}

	err = master.AnnotateNodes(cfg)
	if err != nil {
		return fmt.Errorf("annotate nodes: %v", err)
	}

	err = master.PatchAPIService(cfg)
	if err != nil {
		return fmt.Errorf("annotate nodes: %v", err)
	}

	return nil
}

// InitCluster creates a new cluster on the node.
// The node becomes the first master node.
func (s Server) InitCluster(cfg Config) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return s.error("ssh connection: %v", err)
	}
	defer ssh.Close()

	nodeCfg := cfg.nodeConfig(s)

	err = clusterInit(nodeCfg)(ssh)
	if err != nil {
		return fmt.Errorf("%v: %v", s.Name, err)
	}

	return nil
}

// CreateUsers creates and enabled/disables cluster role bindings.
func (s Server) CreateUsers(cfg Config) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return s.error("ssh connection: %v", err)
	}
	defer ssh.Close()

	for _, user := range cfg.Users {
		manageUser := createUser
		if !user.Active {
			manageUser = deleteUser
		}

		err = manageUser(ssh, user)
		if err != nil {
			return err
		}
	}

	return nil
}

func createUser(ssh remote.Remote, u User) error {
	// select user roles from https://kubernetes.io/docs/reference/access-authn-authz/rbac/#user-facing-roles
	cmd := fmt.Sprintf(
		"kubectl create clusterrolebinding %s --clusterrole=%s --user=%s --dry-run=client -o yaml | kubectl apply -f -",
		u.Name,
		u.Role,
		u.Name)

	if u.Namespace != "" {
		err := createNamespace(ssh, u.Namespace)
		if err != nil {
			return fmt.Errorf("create namespace %s: %v", u.Namespace, err)
		}

		cmd = fmt.Sprintf(
			"kubectl create -n %s rolebinding %s --clusterrole=%s --user=%s --dry-run=client -o yaml | kubectl apply -n %s -f -",
			u.Namespace,
			u.Name,
			u.Role,
			u.Name,
			u.Namespace)
	}

	err := ssh.RunStdout(cmd)
	if err != nil {
		return fmt.Errorf("create user %s: %v", u.Name, err)
	}

	return nil
}

func createNamespace(ssh remote.Remote, name string) error {
	cmd := fmt.Sprintf("kubectl create namespace %s --dry-run=client -o yaml | kubectl apply -f -", name)

	err := ssh.RunStdout(cmd)
	if err != nil {
		return err
	}

	return nil
}

func deleteUser(ssh remote.Remote, u User) error {
	cmd := fmt.Sprintf(
		"kubectl create clusterrolebinding %s --clusterrole=%s --user=%s --dry-run=client -o yaml | kubectl delete -f - --ignore-not-found=true",
		u.Name,
		u.Role,
		u.Name)

	if u.Namespace != "" {
		cmd = fmt.Sprintf(
			"kubectl create -n %s rolebinding %s --clusterrole=%s --user=%s --dry-run=client -o yaml | kubectl delete -n %s -f - --ignore-not-found=true",
			u.Namespace,
			u.Name,
			u.Role,
			u.Name,
			u.Namespace)
	}

	err := ssh.RunStdout(cmd)
	if err != nil {
		return fmt.Errorf("delete user %s: %v", u.Name, err)
	}

	return nil
}

// AnnotateNodes adds annotations to the nodes.
func (s Server) AnnotateNodes(cfg Config) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return s.error("ssh connection: %v", err)
	}
	defer ssh.Close()

	ssh.Log("annotate nodes")

	for _, n := range cfg.Nodes {
		cmd := fmt.Sprintf(`kubectl annotate node %s \
            --overwrite=true \
            tilt.dev/registry- \
			tilt.dev/registry-from-cluster-`,
			n.Name)

		err := ssh.Run(cmd)
		if err != nil {
			return fmt.Errorf("annotate node %s: %v", n.Name, err)
		}
	}

	return nil
}

// PatchAPIService avoids connection drops via session stickiness.
func (s Server) PatchAPIService(cfg Config) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return s.error("ssh connection: %v", err)
	}
	defer ssh.Close()

	ssh.Log("patch API service")

	cmd := `kubectl -n default patch service kubernetes -p '{"spec":{"sessionAffinity":"ClientIP"}}'`
	err = ssh.Run(cmd)
	if err != nil {
		return fmt.Errorf("patch API service: %v", err)
	}

	return nil
}

// JoinMasters joins the nodes as a masters to the cluster.
func (ss Servers) JoinMasters(primaryMaster Server, cfg Config) error {
	masterSSH, err := primaryMaster.remoteOperatingSystem()
	if err != nil {
		return primaryMaster.error("ssh connnection: %v", err)
	}
	defer masterSSH.Close()

	for _, s := range ss {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return s.error("ssh connection: %v", err)
		}
		defer ssh.Close()

		nodeCfg := cfg.nodeConfig(s)

		err = masterJoin(nodeCfg, masterSSH)(ssh)
		if err != nil {
			return s.error("join master: %v", err)
		}
	}

	return nil
}

// JoinWorker joins the node as a worker to the cluster.
func (ss Servers) JoinWorker(master Server, cfg Config) error {
	masterSSH, err := master.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer masterSSH.Close()

	for _, s := range ss {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return err
		}
		defer ssh.Close()

		nodeCfg := cfg.nodeConfig(s)

		err = workerJoin(nodeCfg, masterSSH)(ssh)
		if err != nil {
			return s.error("join worker: %v", err)
		}
	}

	return nil
}

func clusterInit(cfg NodeConfig) adminitrationTask {
	task := initCluster(cfg.Config)
	task = installStatusScript(task)
	task = initMasterNode(cfg, task)
	task = kubeadmPatches(cfg.Config, task)
	task = initClusterConfig(cfg.Config, task)

	return task
}

func masterJoin(cfg NodeConfig, masterSSH remote.Remote) adminitrationTask {
	task := joinMaster(cfg.Config)
	task = installStatusScript(task)
	task = initMasterNode(cfg, task)
	task = kubeadmPatches(cfg.Config, task)
	task = joinMasterConfig(masterSSH, cfg.Config, task)

	return task
}

func workerJoin(cfg NodeConfig, masterSSH remote.Remote) adminitrationTask {
	task := joinWorker()
	task = initWorkerNode(cfg, task)
	task = kubeadmPatches(cfg.Config, task)
	task = joinWorkerConfig(masterSSH, cfg.Config, task)

	return task
}

func joinWorker() adminitrationTask {
	return func(r remote.Remote) error {
		r.Log("join worker")

		err := r.RunStdout(fmt.Sprintf("kubeadm join --config %s", kubeadmConfigPath))
		if err != nil {
			return err
		}

		return nil
	}
}

func installCNI(r remote.Remote, cfg Config) error {
	// cni
	r.Log("install CNI")

	err := r.Run(fmt.Sprintf(`
	set -o errexit
	if set +o | grep -F 'set +o pipefail'; then
		set -o pipefail
	fi
	if test -x /opt/cni/bin/portmap && /opt/cni/bin/portmap version | grep "%s"; then
		exit 0
	fi
	mkdir -p /opt/cni/bin
	curl --fail --silent --show-error --location --max-time 120 \
			"https://github.com/containernetworking/plugins/releases/download/%s/cni-plugins-linux-amd64-%s.tgz" \
			| tar -C /opt/cni/bin -xz
		`, CNI_VERSION, CNI_VERSION, CNI_VERSION))
	if err != nil {
		return err
	}

	return nil
}

func installKubeadm(r remote.Remote, cfg Config) error {
	// kubeadm
	r.Log("install kubeadm")

	err := r.Run(fmt.Sprintf(`
set -eux
export PATH=/opt/bin:$PATH

kubeadm_is_expected_version() {
	kubeadm version | tee --append /dev/stderr | grep --fixed-strings --quiet '%s'
}

if kubeadm_is_expected_version; then
	exit 0
fi

curl --fail --silent --show-error --location --max-time 120 \
	--output /opt/bin/kubeadm_ \
	https://dl.k8s.io/release/%s/bin/linux/amd64/kubeadm
chmod +x /opt/bin/kubeadm_
mv /opt/bin/kubeadm_ /opt/bin/kubeadm

if ! kubeadm_is_expected_version; then
	exit 1
fi
`, KUBERNETES_VERSION, KUBERNETES_VERSION))
	if err != nil {
		return err
	}

	return nil
}

func installKubectl(r remote.Remote, cfg NodeConfig) error {
	r.Log("setup kubectl")

	err := r.Run(fmt.Sprintf(`
set -eux
export PATH=/opt/bin:$PATH

kubectl_is_expected_version() {
	kubectl version --client | tee --append /dev/stderr | grep --fixed-strings --quiet '%s'
}

if kubectl_is_expected_version; then
	exit 0
fi

curl --fail --silent --show-error --location --max-time 120 \
	--output /opt/bin/kubectl_ \
	https://dl.k8s.io/release/%s/bin/linux/amd64/kubectl
chmod +x /opt/bin/kubectl_
mv /opt/bin/kubectl_ /opt/bin/kubectl

if ! kubectl_is_expected_version; then
	exit 1
fi
`, KUBERNETES_VERSION, KUBERNETES_VERSION))
	if err != nil {
		return err
	}

	err = r.Run(`
		mkdir -p /root/.kube
		if [ -f /root/.kube/config ] || [ -L /root/.kube/config ]; then unlink /root/.kube/config; fi
		ln -s /etc/kubernetes/admin.conf /root/.kube/config`)
	if err != nil {
		return err
	}

	err = r.Run(`grep "kubectl completion bash" ~/.bashrc > /dev/null || echo "source <(kubectl completion bash)" >> ~/.bashrc`)
	if err != nil {
		return err
	}

	return nil
}

func installKubelet(r remote.Remote, cfg NodeConfig) error {
	r.Log("setup kubelet")

	changed, err := modifyRenderedAsset(r, "/etc/sysconfig/kubelet", "kubelet.tpl", cfg)
	if err != nil {
		return err
	}

	kubeletBinaryExists, err := r.FileExists("/usr/bin/kubelet")
	if err != nil {
		return err
	}

	err = r.Run(fmt.Sprintf(`
set -eux
export PATH=/opt/bin:$PATH

kubelet_is_expected_version() {
	kubelet --version | tee --append /dev/stderr | grep --fixed-strings --quiet '%s'
}

if kubelet_is_expected_version; then
	exit 0
fi

curl --fail --silent --show-error --location --max-time 120 \
	--output /opt/bin/kubelet_ \
	https://dl.k8s.io/release/%s/bin/linux/amd64/kubelet
chmod +x /opt/bin/kubelet_
mv /opt/bin/kubelet_ /opt/bin/kubelet

if ! kubelet_is_expected_version; then
	exit 1
fi
`, KUBERNETES_VERSION, KUBERNETES_VERSION))
	if err != nil {
		return err
	}

	err = r.Run(fmt.Sprintf(`
	curl --fail --silent --show-error --location --max-time 120 \
		"https://raw.githubusercontent.com/kubernetes/release/%s/cmd/krel/templates/latest/kubelet/kubelet.service" \
		| sed "s:/usr/bin:/opt/bin:g" \
		> /etc/systemd/system/kubelet.service_
	mv /etc/systemd/system/kubelet.service_ /etc/systemd/system/kubelet.service

	mkdir -p /etc/systemd/system/kubelet.service.d
	curl --fail --silent --show-error --location --max-time 120 \
		"https://raw.githubusercontent.com/kubernetes/release/%s/cmd/krel/templates/latest/kubeadm/10-kubeadm.conf" \
		| sed "s:/usr/bin:/opt/bin:g" \
		> /etc/systemd/system/kubelet.service.d/10-kubeadm.conf_
	mv /etc/systemd/system/kubelet.service.d/10-kubeadm.conf_ /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

	systemctl enable --now kubelet
	`, RELEASE_VERSION, RELEASE_VERSION))
	if err != nil {
		return err
	}

	if changed && kubeletBinaryExists {
		err = r.RestartService("kubelet")
		if err != nil {
			return err
		}
	}

	return nil
}

func initMasterNode(cfg NodeConfig, next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		err := installCNI(r, cfg.Config)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = installKubeadm(r, cfg.Config)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = installKubectl(r, cfg)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = installKubelet(r, cfg)
		if err != nil {
			return err
		}

		err = next(r)
		if err != nil {
			return err
		}

		return nil
	}
}

func initWorkerNode(cfg NodeConfig, next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		err := installCNI(r, cfg.Config)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = installKubeadm(r, cfg.Config)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = installKubelet(r, cfg)
		if err != nil {
			return err
		}

		err = next(r)
		if err != nil {
			return err
		}

		return nil
	}
}

func initClusterConfig(cfg Config, next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		r.Log("prepare init cluster configuration")

		initYaml, err := renderAsset("kubeadm-init.yaml.tpl", cfg)
		if err != nil {
			return err
		}

		clusterYaml, err := renderAsset("kubeadm-cluster.yaml.tpl", cfg)
		if err != nil {
			return err
		}

		kubeproxyYaml, err := renderAsset("kubeproxy-config.yaml.tpl", cfg)
		if err != nil {
			return err
		}

		kubeletYaml, err := renderAsset("kubelet-config.yaml.tpl", cfg)
		if err != nil {
			return err
		}

		yml := fmt.Sprintf("%s\n---\n%s\n---\n%s\n---\n%s", string(initYaml), string(clusterYaml), string(kubeproxyYaml), string(kubeletYaml))

		err = r.WriteFile([]byte(yml), kubeadmConfigPath, 0644)
		if err != nil {
			return err
		}

		err = next(r)
		if err != nil {
			return err
		}

		err = r.Unlink(kubeadmConfigPath)
		if err != nil {
			return err
		}

		return nil
	}
}

func initCluster(cfg Config) adminitrationTask {
	return func(r remote.Remote) error {
		r.Log("init master node")

		endpoint := fmt.Sprintf("%s.%s", cfg.Cloudflare.Subdomains.API, cfg.Cloudflare.Zone)

		err := r.Run(fmt.Sprintf("echo 127.0.0.1 %s >> /etc/hosts", endpoint))
		if err != nil {
			return fmt.Errorf("add endpoint to local host: %v", err)
		}

		err = r.RunStdout("kubeadm init phase preflight")
		if err != nil {
			return fmt.Errorf("preflight checks: %v", err)
		}

		err = r.RunStdout(fmt.Sprintf("kubeadm init --config %s", kubeadmConfigPath))
		if err != nil {
			return fmt.Errorf("initilize master node: %v", err)
		}

		return nil
	}
}

func installStatusScript(next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		r.Log("install status script")

		sh, err := loadAsset("status.sh")
		if err != nil {
			return err
		}

		err = r.WriteFile(sh, "/opt/bin/status.sh", 0755)
		if err != nil {
			return err
		}

		r.Log("install old status script")

		sh, err = loadAsset("status-deprecated.sh")
		if err != nil {
			return err
		}

		err = r.WriteFile(sh, "/opt/bin/status-deprecated.sh", 0755)
		if err != nil {
			return err
		}

		err = next(r)
		if err != nil {
			return err
		}

		return nil
	}
}

func joinMasterConfig(masterSSH remote.Remote, cfg Config, next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		joinCfg, err := masterJoinConfiguration(masterSSH, cfg)
		if err != nil {
			return err
		}

		yml, err := renderAsset("kubeadm-join.yaml.tpl", joinCfg)
		if err != nil {
			return err
		}

		err = r.WriteFile(yml, kubeadmConfigPath, 0644)
		if err != nil {
			return err
		}

		err = next(r)
		if err != nil {
			return err
		}

		err = r.Unlink(kubeadmConfigPath)
		if err != nil {
			return err
		}

		return nil
	}
}

func joinMaster(cfg Config) adminitrationTask {
	return func(r remote.Remote) error {
		r.Log("join master")

		err := r.RunStdout(fmt.Sprintf("kubeadm join --config %s", kubeadmConfigPath))
		if err != nil {
			return err
		}

		return nil
	}
}

func joinWorkerConfig(masterSSH remote.Remote, cfg Config, next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		joinCfg, err := workerJoinConfiguration(masterSSH, cfg)
		if err != nil {
			return err
		}

		yml, err := renderAsset("kubeadm-join.yaml.tpl", joinCfg)
		if err != nil {
			return err
		}

		err = r.WriteFile(yml, kubeadmConfigPath, 0644)
		if err != nil {
			return err
		}

		err = next(r)
		if err != nil {
			return err
		}

		err = r.Unlink(kubeadmConfigPath)
		if err != nil {
			return err
		}

		return nil
	}
}

func masterJoinConfiguration(master remote.Remote, cfg Config) (JoinConfiguration, error) {
	joinCfg, err := workerJoinConfiguration(master, cfg)
	if err != nil {
		return JoinConfiguration{}, err
	}

	key, err := master.CertKey()
	if err != nil {
		return JoinConfiguration{}, err
	}

	joinCfg.CertificateKey = key

	return joinCfg, nil
}

func workerJoinConfiguration(master remote.Remote, cfg Config) (JoinConfiguration, error) {
	token, err := master.JoinToken()
	if err != nil {
		return JoinConfiguration{}, err
	}

	hash, err := master.CAHash()
	if err != nil {
		return JoinConfiguration{}, err
	}

	joinCfg := JoinConfiguration{
		Config:       cfg,
		CaCertHashes: hash,
		Token:        token,
	}

	return joinCfg, nil
}

// KubeadmUpdate updates the clusters following the instructions
// at https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/
func (ss Servers) KubeadmUpdate(cfg Config, skipDrain, skipAwaitReady, verbose bool) error {
	primaryMaster, err := cfg.Nodes.PrimaryMaster()
	if err != nil {
		return err
	}

	masterSSH, err := primaryMaster.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer masterSSH.Close()

	// only upgrade cluster and addons when primary master is not skipped via --limit
	for range ss.SelectPrimaryMasters() {
		err = masterSSH.AwaitReady(skipAwaitReady)
		if err != nil {
			return err
		}

		nodeCfg := cfg.nodeConfig(primaryMaster)

		err = clusterUpgrade(nodeCfg, skipDrain, masterSSH)(masterSSH)
		if err != nil {
			primaryMaster.log("kubeadm cluster upgrade: %v", err)
			return err
		}
	}

	for _, s := range ss.SelectBackupMasters() {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return err
		}
		defer ssh.Close()

		err = masterSSH.AwaitReady(skipAwaitReady)
		if err != nil {
			return err
		}

		nodeCfg := cfg.nodeConfig(s)

		err = masterUpgrade(nodeCfg, skipDrain, masterSSH)(ssh)
		if err != nil {
			s.log("kubeadm master upgrade: %v", err)
			return err
		}
	}

	for _, s := range ss.SelectWorkers() {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return err
		}
		defer ssh.Close()

		err = masterSSH.AwaitReady(skipAwaitReady)
		if err != nil {
			return err
		}

		nodeCfg := cfg.nodeConfig(s)

		err = workerUpgrade(nodeCfg, skipDrain, masterSSH)(ssh)
		if err != nil {
			s.log("kubeadm node upgrade: %v", err)
			return err
		}
	}

	err = primaryMaster.AnnotateNodes(cfg)
	if err != nil {
		return fmt.Errorf("annotate nodes: %v", err)
	}

	err = primaryMaster.PatchAPIService(cfg)
	if err != nil {
		return fmt.Errorf("annotate nodes: %v", err)
	}

	time.Sleep(5 * time.Second)

	err = masterSSH.AwaitReady(skipAwaitReady)
	if err != nil {
		return err
	}

	return nil
}

func clusterUpgrade(cfg NodeConfig, skipDrain bool, masterSSH remote.Remote) adminitrationTask {
	task := upgradeCluster()
	task = skipLocalEndpoint(cfg.Config, task)
	task = installStatusScript(task)
	task = kubeadmPatches(cfg.Config, task)
	task = upgradeMasterNode(masterSSH, cfg, task)
	task = drainNode(masterSSH, skipDrain, task)

	return task
}

func masterUpgrade(cfg NodeConfig, skipDrain bool, masterSSH remote.Remote) adminitrationTask {
	task := upgradeMaster()
	task = installStatusScript(task)
	task = kubeadmPatches(cfg.Config, task)
	task = upgradeMasterNode(masterSSH, cfg, task)
	task = drainNode(masterSSH, skipDrain, task)

	return task
}

func workerUpgrade(cfg NodeConfig, skipDrain bool, masterSSH remote.Remote) adminitrationTask {
	task := upgradeWorker()
	task = kubeadmPatches(cfg.Config, task)
	task = upgradeWorkerNode(masterSSH, cfg, task)
	task = drainNode(masterSSH, skipDrain, task)

	return task
}

func drainNode(master remote.Remote, skipDrain bool, next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		err := master.Drain(r.Name, skipDrain)
		if err != nil {
			return err
		}

		err = next(r)
		if err != nil {
			return err
		}

		err = master.Uncordon(r.Name, skipDrain)
		if err != nil {
			return err
		}

		return nil
	}
}

func upgradeMasterNode(master remote.Remote, cfg NodeConfig, next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		err := installCNI(r, cfg.Config)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = installKubeadm(r, cfg.Config)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = installKubectl(r, cfg)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = installKubelet(r, cfg)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = next(r)
		if err != nil {
			return err
		}

		err = r.RestartService("kubelet")
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		r.Log("restart kube proxy")

		err = master.Run(fmt.Sprintf("kubectl -n kube-system delete pod -l k8s-app=kube-proxy --field-selector spec.nodeName=%s", r.Name))
		if err != nil {
			return err
		}

		return nil
	}
}

func upgradeWorkerNode(master remote.Remote, cfg NodeConfig, next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		err := installCNI(r, cfg.Config)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = installKubeadm(r, cfg.Config)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = installKubelet(r, cfg)
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		err = next(r)
		if err != nil {
			return err
		}

		err = r.RestartService("kubelet")
		if err != nil {
			return fmt.Errorf("%v: %v", r.Name, err)
		}

		r.Log("restart kube proxy")

		err = master.Run(fmt.Sprintf("kubectl -n kube-system delete pod -l k8s-app=kube-proxy --field-selector spec.nodeName=%s", r.Name))
		if err != nil {
			return err
		}

		return nil
	}
}

// kubeadmPatches creates patch files for the control plane components on the node
func kubeadmPatches(cfg Config, next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		err := r.MkDir(kubeadmPatchDir)
		if err != nil {
			return err
		}

		patches := generatePatches(cfg)

		for _, resource := range []string{"etcd", "kube-apiserver", "kube-controller-manager", "kube-scheduler"} {
			patchYml, err := renderAsset(fmt.Sprintf("patch-%s.yaml.tpl", resource), patches)
			if err != nil {
				return err
			}

			err = r.WriteFile(patchYml, filepath.Join(kubeadmPatchDir, fmt.Sprintf("%s.yaml", resource)), 0644)
			if err != nil {
				return err
			}
		}

		err = next(r)
		if err != nil {
			return err
		}

		err = r.Remove(kubeadmPatchDir)
		if err != nil {
			return err
		}

		return nil
	}
}

func upgradeClusterConfig(cfg Config, next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		kubeadmConfigYaml, err := renderAsset("kubeadm-cluster.yaml.tpl", cfg)
		if err != nil {
			return err
		}

		err = r.WriteFile(kubeadmConfigYaml, kubeadmConfigPath, 0644)
		if err != nil {
			return err
		}

		kubeletConfigYaml, err := renderAsset("kubelet-config.yaml.tpl", cfg)
		if err != nil {
			return err
		}

		err = r.WriteFile(kubeletConfigYaml, kubeletConfigPath, 0644)
		if err != nil {
			return err
		}

		kubeproxyConfigYaml, err := renderAsset("kubeproxy-config.yaml.tpl", cfg)
		if err != nil {
			return err
		}

		err = r.WriteFile(kubeproxyConfigYaml, kubeproxyConfigPath, 0644)
		if err != nil {
			return err
		}

		err = next(r)
		if err != nil {
			return err
		}

		err = r.Unlink(kubeadmConfigPath)
		if err != nil {
			return err
		}

		err = r.Unlink(kubeletConfigPath)
		if err != nil {
			return err
		}

		err = r.Unlink(kubeproxyConfigPath)
		if err != nil {
			return err
		}

		return nil
	}
}

func skipLocalEndpoint(cfg Config, next adminitrationTask) adminitrationTask {
	return func(r remote.Remote) error {
		endpoint := fmt.Sprintf("%s.%s", cfg.Cloudflare.Subdomains.API, cfg.Cloudflare.Zone)

		err := r.Run(fmt.Sprintf("sed -i '/%s/d' /etc/hosts", strings.ReplaceAll(endpoint, ".", "\\.")))
		if err != nil {
			return fmt.Errorf("remove endpoint to local host: %v", err)
		}

		defer func() {
			err := r.Run(fmt.Sprintf("echo 127.0.0.1 %s >> /etc/hosts", endpoint))
			if err != nil {
				log.Println(fmt.Errorf("add endpoint to local host: %v", err))
			}
		}()

		err = next(r)
		if err != nil {
			return err
		}

		return nil
	}
}

func upgradeCluster() adminitrationTask {
	return func(r remote.Remote) error {
		// https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/
		r.Log("kubeadm upgrade apply")

		err := r.RunStdout(fmt.Sprintf("kubeadm upgrade apply %s --patches %s --yes", KUBERNETES_VERSION, kubeadmPatchDir))
		if err != nil {
			return err
		}

		return nil
	}
}

func upgradeMaster() adminitrationTask {
	return func(r remote.Remote) error {
		r.Log("upgrade master")

		err := r.RunStdout(fmt.Sprintf("kubeadm upgrade node --patches %s", kubeadmPatchDir))
		if err != nil {
			return err
		}

		return nil
	}
}

func upgradeWorker() adminitrationTask {
	return func(r remote.Remote) error {
		r.Log("upgrade worker")

		err := r.RunStdout("kubeadm upgrade node")
		if err != nil {
			return err
		}

		return nil
	}
}

func applyClusterConfig(verbose bool) adminitrationTask {
	return func(r remote.Remote) error {
		r.Log("applying cluster configuration changes")

		if verbose {
			err := r.RunStdout(fmt.Sprintf("kubectl --namespace=kube-system create configmap kubeadm-config --dry-run=client --from-file=ClusterConfiguration=%s --output=yaml | kubectl --namespace=kube-system diff --filename=- || true", kubeadmConfigPath))
			if err != nil {
				return err
			}
		}

		err := r.RunStdout(fmt.Sprintf("kubectl --namespace=kube-system create configmap kubeadm-config --dry-run=client --from-file=ClusterConfiguration=%s --output=yaml | kubectl --namespace=kube-system patch --patch-file=/dev/stdin configmap/kubeadm-config", kubeadmConfigPath))
		if err != nil {
			return err
		}

		if verbose {
			err = r.RunStdout(fmt.Sprintf("kubectl --namespace=kube-system create configmap kubelet-config --dry-run=client --from-file=kubelet=%s --output=yaml | kubectl --namespace=kube-system diff --filename=- || true", kubeletConfigPath))
			if err != nil {
				return err
			}
		}

		err = r.RunStdout(fmt.Sprintf("kubectl --namespace=kube-system create configmap kubelet-config --dry-run=client --from-file=kubelet=%s --output=yaml | kubectl --namespace=kube-system patch --patch-file=/dev/stdin configmap/kubelet-config", kubeletConfigPath))
		if err != nil {
			return err
		}

		if verbose {
			err = r.RunStdout(fmt.Sprintf("kubectl --namespace=kube-system create configmap kube-proxy --dry-run=client --from-file=config.conf=%s --output=yaml | kubectl --namespace=kube-system diff --filename=- || true", kubeproxyConfigPath))
			if err != nil {
				return err
			}
		}

		err = r.RunStdout(fmt.Sprintf("kubectl --namespace=kube-system create configmap kube-proxy --dry-run=client --from-file=config.conf=%s --output=yaml | kubectl --namespace=kube-system patch --patch-file=/dev/stdin configmap/kube-proxy", kubeproxyConfigPath))
		if err != nil {
			return err
		}

		return nil
	}
}

// ResetKubeadm uses kubeadm to reset kubernetes on each node.
func (ss Servers) ResetKubeadm() error {
	var err error

	for _, s := range ss {
		err = s.ResetKubeadm()
		if err != nil {
			log.Printf("%v: %v", s.Name, err)
		}
	}

	return err
}

// ResetKubeadm uses kubeadm to reset kubernetes.
func (s Server) ResetKubeadm() error {
	s.log("reset node")

	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer ssh.Close()

	_ = ssh.RunStdout("kubeadm reset -f")
	_ = ssh.RunStdout("crictl pods | tail -n+2 | grep --invert-match calico | awk '{ print $1 }' | xargs --no-run-if-empty -L 1 crictl rmp -f")
	_ = ssh.RunStdout("crictl pods -q | xargs --no-run-if-empty -L 1 crictl rmp -f")
	_ = ssh.RunStdout("mkdir -p /root/.kube")
	_ = ssh.RunStdout("rm -rf /root/.kube")

	return err
}

func (ss Servers) ReconfigureCluster(cfg Config, skipAwaitReady, verbose bool) error {
	// only reconfigure cluster when primary master is not skipped via --limit
	if len(ss.SelectPrimaryMasters()) == 0 {
		log.Println("reconfigure cluster: could not find required primary master in selected nodes")
		return nil
	}

	primaryMaster, err := cfg.Nodes.PrimaryMaster()
	if err != nil {
		return err
	}

	masterSSH, err := primaryMaster.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer masterSSH.Close()

	err = masterSSH.AwaitReady(skipAwaitReady)
	if err != nil {
		return err
	}

	nodeCfg := cfg.nodeConfig(primaryMaster)

	err = upgradeConfiguration(nodeCfg, verbose)(masterSSH)
	if err != nil {
		primaryMaster.log("update configuration: %v", err)
		return err
	}

	err = applyConfigurationChanges(nodeCfg)(masterSSH)
	if err != nil {
		primaryMaster.log("apply configuration changes: %v", err)
		return err
	}

	for _, s := range ss.SelectBackupMasters() {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return err
		}
		defer ssh.Close()

		err = masterSSH.AwaitReady(skipAwaitReady)
		if err != nil {
			return err
		}

		err = applyConfigurationChanges(nodeCfg)(ssh)
		if err != nil {
			s.log("apply configuration changes: %v", err)
			return err
		}
	}

	err = masterSSH.AwaitReady(skipAwaitReady)
	if err != nil {
		return err
	}

	return nil
}

func upgradeConfiguration(cfg NodeConfig, verbose bool) adminitrationTask {
	task := applyClusterConfig(verbose)
	return upgradeClusterConfig(cfg.Config, task)
}

// applyConfigurationChanges runs tasks to update the kubeadm, kube-proxy and kubelet configmaps
func applyConfigurationChanges(cfg NodeConfig) adminitrationTask {
	// https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-reconfigure/
	task := upgradeMasterConfig()
	return kubeadmPatches(cfg.Config, task)
}

// upgradeMasterConfig generates static Pod manifest files and restarts kubelet and kube-proxy
func upgradeMasterConfig() adminitrationTask {
	// https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-reconfigure/
	return func(r remote.Remote) error {
		r.Log("apply configuration changes")

		// reconfigure control-plane components including etcd
		err := r.RunStdout(fmt.Sprintf("kubeadm upgrade node phase control-plane --certificate-renewal=false --etcd-upgrade=true --patches %s", kubeadmPatchDir))
		if err != nil {
			return err
		}

		// reconfigure kubelet
		err = r.RunStdout(fmt.Sprintf("kubeadm upgrade node phase kubelet-config --patches %s", kubeadmPatchDir))
		if err != nil {
			return err
		}

		// restart kubelet
		err = r.RunStdout("systemctl daemon-reload")
		if err != nil {
			return err
		}

		err = r.RunStdout("systemctl restart kubelet")
		if err != nil {
			return err
		}

		// restart kube-proxy
		err = r.RunStdout(fmt.Sprintf("kubectl --namespace kube-system delete pod --selector k8s-app=kube-proxy --field-selector spec.nodeName=%s", r.Name))
		if err != nil {
			return err
		}

		return nil
	}
}
