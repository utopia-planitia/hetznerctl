package hetznerctl

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"path/filepath"
	"strings"
	"time"

	"al.essio.dev/pkg/shellescape"
	"gitlab.com/utopia-planitia/hetznerctl/pkg/remote"
)

const (
	HELM_RETRIES = 3
)

func (s Server) SetupAddons(cfg Config, delete, verbose bool, tries int) error {
	err := s.ApplyServices(cfg, []string{"addons"}, false, verbose, tries, []string{"phase=setup"})
	if err != nil {
		return err
	}

	return nil
}

func (s Server) DeployServices(cfg Config, services []string, delete, verbose bool, tries int) error {
	return s.ApplyServices(cfg, services, delete, verbose, tries, []string{})
}

func (s Server) ApplyServices(cfg Config, services []string, delete, verbose bool, tries int, selectors []string) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}

	dest, err := ssh.TempDir("hetznerctl-services")
	if err != nil {
		return err
	}
	defer func() {
		err := ssh.Remove(dest)
		if err != nil {
			s.log("delete %s: %v", dest, err)
		}
	}()

	for _, service := range services {
		s.log("upload service %s", service)

		err = ssh.UploadFolderViaTar(filepath.Join("services", service), filepath.Join(dest, service))
		if err != nil {
			return fmt.Errorf("upload service files to kubernetes master: %v", err)
		}
	}

	yml := bytes.Buffer{}
	err = PrintConfig(bufio.NewWriter(&yml), cfg)
	if err != nil {
		return fmt.Errorf("merge config with default values: %v", err)
	}

	err = ssh.WriteFile(yml.Bytes(), filepath.Join(dest, "cluster.yaml"), 0644)
	if err != nil {
		return fmt.Errorf("upload cluster configuration: %v", err)
	}

	allHelmfiles, err := listHelmfiles(services)
	if err != nil {
		return err
	}

	if len(selectors) != 0 {
		for _, helmfile := range allHelmfiles {
			s.log("apply filefile %s", helmfile)

			err := applyRemoteHelmfile(ssh, dest, helmfile, selectors, verbose, tries)
			if err != nil {
				return err
			}
		}

		return nil
	}

	if delete {
		for _, helmfile := range allHelmfiles {
			s.log("destroy helm release with %s", helmfile)

			err := destroyRemoteHelmfile(ssh, dest, helmfile, []string{}, verbose, tries)
			if err != nil {
				return err
			}
		}

		return nil
	}

	for _, helmfile := range allHelmfiles {
		s.log("phase=delete with %s", helmfile)

		err := applyRemoteHelmfile(ssh, dest, helmfile, []string{"phase=delete"}, verbose, tries)
		if err != nil {
			return err
		}
	}

	for _, helmfile := range allHelmfiles {
		s.log("phase=crds with %s", helmfile)

		err := applyRemoteHelmfile(ssh, dest, helmfile, []string{"phase=crds"}, verbose, tries)
		if err != nil {
			return err
		}
	}

	for _, helmfile := range allHelmfiles {
		s.log("phase=init (setup) with %s", helmfile)

		err := applyRemoteHelmfile(ssh, dest, helmfile, []string{"phase=init"}, verbose, tries)
		if err != nil {
			return err
		}
	}

	for _, helmfile := range allHelmfiles {
		s.log("apply %s", helmfile)

		err := applyRemoteHelmfile(ssh, dest, helmfile, []string{"phase!=delete,phase!=crds,phase!=init,phase!=only-testing"}, verbose, tries)
		if err != nil {
			return err
		}
	}

	return nil
}

func listHelmfiles(services []string) ([]string, error) {
	allHelmfiles := []string{}

	for _, service := range services {
		helmfiles, err := listServiceHelmfiles(service)
		if err != nil {
			return []string{}, err
		}

		allHelmfiles = append(allHelmfiles, helmfiles...)
	}

	return allHelmfiles, nil
}

func listServiceHelmfiles(service string) ([]string, error) {
	matches := []string{}

	yamlMatches, err := filepath.Glob(filepath.Join("services", service, "*", "helmfile.yaml"))
	if err != nil {
		return nil, err
	}

	matches = append(matches, yamlMatches...)

	tplMatches, err := filepath.Glob(filepath.Join("services", service, "*", "helmfile.yaml.tpl"))
	if err != nil {
		return nil, err
	}

	matches = append(matches, tplMatches...)

	helmfiles := []string{}

	for _, helmfilePath := range matches {
		path := strings.TrimPrefix(helmfilePath, "services/")

		helmfiles = append(helmfiles, path)
	}

	return helmfiles, nil
}

func applyRemoteHelmfile(ssh remote.Remote, remoteServicesPath, helmfile string, selectors []string, verbose bool, remainingRetries int) error {
	selectorArgs := "--allow-no-matching-release "
	for _, selector := range selectors {
		selectorArgs += fmt.Sprintf("--selector %s ", shellescape.Quote(selector))
	}

	cmdTemplate := "helmfile --state-values-file=%s --file=%s %s sync --skip-crds"
	clusterYamlPath := filepath.Join(remoteServicesPath, "cluster.yaml")
	helmfilePath := filepath.Join(remoteServicesPath, helmfile)
	cmd := fmt.Sprintf(cmdTemplate, clusterYamlPath, helmfilePath, selectorArgs)

	var logErr error

	if verbose {
		err := ssh.RunStdout(cmd)
		if err != nil {
			logErr = fmt.Errorf("deploy helmfile %s: %v", helmfile, err)
		}
	} else {
		err := ssh.Run(cmd)
		if err != nil {
			logErr = fmt.Errorf("deploy helmfile %s: %v", helmfile, err)
		}
	}

	if logErr == nil {
		return nil
	}

	remainingRetries--

	if remainingRetries <= 0 {
		return logErr
	}

	log.Println(logErr)

	log.Printf("Helmfile sync failed. Sleeping 60s before retry. %d tries remain.", remainingRetries)

	time.Sleep(60 * time.Second)

	return applyRemoteHelmfile(ssh, remoteServicesPath, helmfile, selectors, verbose, remainingRetries)
}

func destroyRemoteHelmfile(ssh remote.Remote, remoteServicesPath, helmfile string, selectors []string, verbose bool, remainingRetries int) error {
	selectorArgs := "--allow-no-matching-release "
	for _, selector := range selectors {
		selectorArgs += fmt.Sprintf("--selector %s ", shellescape.Quote(selector))
	}

	cmdTemplate := "helmfile --state-values-file=%s --file=%s %s destroy"
	clusterYamlPath := filepath.Join(remoteServicesPath, "cluster.yaml")
	helmfilePath := filepath.Join(remoteServicesPath, helmfile)
	cmd := fmt.Sprintf(cmdTemplate, clusterYamlPath, helmfilePath, selectorArgs)

	var logErr error

	if verbose {
		err := ssh.RunStdout(cmd)
		if err != nil {
			logErr = fmt.Errorf("destroy helmfile %s: %v", helmfile, err)
		}
	} else {
		err := ssh.Run(cmd)
		if err != nil {
			logErr = fmt.Errorf("destroy helmfile %s: %v", helmfile, err)
		}
	}

	if logErr == nil {
		return nil
	}

	remainingRetries--

	if remainingRetries <= 0 {
		return logErr
	}

	log.Println(logErr)

	log.Printf("Helmfile destroy failed. Sleeping 60s before retry. %d tries remain.", remainingRetries)

	time.Sleep(60 * time.Second)

	return destroyRemoteHelmfile(ssh, remoteServicesPath, helmfile, selectors, verbose, remainingRetries)
}
