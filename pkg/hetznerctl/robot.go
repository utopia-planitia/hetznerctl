package hetznerctl

import (
	"fmt"
	"os"

	"gitlab.com/utopia-planitia/hetznerctl/pkg/robot"
)

// RobotNode creates access hetzner robot API.
func (s Server) RobotNode() robot.Server {
	return robot.Server{
		Username: s.Hetzner.Username,
		Password: s.Hetzner.Password,
		IP:       s.Network.PublicIP,
	}
}

// FirewallStatus prints the firewall status of each node.
func (ss Servers) FirewallStatus() error {
	for _, s := range ss {
		status, err := s.RobotNode().FirewallStatus()
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}

		os.Stdout.WriteString(status)
	}

	return nil
}

// EnableFirewall enables the firewall of each node.
func (ss Servers) EnableFirewall() error {
	for _, s := range ss {
		err := s.RobotNode().EnableFirewall()
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}

		s.log("firewall is going to be enabled")
	}

	return nil
}

// DisableFirewall disables the firewall of each node.
func (ss Servers) DisableFirewall() error {
	for _, s := range ss {
		err := s.RobotNode().DisableFirewall()
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}

		s.log("firewall is going to be disabled")
	}

	return nil
}

// RescueModeStatus prints the rescue mode status of each node.
func (ss Servers) RescueModeStatus() error {
	for _, s := range ss {
		active, err := s.RobotNode().RescueModeStatus()
		if err != nil {
			return fmt.Errorf("%v", err)
		}

		if active {
			s.log("rescue mode is enabled")
		} else {
			s.log("rescue mode is disabled")
		}
	}

	return nil
}

// RescueModeStatus prints the rescue mode status of each node.
func (ss Servers) RescueModeOptions() error {
	for _, s := range ss {
		options, err := s.RobotNode().RescueModeOptions()
		if err != nil {
			return fmt.Errorf("%v", err)
		}

		s.log("os: %v, arch: %v", options.Os, options.Arch)
	}

	return nil
}

// RescueModePassword prints the rescue mode password of each node.
func (ss Servers) RescueModePassword() error {
	for _, s := range ss {
		password, active, err := s.RobotNode().RescueModePassword()
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}

		if !active {
			s.log("rescue mode is disabled")
			continue
		}

		s.log("rescue mode is enabled, password is %v", password)
	}

	return nil
}

// EnableRescueMode enables the rescue mode of each node.
func (ss Servers) EnableRescueMode(cfg Config) error {
	for _, s := range ss {
		_, err := s.RobotNode().EnableRescueMode(cfg.Rescue.OS, cfg.Rescue.Arch)
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}

		s.log("rescue mode is set to enabled")
	}

	return nil
}

// DisableRescueMode disable the rescue mode of each node.
func (ss Servers) DisableRescueMode() error {
	for _, s := range ss {
		err := s.RobotNode().DisableRescueMode()
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}

		s.log("rescue mode is set to disabled")
	}

	return nil
}
