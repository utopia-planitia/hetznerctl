package hetznerctl

const (
	// renovate: datasource=github-tags depName=containernetworking/plugins
	CNI_VERSION = "v1.6.2"

	// renovate: datasource=github-tags depName=kubernetes/kubernetes
	KUBERNETES_VERSION = "v1.32.3"

	// renovate: datasource=github-tags depName=kubernetes/release
	RELEASE_VERSION = "v0.18.0"

	// renovate: datasource=github-tags depName=helm/helm
	HELM_VERSION = "v3.17.2"

	// renovate: datasource=github-tags depName=helmfile/helmfile
	HELMFILE_VERSION = "v0.171.0"

	// renovate: datasource=github-tags depName=kubernetes-sigs/kustomize
	KUSTOMIZE_VERSION = "5.6.0"

	// renovate: datasource=github-tags depName=vmware-tanzu/sonobuoy
	SONOBUOY_VERSION = "v0.57.3"

	// renovate: datasource=github-tags depName=kubernetes-sigs/cri-tools
	CRICTL_VERSION = "v1.32.0"

	// renovate: datasource=github-releases depName=vmware-tanzu/velero
	VELERO_VERSION = "v1.15.2"

	// renovate: datasource=github-tags depName=kubevirt/kubevirt
	VIRTCTL_VERSION = "v1.5.0"

	// renovate: datasource=github-tags depName=utopia-planitia/k8status
	K8STATUS_VERSION = "v0.2.2"
)
