package hetznerctl

import (
	"encoding/json"
	"fmt"
	"io"
	"log"

	"github.com/flatcar/container-linux-config-transpiler/config"
)

const (
	defaultFlatcarInstaller = "https://raw.githubusercontent.com/flatcar-linux/init/flatcar-master/bin/flatcar-install"
	ignitionTemplateFile    = "flatcar-config.yaml.tpl"
	signKeyFile             = "/tmp/sign.pub"
)

// SetupFlatcar installs flatcar os.
func (s Server) SetupFlatcar(cfg Config) error {
	err := s.RobotNode().DisableFirewall()
	if err != nil {
		return err
	}

	err = s.BootRescueMode(cfg)
	if err != nil {
		return err
	}

	err = s.deleteDisks(cfg)
	if err != nil {
		return err
	}

	err = s.InstallFlatcar(cfg)
	if err != nil {
		return err
	}

	err = s.RebootToSSH(RebootRescueSystem)
	if err != nil {
		return err
	}

	err = s.downloadTooling()
	if err != nil {
		return err
	}

	err = s.RebootToSSH(RebootSystem)
	if err != nil {
		return err
	}

	return nil
}

func (s Server) deleteDisks(cfg Config) error {
	err := s.ddDisk(cfg)
	if err != nil {
		return err
	}

	// reboot
	err = s.BootRescueMode(cfg)
	if err != nil {
		return err
	}

	err = s.truncateDisk(cfg)
	if err != nil {
		return err
	}

	return nil
}

func (s Server) ddDisk(cfg Config) error {
	// dd disks
	ssh, err := s.remoteRescueMode()
	if err != nil {
		return err
	}
	defer ssh.Close()

	err = ssh.DDDisk(cfg.Disks.OS)
	if err != nil {
		return fmt.Errorf("dd operating system disk: %v", err)
	}

	err = ssh.DDDisk(cfg.Disks.Data)
	if err != nil {
		return fmt.Errorf("dd storage data disk: %v", err)
	}

	return nil
}

func (s Server) truncateDisk(cfg Config) error {
	ssh, err := s.remoteRescueMode()
	if err != nil {
		return err
	}
	defer ssh.Close()

	err = ssh.DeleteDisk(cfg.Disks.OS)
	if err != nil {
		return fmt.Errorf("delete operating system disk: %v", err)
	}

	err = ssh.DeleteDisk(cfg.Disks.Data)
	if err != nil {
		return fmt.Errorf("delete storage data disk: %v", err)
	}

	return nil
}

// InstallFlatcar installs flatcar os on each node using the official installer.
func (ss Servers) InstallFlatcar(cfg Config) error {
	for _, s := range ss {
		err := s.InstallFlatcar(cfg)
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}
	}

	return nil
}

// InstallFlatcar executes the official flatcar os installer to install flatcar.
// The installer is downloaded and the rendered igition config is uploaded as a preparation.
func (s Server) InstallFlatcar(cfg Config) error {
	ssh, err := s.remoteRescueMode()
	if err != nil {
		return err
	}
	defer ssh.Close()

	flatcarInstaller := defaultFlatcarInstaller
	if cfg.Flatcar.InstallScriptUrl != "" {
		flatcarInstaller = cfg.Flatcar.InstallScriptUrl
	}
	s.log("download flatcar-install from %s", flatcarInstaller)
	err = ssh.Run(fmt.Sprintf("curl --fail --silent --show-error --location --max-time 120 --output flatcar-install %v", flatcarInstaller))
	if err != nil {
		return fmt.Errorf("download flatcar installer: %v", err)
	}

	s.log("install gawk")
	err = ssh.Run("apt install -y gawk")
	if err != nil {
		return fmt.Errorf("install flatcar installer dependency \"gawk\": %v", err)
	}

	s.log("prepare flatcar config")
	ignCfg, err := s.RenderIgnitionConfig(cfg)
	if err != nil {
		return fmt.Errorf("%v: %v", s.Name, err)
	}

	err = ssh.WriteFile(ignCfg, "config.ign", 0600)
	if err != nil {
		return fmt.Errorf("upload ignition config: %v", err)
	}

	s.log("download public sign key")
	if cfg.Flatcar.SignKey.URL != "" {
		err = ssh.Run(fmt.Sprintf("curl --fail --silent --show-error --location --max-time 120 --output %s %v", signKeyFile, cfg.Flatcar.SignKey.URL))
		if err != nil {
			return fmt.Errorf("download public sign key: %v", err)
		}

		sha256, err := ssh.Sha256(signKeyFile)
		if err != nil {
			return err
		}

		if sha256 != cfg.Flatcar.SignKey.SHA256 {
			return fmt.Errorf("sha256 of public sign key mismatch: found %s, expected %s", sha256, cfg.Flatcar.SignKey.SHA256)
		}
	}

	s.log("install flatcar")
	cmd := fmt.Sprintf("bash ./flatcar-install -i config.ign -d %s", cfg.Disks.OS)
	if cfg.Flatcar.BaseURL != "" {
		cmd = fmt.Sprintf("%s -b %s", cmd, cfg.Flatcar.BaseURL)
	}
	if cfg.Flatcar.Version != "" {
		cmd = fmt.Sprintf("%s -V %s", cmd, cfg.Flatcar.Version)
	}
	if cfg.Flatcar.SignKey.URL != "" {
		cmd = fmt.Sprintf("%s -k %s", cmd, signKeyFile)
	}

	err = ssh.Run(cmd)
	if err != nil {
		return fmt.Errorf("install flatcar: %v", err)
	}

	return nil
}

// RenderIgnitionConfig prints the ignition config for each node.
func (ss Servers) RenderIgnitionConfig(cfg Config, w io.Writer) error {
	for _, s := range ss {
		ignCfg, err := s.RenderIgnitionConfig(cfg)
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}

		_, err = w.Write(ignCfg)
		if err != nil {
			return err
		}
	}

	return nil
}

// RenderIgnitionConfig prints the ignition config.
func (s Server) RenderIgnitionConfig(cfg Config) ([]byte, error) {
	CLCfg, err := s.RenderContainerLinuxConfig(cfg)
	if err != nil {
		return []byte{}, fmt.Errorf("render contaner linux config: %v", err)
	}

	ignCfg, err := convert(CLCfg)
	if err != nil {
		return []byte{}, fmt.Errorf("convert container linux config to ignition config: %v", err)
	}

	return ignCfg, nil
}

func convert(in []byte) ([]byte, error) {
	cfg, ast, report := config.Parse(in)
	if report.IsFatal() {
		return []byte{}, fmt.Errorf("fatal: %v", report)
	}

	if report.IsDeprecated() {
		return []byte{}, fmt.Errorf("deprecated: %v", report)
	}

	if len(report.Entries) > 0 {
		return []byte{}, fmt.Errorf("reported: %v", report.String())
	}

	ign, report := config.Convert(cfg, "", ast)
	if report.IsFatal() {
		return []byte{}, fmt.Errorf("fatal: %v", report)
	}

	if report.IsDeprecated() {
		return []byte{}, fmt.Errorf("deprecated: %v", report)
	}

	if len(report.Entries) > 0 {
		return []byte{}, fmt.Errorf("reported: %v", report.String())
	}

	return json.Marshal(&ign)
}

// RenderContainerLinuxConfig prints the container linux (core os) config for each node.
func (ss Servers) RenderContainerLinuxConfig(cfg Config, w io.Writer) error {
	for _, s := range ss {
		ignCfg, err := s.RenderContainerLinuxConfig(cfg)
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}

		log.Println(string(ignCfg))
	}

	return nil
}

// ContainerLinuxConfig renders the container linux config template.
type ContainerLinuxConfig struct {
	Config
	Node Server
}

// RenderContainerLinuxConfig prints the container linux (core os) config.
func (s Server) RenderContainerLinuxConfig(cfg Config) ([]byte, error) {
	CLCfg := ContainerLinuxConfig{
		Config: cfg,
		Node:   s,
	}

	return renderAsset(ignitionTemplateFile, CLCfg)
}

// RenderContainerLinuxConfigTemplate prints the container linux (core os) config template.
func RenderContainerLinuxConfigTemplate(w io.Writer) error {
	yml, err := loadAsset(ignitionTemplateFile)
	if err != nil {
		return err
	}

	_, err = w.Write(yml)

	return err
}

// ConfigureFlatcar updates each node by rerunning ignition.
func (ss Servers) ConfigureFlatcar(cfg Config) error {
	for _, s := range ss {
		err := s.ConfigureFlatcar(cfg)
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}
	}

	return nil
}

// IgnitionConfigUpdate updates the ignition config, marks the config to be recompressed, and
// triggers a reboot via kured.
func (s Server) ConfigureFlatcar(cfg Config) error {
	err := s.downloadTooling()
	if err != nil {
		return err
	}

	ignCfg, err := s.RenderIgnitionConfig(cfg)
	if err != nil {
		return fmt.Errorf("%v: %v", s.Name, err)
	}

	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer ssh.Close()

	s.log("move old ignition config")

	err = ssh.Run("mv /usr/share/oem/config.ign /usr/share/oem/config.ign.org")
	if err != nil {
		return fmt.Errorf("move old ignition config: %v", err)
	}

	s.log("update ignition config")

	err = ssh.WriteFile(ignCfg, "/usr/share/oem/config.ign", 0600)
	if err != nil {
		return err
	}

	match, err := ssh.FilesMatch("/usr/share/oem/config.ign", "/usr/share/oem/config.ign.org")
	if err != nil {
		return fmt.Errorf("comparing configs: %v", err)
	}

	if match {
		s.log("ignition config did not change")
		return nil
	}

	s.log("active first_boot ignition re-configuration")

	err = ssh.Touch("/boot/flatcar/first_boot")
	if err != nil {
		return fmt.Errorf("active first boot state: %v", err)
	}

	err = s.Reboot(RebootKured)
	if err != nil {
		return fmt.Errorf("notify kured: %v", err)
	}

	return nil
}
