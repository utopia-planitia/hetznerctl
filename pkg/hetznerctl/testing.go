package hetznerctl

import (
	"bufio"
	"bytes"
	"fmt"
	"path/filepath"

	"al.essio.dev/pkg/shellescape"
	"gitlab.com/utopia-planitia/hetznerctl/pkg/remote"
)

func (s Server) TestServices(cfg Config, services []string, verbose, sync bool, reties int, filter string) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}

	dest, err := ssh.TempDir("hetznerctl-services")
	if err != nil {
		return err
	}
	defer func() {
		err := ssh.Remove(dest)
		if err != nil {
			s.log("delete %s: %v", dest, err)
		}
	}()

	for _, service := range services {
		s.log("upload service %s", service)

		err = ssh.UploadFolderViaTar(filepath.Join("services", service), filepath.Join(dest, service))
		if err != nil {
			return fmt.Errorf("upload service files to kubernetes master: %v", err)
		}
	}

	yml := bytes.Buffer{}
	err = PrintConfig(bufio.NewWriter(&yml), cfg)
	if err != nil {
		return fmt.Errorf("merge config with default values: %v", err)
	}

	err = ssh.WriteFile(yml.Bytes(), filepath.Join(dest, "cluster.yaml"), 0644)
	if err != nil {
		return fmt.Errorf("upload cluster configuration: %v", err)
	}

	allHelmfiles, err := listHelmfiles(services)
	if err != nil {
		return err
	}

	err = ssh.RunStdout("kubectl delete clusterrolebindings.rbac.authorization.k8s.io testing-cluster-admin --ignore-not-found=true --wait=true")
	if err != nil {
		return fmt.Errorf("cleanup previous clusterrolebinding: %v", err)
	}

	for _, helmfile := range allHelmfiles {
		if sync {
			s.log("sync %s", helmfile)

			err := applyRemoteHelmfile(ssh, dest, helmfile, []string{"phase=only-testing"}, verbose, reties)
			if err != nil {
				return err
			}
		}

		s.log("testing %s", helmfile)

		err := testRemoteHelmfile(ssh, dest, helmfile, []string{"phase=only-testing"}, verbose, filter)
		if err != nil {
			return err
		}

		s.log("cleanup tests %s", helmfile)

		err = destroyRemoteHelmfile(ssh, dest, helmfile, []string{"phase=only-testing"}, verbose, reties)
		if err != nil {
			return err
		}
	}

	return err
}

func testRemoteHelmfile(ssh remote.Remote, remoteServicesPath, helmfile string, selectors []string, verbose bool, filter string) error {
	selectorArgs := "--allow-no-matching-release "
	for _, selector := range selectors {
		selectorArgs += fmt.Sprintf("--selector %s ", shellescape.Quote(selector))
	}

	if filter != "" {
		filter = fmt.Sprintf("--filter=%s", filter)
	}

	cmdTemplate := "helmfile --state-values-file=%s --file=%s %s test --timeout=600 --args=\"--logs %s\""
	clusterYamlPath := filepath.Join(remoteServicesPath, "cluster.yaml")
	helmfilePath := filepath.Join(remoteServicesPath, helmfile)
	cmd := fmt.Sprintf(cmdTemplate, clusterYamlPath, helmfilePath, selectorArgs, filter)

	if verbose {
		err := ssh.RunStdout(cmd)
		if err != nil {
			return fmt.Errorf("test helmfile %s: %v", helmfile, err)
		}
	} else {
		err := ssh.Run(cmd)
		if err != nil {
			return fmt.Errorf("test helmfile %s: %v", helmfile, err)
		}
	}

	return nil
}
