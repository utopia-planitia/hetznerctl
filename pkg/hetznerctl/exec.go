package hetznerctl

import (
	"fmt"
	"log"
	"strings"
)

// Exec runs a command for each server.
func (ss Servers) Exec(args []string, abortOnFailure bool) error {
	var foundErr error

	for _, s := range ss {
		fmt.Printf("%s:\n", s.Name)

		err := s.Exec(args)
		if err == nil {
			continue
		}

		if abortOnFailure {
			return fmt.Errorf("%v: %v", s.Name, err)
		}

		log.Printf("%v: %v", s.Name, err)
		foundErr = err
	}

	return foundErr
}

// Exec runs a command on the remote server.
func (s Server) Exec(args []string) error {
	cmd := strings.TrimSpace(strings.Join(args, " "))
	if cmd == "" {
		return fmt.Errorf("command missing")
	}

	remote, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}

	err = remote.RunStdout(cmd)
	if err != nil {
		return err
	}

	return nil
}
