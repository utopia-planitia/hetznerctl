package hetznerctl

import (
	"fmt"

	"gitlab.com/utopia-planitia/hetznerctl/pkg/remote"
)

// PrepareMaintenance is a hook to begin maintenance.
func (s Server) PrepareMaintenance(c Config) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return fmt.Errorf("connect primary master: %v", err)
	}
	defer ssh.Close()

	return disableAutomaticActions(ssh)
}

// FinalizeMaintenance is a hook to end maintenance.
func (s Server) FinalizeMaintenance(c Config) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return fmt.Errorf("connect primary master: %v", err)
	}
	defer ssh.Close()

	return enableAutomaticActions(ssh)
}

func disableAutomaticActions(ssh remote.Remote) error {
	err := ssh.Run(`kubectl -n kube-system annotate --overwrite ds kured weave.works/kured-node-lock='{"nodeID":"manual"}'`)
	if err != nil {
		return fmt.Errorf("disable kured: %v", err)
	}

	return nil
}

func enableAutomaticActions(ssh remote.Remote) error {
	err := ssh.Run(`kubectl -n kube-system annotate ds kured weave.works/kured-node-lock-`)
	if err != nil {
		return fmt.Errorf("enable kured: %v", err)
	}

	return nil
}
