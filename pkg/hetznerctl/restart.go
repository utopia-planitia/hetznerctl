package hetznerctl

import (
	"fmt"
)

// RestartService restarts the given services on each node.
func (ss Servers) RestartService(svc string) error {
	for _, s := range ss {
		err := s.RestartService(svc)
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}
	}

	return nil
}

// RestartService restarts the given services by calling systemctl.
func (s Server) RestartService(svc string) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer ssh.Close()

	return ssh.RestartService(svc)
}
