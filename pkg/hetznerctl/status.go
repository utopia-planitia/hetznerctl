package hetznerctl

import (
	"fmt"
	"os"
	"strings"
)

// KuredStatus prints kured status for each server.
func (ss Servers) KuredStatus(sentinel string) (bool, error) {
	anyNeedsRestart := false

	for _, s := range ss {
		needsRestart, err := s.KuredStatus(sentinel)
		if err != nil {
			return false, fmt.Errorf("%v: %v", s.Name, err)
		}

		anyNeedsRestart = anyNeedsRestart || needsRestart
	}

	return anyNeedsRestart, nil
}

// KuredStatus prints kured status.
func (s Server) KuredStatus(sentinel string) (bool, error) {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return false, err
	}
	defer ssh.Close()

	err = ssh.Run(fmt.Sprintf("ls %s", sentinel))
	if err != nil {
		if strings.Contains(strings.TrimSpace(err.Error()), "No such file or directory") {
			s.log("up and running")

			return false, nil
		}

		return false, fmt.Errorf("looking up kured sentinel %s: %v", sentinel, err)
	}

	s.log("waiting for restart")

	return true, nil
}

// UptimeStatus prints the uptime for each server.
func (ss Servers) UptimeStatus() error {
	for _, s := range ss {
		err := s.UptimeStatus()
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}
	}

	return nil
}

// UptimeStatus prints the uptime.
func (s Server) UptimeStatus() error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer ssh.Close()

	err = ssh.RunStdout("uptime")
	if err != nil {
		return fmt.Errorf("request for uptime: %v", err)
	}

	return nil
}

// NodesStatus prints the node status known to kubernetes.
func (s Server) NodesStatus() error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer ssh.Close()

	err = ssh.RunStdout("kubectl get nodes -o wide")
	if err != nil {
		return fmt.Errorf("kubectl get nodes: %v", err)
	}

	return nil
}

// KubernetesStatus prints the status of kubernetes.
func (s Server) KubernetesStatus() error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer ssh.Close()

	err = ssh.Execute("status.sh", os.Stdin, os.Stdout, os.Stderr)
	if err != nil {
		return fmt.Errorf("status.sh: %v", err.Error())
	}

	return nil
}
