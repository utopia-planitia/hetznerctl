package hetznerctl

import "testing"

func Test_rewriteKubeConfig(t *testing.T) {
	type args struct {
		config   string
		username string
		cfg      Config
	}

	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "default user",
			args: args{
				config: `apiVersion: v1
clusters:
- cluster:
	certificate-authority-data: YmFzZTY0Cg==
    server: https://1.2.3.4:6443
  name: kubernetes
contexts:
- context:
	cluster: kubernetes
	user: test
  name: test@kubernetes
current-context: test@kubernetes
kind: Config
preferences: {}
users:
- name: test
  user:
	client-certificate-data: YmFzZTY0Cg==
	client-key-data: YmFzZTY0Cg==`,
				username: "test",
				cfg: Config{
					Cluster: struct {
						Name            string
						Domain          string
						Password        string
						BasicAuth       string "yaml:\"basic_auth\""
						MasterNodeCount int    `yaml:"master_node_count"`
						PodsPerNode     int    `yaml:"pods_per_node"`
					}{
						Name: "cluster-name",
					},
					Cloudflare: struct {
						Key        string
						Email      string
						Zone       string
						Subdomains struct {
							API  string
							HTTP string
						}
					}{
						Zone: "domain.tld",
						Subdomains: struct {
							API  string
							HTTP string
						}{
							API: "k8s",
						},
					},
				},
			},
			want: `apiVersion: v1
clusters:
- cluster:
	certificate-authority-data: YmFzZTY0Cg==
    server: https://k8s.domain.tld:6443
  name: cluster-name
contexts:
- context:
	cluster: cluster-name
	user: cluster-name.test
  name: test@cluster-name
current-context: test@cluster-name
kind: Config
preferences: {}
users:
- name: cluster-name.test
  user:
	client-certificate-data: YmFzZTY0Cg==
	client-key-data: YmFzZTY0Cg==`,
		},
		{
			name: "kubernetes admin",
			args: args{
				config: `apiVersion: v1
clusters:
- cluster:
	certificate-authority-data: YmFzZTY0Cg==
    server: https://k8s.domain.tld:6443
  name: kubernetes
contexts:
- context:
	cluster: kubernetes
	user: kubernetes-admin
  name: kubernetes-admin@kubernetes
current-context: kubernetes-admin@kubernetes
kind: Config
preferences: {}
users:
- name: kubernetes-admin
  user:
	client-certificate-data: YmFzZTY0Cg==
	client-key-data: YmFzZTY0Cg==`,
				username: "kubernetes-admin",
				cfg: Config{
					Cluster: struct {
						Name            string
						Domain          string
						Password        string
						BasicAuth       string "yaml:\"basic_auth\""
						MasterNodeCount int    `yaml:"master_node_count"`
						PodsPerNode     int    `yaml:"pods_per_node"`
					}{
						Name: "cluster-name",
					},
					Cloudflare: struct {
						Key        string
						Email      string
						Zone       string
						Subdomains struct {
							API  string
							HTTP string
						}
					}{
						Zone: "domain.tld",
						Subdomains: struct {
							API  string
							HTTP string
						}{
							API: "k8s",
						},
					},
				},
			},
			want: `apiVersion: v1
clusters:
- cluster:
	certificate-authority-data: YmFzZTY0Cg==
    server: https://k8s.domain.tld:6443
  name: cluster-name
contexts:
- context:
	cluster: cluster-name
	user: cluster-name.kubernetes-admin
  name: kubernetes-admin@cluster-name
current-context: kubernetes-admin@cluster-name
kind: Config
preferences: {}
users:
- name: cluster-name.kubernetes-admin
  user:
	client-certificate-data: YmFzZTY0Cg==
	client-key-data: YmFzZTY0Cg==`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := rewriteKubeConfig(tt.args.config, tt.args.username, "", tt.args.cfg); got != tt.want {
				t.Errorf("rewriteKubeConfig() = %v, want %v", got, tt.want)
			}
		})
	}
}
