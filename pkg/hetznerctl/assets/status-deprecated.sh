#!/usr/bin/env bash

EXIT_CODE=0

date

VERSION=$( kubectl version 2>&1 )
if [[ $? -ne 0 ]]; then
	# fail in case apiserver is not available
	echo "kubectl version failed"
	echo "${VERSION}"
	exit 1;
fi

# disabled because of
# https://github.com/kubernetes/kubeadm/issues/2222
# https://github.com/kubernetes/enhancements/issues/553
#CS=$( kubectl get componentstatus )
#CS_COUNT=$( echo "$CS" | tail -n +2 | wc -l )
#CS_READY=$( echo "$CS" | tail -n +2 | grep Healthy | wc -l )
#if [[ "${CS_COUNT}" = "${CS_READY}" ]]; then
#	echo "all master components are healthy"
#fi

NODES=$( kubectl get no -o wide 2>&1 | grep -v 'No resources found.' )
NODES_COUNT=$( echo "$NODES" | tail -n +2 | wc -l )
NODES_READY=$( echo "$NODES" | tail -n +2 | grep ' Ready ' | wc -l )
echo "${NODES_READY} of ${NODES_COUNT} nodes are ready"

if kubectl get namespaces cassandra >/dev/null 2>&1; then
	if kubectl -n cassandra get secret k8ssandra-superuser >/dev/null 2>&1; then
		CASSANDRA_SUPER_USERNAME=$( timeout 2 kubectl -n cassandra get secret k8ssandra-superuser -o jsonpath="{.data.username}" | base64 --decode )
		CASSANDRA_SUPER_PASSWORD=$( timeout 2 kubectl -n cassandra get secret k8ssandra-superuser -o jsonpath="{.data.password}" | base64 --decode )
		CASSANDRA_NODES=$( timeout 2 kubectl --namespace cassandra exec statefulset/k8ssandra-dc1-default-sts --container cassandra -- nodetool -u ${CASSANDRA_SUPER_USERNAME} -pw ${CASSANDRA_SUPER_PASSWORD} --host ::FFFF:127.0.0.1 status | grep --extended-regexp '^[UD][NLJM]\s+' )
	else
		CASSANDRA_NODES=$( timeout 2 kubectl --namespace cassandra exec statefulset/cassandra --container cassandra -- nodetool --host ::FFFF:127.0.0.1 status | grep --extended-regexp '^[UD][NLJM]\s+' )
	fi
	if [[ "${CASSANDRA_NODES:-}" != "" ]]; then
		CASSANDRA_COUNT=$( echo "${CASSANDRA_NODES:-}" | wc --lines )
		CASSANDRA_READY=$( echo "${CASSANDRA_NODES:-}" | grep --count --extended-regexp '^UN' )
	fi
	echo "${CASSANDRA_READY:-?} of ${CASSANDRA_COUNT:-?} cassandra nodes are ready"
fi

if kubectl get ns rook-ceph >/dev/null 2>&1; then
	CEPH_STATUS=$( timeout 2 kubectl -n rook-ceph exec deployment/rook-ceph-tools -- ceph status )
    if [[ "${CEPH_STATUS}" = "" ]]; then
		CEPH_STATUS="Unknown"
    fi

	CEPH_HEALTH=$( echo "$CEPH_STATUS" | grep "health" | xargs )
	CEPH_HEALTH_PG=$( echo "$CEPH_STATUS" | grep "objects misplaced" | xargs )
	echo "ceph: ${CEPH_HEALTH} ${CEPH_HEALTH_PG}"
fi

VOLUMES=$( kubectl get pv 2>&1 | grep -v 'No resources found.' )
VOLUMES_COUNT=$( echo "$VOLUMES" | tail -n +2 | wc -l )
VOLUMES_BOUND_AVAILABLE=$( echo "$VOLUMES" | tail -n +2 | grep -e Bound -e Available | wc -l )
echo "${VOLUMES_BOUND_AVAILABLE} of ${VOLUMES_COUNT} volumes are bound or available"

NAMESPACES=$( kubectl get ns )
NAMESPACES_COUNT=$( echo "$NAMESPACES" | tail -n +2 | wc -l )
NAMESPACES_ACTIVE=$( echo "$NAMESPACES" | tail -n +2 | grep Active | wc -l )
echo "${NAMESPACES_ACTIVE} of ${NAMESPACES_COUNT} namespaces are active"

CLAIMS=$( kubectl get --all-namespaces=true pvc 2>&1 | grep -v 'No resources found.' )
CLAIMS_COUNT=$( echo "$CLAIMS" | tail -n +2 | wc -l )
CLAIMS_BOUND_AVAILABLE=$( echo "$CLAIMS" | tail -n +2 | grep -e Bound | wc -l )
echo "${CLAIMS_BOUND_AVAILABLE} of ${CLAIMS_COUNT} volume claims are bound"

PODS=$( kubectl get po --all-namespaces=true -o wide 2>&1 | grep -v 'No resources found.' )
PODS_COUNT=$( echo "$PODS" | tail -n +2 | wc -l )
PODS_NOT_READY_WITH_HEADER=$( echo "$PODS" | grep -v Completed | grep -Ev "\b([0-9][0-9]*)\b/\1" )
PODS_NOT_READY=$( echo "$PODS_NOT_READY_WITH_HEADER" | tail -n +2 )
PODS_NOT_READY_COUNT=$( echo -n "$PODS_NOT_READY" | wc -l )
PODS_READY=$( expr ${PODS_COUNT} - ${PODS_NOT_READY_COUNT} )
echo "${PODS_READY} of ${PODS_COUNT} pods are running/completed"

ESSENTIAL_NOT_READY_PODS=$( echo "${PODS_NOT_READY}" | awk '{ print $1 }' \
	| grep -v "\-ci$" | grep -v "\-ci-" | grep -v "^ci-" \
	| grep -v "\-lab$" | grep -v "\-lab-" | grep -v "^lab-" \
	)
ESSENTIAL_NOT_READY_PODS_COUNT=$( echo -n "$ESSENTIAL_NOT_READY_PODS" | wc -l )

#if [[ ! "${CS_COUNT}" = "${CS_READY}" ]]; then
#	echo "master components:"
#	echo "$CS" | head -n 1
#	echo "$CS" | tail -n +2 | sort
#	EXIT_CODE=40
#fi

echo ""

if [[ ! "${NODES_READY}" = "${NODES_COUNT}" ]]; then
	echo "nodes:"
	echo "$NODES" | grep -v ' Ready '
	EXIT_CODE=41
fi

if [[ ! "${CASSANDRA_COUNT}" = "${CASSANDRA_READY}" ]]; then
	echo "cassandra nodes:"
	echo "$CASSANDRA_NODES" | tail -n +6 | grep -v "UN "
	EXIT_CODE=46
fi

if [[ ! "${CEPH_HEALTH}" = "" ]] && [[ ! "${CEPH_HEALTH}" = "health: HEALTH_OK" ]]; then
	echo "ceph health:"
	echo "$CEPH_HEALTH"
	EXIT_CODE=47
fi

if [[ ! "${CEPH_HEALTH_PG}" = "" ]]; then
	echo "ceph placement groups:"
	echo "$CEPH_HEALTH_PG"
	EXIT_CODE=48
fi

if [[ ! "${VOLUMES_BOUND_AVAILABLE}" = "${VOLUMES_COUNT}" ]]; then
	echo "volumes:"
	echo "$VOLUMES" | grep -v -e Bound -e Available \
		| sed 's/ACCESS MODES/ACCESS_MODES/g' | sed 's/RECLAIM POLICY/RECLAIM_POLICY/g' \
		| column -t
	EXIT_CODE=42
fi

if [[ ! "${NAMESPACES_ACTIVE}" = "${NAMESPACES_COUNT}" ]]; then
	echo "namespaces:"
	echo "$NAMESPACES" | grep -v Active
	EXIT_CODE=43
fi

if [[ ! "${CLAIMS_BOUND_AVAILABLE}" = "${CLAIMS_COUNT}" ]]; then
	echo "volume claims:"
	echo "$CLAIMS" | grep -v -e Bound | sed 's/ACCESS MODES/ACCESS_MODES/g' | column -t
	EXIT_CODE=44
fi

if [[ ! "${PODS_COUNT}" = "${PODS_READY}" ]]; then
	echo "pods:"
	echo "$PODS_NOT_READY_WITH_HEADER" \
	    | sed 's/NOMINATED NODE/NOMINATED_NODE/g' | sed 's/READINESS GATES/READINESS_GATES/g' \
		| column -t
fi

if [[ "${ESSENTIAL_NOT_READY_PODS_COUNT}" != "0" ]]; then
	EXIT_CODE=45
fi

exit $EXIT_CODE
