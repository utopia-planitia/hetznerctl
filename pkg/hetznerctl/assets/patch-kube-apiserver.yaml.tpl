apiVersion: v1
kind: Pod
metadata:
  name: kube-apiserver
  namespace: kube-system
spec:
  containers:
    - name: kube-apiserver
      resources:
        requests:
          cpu: {{ .APIServer.Request.CPU }}
          memory: {{ .APIServer.Request.Memory }}
        limits:
          cpu: {{ .APIServer.Limit.CPU }}
          memory: {{ .APIServer.Limit.Memory }}
