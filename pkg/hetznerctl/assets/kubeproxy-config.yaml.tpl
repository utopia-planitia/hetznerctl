# https://godoc.org/k8s.io/kube-proxy/config/v1alpha1#KubeProxyIPVSConfiguration
apiVersion: kubeproxy.config.k8s.io/v1alpha1
kind: KubeProxyConfiguration
mode: iptables
metricsBindAddress: 0.0.0.0:10249
