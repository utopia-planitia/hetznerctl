# https://godoc.org/k8s.io/kubernetes/cmd/kubeadm/app/apis/kubeadm/v1beta3#ClusterConfiguration
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
kubernetesVersion: {{ .KubernetesVersion }}
controlPlaneEndpoint: {{ .APIDomain }}:6443
apiServer:
  extraArgs:
    enable-admission-plugins: AlwaysPullImages
    etcd-servers: "{{ range $index, $node := .Nodes.SelectMasters }}{{ if $index }},{{ end }}https://{{ $node.Network.PublicIP }}:2379{{ end }}"
controllerManager:
  extraArgs:
    bind-address: 0.0.0.0
    flex-volume-plugin-dir: "/opt/libexec/kubernetes/kubelet-plugins/volume/exec/"
scheduler:
  extraArgs:
    bind-address: 0.0.0.0
etcd:
  local:
    extraArgs:
      listen-metrics-urls: http://0.0.0.0:2381
      listen-client-http-urls: https://0.0.0.0:2382
      # https://etcd.io/docs/v3.3.12/op-guide/configuration/
      snapshot-count:            "5000" # from 100000
      heartbeat-interval:          "50" # from 100
      election-timeout:          "2000" # from 1000
      auto-compaction-retention:   "1h" # from 0=infinity
networking:
  serviceSubnet: "10.96.0.0/12"
  podSubnet: "10.244.0.0/16"
  dnsDomain: "cluster.local"
