storage:
  filesystems:
    - name: "OEM"
      mount:
        device: "/dev/disk/by-label/OEM"
        format: "btrfs"
  files:
    - filesystem: "OEM"
      path: "/grub.cfg"
      mode: 0644
      contents:
        inline: |
          set linux_append="$linux_append systemd.unified_cgroup_hierarchy=0 systemd.legacy_systemd_cgroup_controller"

    - filesystem: "root"
      path:       "/etc/hostname"
      mode:       0644
      contents:
        inline: {{ .Node.Name }}

    - filesystem: root
      path: /etc/hosts
      mode: 0644
      append: true
      contents:
        inline: |
          127.0.0.1 {{ .Node.Name }}
          {{ if .DockerHub.DisableViaDns }}
          0.0.0.0 index.docker.io auth.docker.io registry-1.docker.io dseasb33srnrn.cloudfront.net production.cloudflare.docker.com
          {{ end }}

    # force the br_netfilter module to be loaded early during boot
    # because without it sysctl cannot create the /proc/sys/net/bridge/bridge-nf-call-iptables file
    - filesystem: root
      path: /etc/modules-load.d/br_netfilter.conf
      mode: 0644
      contents:
        inline: |
          br_netfilter

    - filesystem: root
      path: /etc/modules-load.d/binfmt_misc.conf
      mode: 0644
      contents:
        inline: |
          binfmt_misc

    - filesystem: root
      path: /etc/modules-load.d/kvm.conf
      mode: 0644
      contents:
        inline: |
          kvm
          tun
          tap
          vhost_net

    # disable halt_poll_ns to reduce CPU consumption on the host system
    # when the guest OS of the virtual machine is idle
    # https://www.ibm.com/docs/en/linux-on-systems?topic=tuning-kvm-settings
    # https://www.kernel.org/doc/Documentation/virtual/kvm/halt-polling.txt
    - filesystem: root
      path: /etc/modprobe.d/kvm.conf
      mode: 0644
      contents:
        inline: |
          options kvm halt_poll_ns=0

    - filesystem: "root"
      path:       "/root/.bashrc"
      mode:       0644
      contents:
        inline: |
          export PATH=/opt/bin/:$PATH

          # Bash completions
          source /opt/bin/bash_completion
          if command -v crictl >/dev/null;    then source <(crictl completion bash);    fi
          if command -v helm >/dev/null;      then source <(helm completion bash);      fi
          if command -v kubeadm >/dev/null;   then source <(kubeadm completion bash);   fi
          if command -v kubectl >/dev/null;   then source <(kubectl completion bash);   fi
          if command -v kustomize >/dev/null; then source <(kustomize completion bash); fi
          if command -v velero >/dev/null;    then source <(velero completion bash);    fi
          if command -v virtctl >/dev/null;   then source <(virtctl completion bash);   fi

          # Bash aliases
          if ! command -v ceph >/dev/null && command -v kubectl >/dev/null; then
            alias ceph='kubectl --namespace rook-ceph exec --stdin --tty deployments/rook-ceph-tools -- ceph "${@}"'
          fi

    - filesystem: "root"
      path:       "/opt/bin/bash_completion"
      mode:       0644
      contents:
        remote:
          url: https://raw.githubusercontent.com/scop/bash-completion/2.10/bash_completion

    # https://wiki.archlinux.org/index.php/systemd-timesyncd
    # https://stackoverflow.com/a/21305933/1691005
    - filesystem: "root"
      path: "/etc/systemd/timesyncd.conf"
      mode: 0644
      contents:
        inline: |
          [Time]
          NTP={{ range $index, $element := .NTPServers}}{{if $index}} {{end}}{{$element}}{{end}}
          # PollIntervalMinSec defaults to 32 seconds and must not be smaller than 16 seconds
          PollIntervalMinSec=16
          # PollIntervalMaxSec defaults to 2048 seconds (34 min 8 s) and must be larger than PollIntervalMinSec
          PollIntervalMaxSec=256	

    - filesystem: "root"
      path: "/etc/sysctl.d/20-strict-kernel.conf"
      mode:       0644
      contents:
        inline: |
          # https://github.com/kubernetes/kubernetes/issues/66241#issuecomment-460832038
          vm.overcommit_memory=1
          kernel.panic=10
          kernel.panic_on_oops=1

          # increased due to https://github.com/kubernetes-sigs/kind/issues/717
          # see https://forum.proxmox.com/threads/failed-to-allocate-directory-watch-too-many-open-files.28700/
          # see https://lxd.readthedocs.io/en/latest/production-setup/
          # see https://docs.sonarqube.org/latest/requirements/requirements/
          fs.inotify.max_queued_events=262144
          fs.inotify.max_user_instances=2048
          fs.inotify.max_user_watches=1048576
          fs.file-max=1048576
          fs.nr_open=2097152

          # https://github.com/SonarSource/docker-sonarqube/issues/282
          vm.max_map_count=262144

          # https://kubernetes.io/docs/setup/production-environment/container-runtimes/#containerd
          net.ipv4.ip_forward = 1
          net.bridge.bridge-nf-call-ip6tables = 1
          net.bridge.bridge-nf-call-iptables = 1

    # see https://lxd.readthedocs.io/en/latest/production-setup/
    # see https://docs.sonarqube.org/latest/requirements/requirements/
    - filesystem: "root"
      path: "/etc/security/limits.conf"
      mode:       0644
      contents:
        inline: |
          *    soft nofile     262144
          *    hard nofile     262144
          root soft nofile     262144
          root hard nofile     262144
          *    soft memlock unlimited
          *    hard memlock unlimited

    # https://man7.org/linux/man-pages/man5/networkd.conf.5.html
    # https://manpages.debian.org/testing/systemd/networkd.conf.5.en.html
    # can be tested with bash command: `systemd-analyze cat-config systemd/networkd.conf`
    - filesystem: "root"
      path: "/etc/systemd/networkd.conf.d/enable-foreign-routing-for-gateway-and-kube-router.conf"
      mode:       0644
      contents:
        inline: |
          # ~ utopia-planitia custom change ~
          #
          # needed to be re-enabled bec. flatcar changed default to "no" in bugfix
          # see: https://www.flatcar.org/releases/#release-3033.2.3
          # see: https://github.com/flatcar/Flatcar/issues/620
          #
          # affects:
          # - default ipv4 gateway (e.g. leads to no ssh)
          # - cni behavior #kube-router #kuberouter #kube_router
          [Network]
          ManageForeignRoutes=yes
          ManageForeignRoutingPolicyRules=yes

    # https://wiki.hetzner.de/index.php/Network_configuration_using_systemd-networkd/en#Dedicated_servers
    - filesystem: "root"
      path: "/etc/systemd/network/static.network"
      mode:       0644
      contents:
        inline: |
          [Match]
          Name={{ .Node.Network.DeviceName }}
          [Network]
          Gateway={{ .Node.Network.Gateway }}
          Gateway=fe80::1
          DNS={{ range $index, $element := .DNSServers}}{{if $index}} {{end}}{{$element}}{{end}}
          VLAN=vswitch.{{ .VSwitch.ID }}
          Address={{ .Node.Network.IPv6 }}
          [Address]
          Address={{ .Node.Network.PublicIP }}
          Peer={{ .Node.Network.Gateway }}/32

    # https://wiki.hetzner.de/index.php/Vswitch#Beispielkonfiguration_f.C3.BCr_systemd
    - filesystem: "root"
      path: "/etc/systemd/network/vswitch.{{ .VSwitch.ID }}.netdev"
      mode: 0644
      contents:
        inline: |
          [NetDev]
          Name=vswitch.{{ .VSwitch.ID }}
          Kind=vlan
          MTUBytes=1400
          [VLAN]
          Id={{ .VSwitch.ID }}

    - filesystem: "root"
      path: "/etc/systemd/network/vswitch.{{ .VSwitch.ID }}.network"
      mode: 0644
      contents:
        inline: |
          [Match]
          Name=vswitch.{{ .VSwitch.ID }}
          [Network]
          Description="VLAN {{ .VSwitch.ID }}"
          Address={{ .Node.Network.PrivateIP }}/24

    # ip6tables -nvL
    # curl -v http://[2600::] (www.sprint.net)
    # https://www.jimmycuadra.com/posts/securing-coreos-with-iptables/
    # https://www.linux.com/tutorials/iptables-rules-ipv6/
    - filesystem: "root"
      path: "/var/lib/ip6tables/rules-save"
      mode: 0644
      contents:
        inline: |
          *filter
          :INPUT DROP [0:0]
          :FORWARD DROP [0:0]
          :OUTPUT ACCEPT [0:0]
          -A INPUT -i lo -j ACCEPT
          -A INPUT -i {{ .Node.Network.DeviceName }} -p udp --dport 67:68 --sport 67:68 -j ACCEPT
          -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
          -A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
          -A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
          -A INPUT -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT
          -A INPUT -p tcp -m state --state NEW -m tcp --dport 6443 -j ACCEPT
          -A INPUT -p ipv6-icmp -j ACCEPT
          COMMIT
          # comment

    - filesystem: "root"
      path: /var/lib/iptables/rules-save
      mode: 0644
      contents:
        inline: |
          *filter
          :INPUT DROP [0:0]
          :FORWARD DROP [0:0]
          :OUTPUT ACCEPT [0:0]
          -A INPUT -i lo -j ACCEPT
          -A INPUT -i {{ .Node.Network.DeviceName }} -p udp --dport 67:68 --sport 67:68 -j ACCEPT
          -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
          -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
          -A INPUT -p tcp -m tcp --dport 2222 -j ACCEPT
          -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
          -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT
          -A INPUT -p tcp -m tcp --dport 6443 -j ACCEPT
          -A INPUT -p tcp -m tcp --dport 2379:2380 -j ACCEPT
          -A INPUT -p icmp -m icmp --icmp-type 0 -j ACCEPT
          -A INPUT -p icmp -m icmp --icmp-type 3 -j ACCEPT
          -A INPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT
          -A INPUT -p icmp -m icmp --icmp-type 11 -j ACCEPT
          -A INPUT -p tcp --dport 32768:65535 -j ACCEPT
          -A INPUT -p udp --dport 32768:65535 -j ACCEPT
          -A INPUT -p tcp --dport 30000:32767 -j ACCEPT
          -A INPUT -p udp --dport 30000:32767 -j ACCEPT
          -A INPUT -s 10.0.0.0/8 -j ACCEPT
          -A INPUT -s 172.16.0.0/12 -j ACCEPT
          -A INPUT -s 192.168.0.0/16 -j ACCEPT
          COMMIT
          # comment

    - filesystem: "root"
      path: /etc/containerd/config.toml
      mode: 0644
      contents:
        inline: |
          disabled_plugins = []
          oom_score = -999
          root = "/var/lib/containerd"
          state = "/run/containerd"
          version = 2
          subreaper = true

          [grpc]
          address = "/run/containerd/containerd.sock"
          uid = 0
          gid = 0

          [plugins."io.containerd.runtime.v1.linux"]
          shim = "containerd-shim"
          runtime = "runc"
          no_shim = false

          [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc]
          runtime_type = "io.containerd.runc.v2"
          [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
          SystemdCgroup = true

          [plugins."io.containerd.grpc.v1.cri".registry]
            config_path = "/etc/containerd/certs.d"

    {{- $servicesRegistry := .Services.Registry }}
    {{- range (list "registry.registry.svc" "registry.registry.svc:5000")}}
    - filesystem: "root"
      path: /etc/containerd/certs.d/{{ . }}/hosts.toml
      mode: 0644
      # https://github.com/containerd/containerd/blob/main/docs/hosts.md#bypass-tls-verification-example
      contents:
        inline: |
          [host."http://{{ $servicesRegistry.ServiceIP }}:5000"]
            capabilities = ["pull", "resolve", "push"]
            skip_verify = true
          [host."http://{{ $servicesRegistry.ServiceIP }}"]
            capabilities = ["pull", "resolve", "push"]
            skip_verify = true
    {{- end }}

    {{- range .RegistryMirrors }}
    # Flatcar does not boot if the same file is created multiple times using the flatcar-config.yaml.
    # So we are skipping "docker.io" for now because it will be created further below.
    {{- if eq .Registry "docker.io" }}{{ continue }}{{ end }}
    - filesystem: "root"
      path: /etc/containerd/certs.d/{{ .Registry }}/hosts.toml
      mode: 0644
      contents:
        inline: |
          [host."https://{{ .Host }}{{ with .PathWithV2 }}{{ . }}{{ else }}/{{ end }}"]
            {{- if .PathWithV2 }}
            override_path = true
            {{- end }}
            {{- if .Password }}
            capabilities = ["pull", "resolve"]
            [host."https://{{ .Host }}{{ with .PathWithV2 }}{{ . }}{{ else }}/{{ end }}".header]
              Authorization = "Basic {{ printf "%s:%s" .Username .Password | b64enc }}"
            {{- else }}
            capabilities = ["pull"]
            {{- end }}
    {{- end }}

    - filesystem: "root"
      path: /etc/containerd/certs.d/docker.io/hosts.toml
      mode: 0644
      contents:
        inline: |
        {{- if .RegistryMirrors }}
          {{- range .RegistryMirrors }}
          {{- if ne .Registry "docker.io" }}{{ continue }}{{ end }}
          # .RegistryMirrors
          [host."https://{{ .Host }}{{ with .PathWithV2 }}{{ . }}{{ else }}/{{ end }}"]
            {{- if .PathWithV2 }}
            override_path = true
            {{- end }}
            {{- if and .Username .Password }}
            capabilities = ["pull", "resolve"]
            [host."https://{{ .Host }}{{ with .PathWithV2 }}{{ . }}{{ else }}/{{ end }}".header]
              Authorization = "Basic {{ printf "%s:%s" .Username .Password | b64enc }}"
            {{- else }}
            capabilities = ["pull"]
            {{- end }}
          {{- end }}
        {{- else if .RegistryMirrorV2 }}
          {{- with .RegistryMirrorV2 }}
          # .RegistryMirrorV2
          [host."https://{{ .Host }}{{ with .PathWithV2 }}{{ . }}{{ else }}/{{ end }}"]
            {{- if .PathWithV2 }}
            override_path = true
            {{- end }}
            {{- if and .Username .Password }}
            capabilities = ["pull", "resolve"]
            [host."https://{{ .Host }}{{ with .PathWithV2 }}{{ . }}{{ else }}/{{ end }}".header]
              Authorization = "Basic {{ printf "%s:%s" .Username .Password | b64enc }}"
            {{- else }}
            capabilities = ["pull"]
            {{- end }}
          {{- end }}
        {{- else if .RegistryMirror }}
          {{- with .RegistryMirror }}
          # .RegistryMirror
          [host."https://{{ .Endpoint }}"]
            {{- if and .Username .Password }}
            capabilities = ["pull", "resolve"]
            [host."https://{{ .Host }}{{ with .PathWithV2 }}{{ . }}{{ else }}/{{ end }}".header]
              Authorization = "Basic {{ printf "%s:%s" .Username .Password | b64enc }}"
            {{- else }}
            capabilities = ["pull"]
            {{- end }}
          {{- end }}
        {{- end }}
          {{- with .DockerHub }}
          # .DockerHub
          [host."https://registry-1.docker.io"]
            capabilities = ["pull", "resolve"]
            {{- if and .Username .Password }}
            [host."https://registry-1.docker.io".header]
              Authorization = "Basic {{ printf "%s:%s" .Username .Password | b64enc }}"
            {{- end }}
          {{- end }}

    # https://coreos.com/os/docs/latest/update-strategies.html
    - filesystem: "root"
      path: /etc/flatcar/update.conf
      mode: 0644
      contents:
        inline: |
          REBOOT_STRATEGY=off
          {{ if .Flatcar.DisableUpdates }}
          SERVER=disabled
          {{ else if ne .Flatcar.UpdateURL "" }}
          SERVER={{ .Flatcar.UpdateURL }}
          {{ end }}
          {{ if ne .Flatcar.AppID "" }}
          FLATCAR_RELEASE_APPID={{ .Flatcar.AppID }}
          {{ end }}
          {{ if ne .Flatcar.Group "" }}
          GROUP={{ .Flatcar.Group }}
          {{ end }}

systemd:
  units:
    - name: ip6tables-restore.service
      enable: true

    - name: iptables-restore.service
      enable: true

    - name: containerd.service
      enable: true
      contents: |
        [Unit]
        Description=containerd container runtime
        After=network.target

        [Service]
        Delegate=yes
        ExecStartPre=mkdir -p /run/docker/libcontainerd
        ExecStartPre=ln -fs /run/containerd/containerd.sock /run/docker/libcontainerd/docker-containerd.sock
        ExecStart=containerd --config /etc/containerd/config.toml
        KillMode=process
        Type=notify
        Restart=always
        RestartSec=5

        # (lack of) limits from the upstream docker service unit
        LimitNOFILE=1048576
        LimitNPROC=infinity
        LimitCORE=infinity
        TasksMax=infinity

        [Install]
        WantedBy=multi-user.target

    - name: enable-ksm.service
      enable: true
      contents: |
        [Unit]
        Description=Enable KSM config
        Before=network-pre.target
        Wants=network-pre.target

        [Service]
        Type=oneshot
        ExecStart=echo 1 > /sys/kernel/mm/ksm/run

        [Install]
        WantedBy=basic.target

    - name: update-ssh-keys-after-ignition.service
      enable: false

# https://coreos.com/os/docs/latest/migrating-to-clcs.html#users
passwd:
  users:
    - name: core
      ssh_authorized_keys:
      {{- range .SSHAuthorizedKeys }}
        - {{ . }}
      {{- end }}
