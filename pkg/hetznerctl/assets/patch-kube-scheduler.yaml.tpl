apiVersion: v1
kind: Pod
metadata:
  name: kube-scheduler
  namespace: kube-system
spec:
  containers:
    - name: kube-scheduler
      resources:
        requests:
          cpu: {{ .Scheduler.Request.CPU }}
          memory: {{ .Scheduler.Request.Memory }}
        limits:
          cpu: {{ .Scheduler.Limit.CPU }}
          memory: {{ .Scheduler.Limit.Memory }}
