# https://godoc.org/k8s.io/kubelet/config/v1beta1#KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cpuCFSQuota: false
maxPods: {{ .Cluster.PodsPerNode }}
serializeImagePulls: false
registryPullQPS: 10
registryBurst: 20
protectKernelDefaults: true
evictionHard:
  memory.available: "2Gi"
  nodefs.available: "15%"
  nodefs.inodesFree: "10%"
  imagefs.available: "15%"
  imagefs.inodesFree: "10%"
evictionSoft:
  memory.available: "4Gi"
  nodefs.available: "31%"
  nodefs.inodesFree: "20%"
  imagefs.available: "31%"
  imagefs.inodesFree: "20%"
evictionSoftGracePeriod:
  memory.available: "2m30s"
  nodefs.available: "2m30s"
  nodefs.inodesFree: "2m30s"
  imagefs.available: "2m30s"
  imagefs.inodesFree: "2m30s"
systemReserved:
  cpu: "500m"
  memory: "4Gi"
kubeReserved:
  cpu: "500m"
  memory: "4Gi"
containerRuntimeEndpoint: unix:///run/containerd/containerd.sock
volumePluginDir: /opt/libexec/kubernetes/kubelet-plugins/volume/exec/
