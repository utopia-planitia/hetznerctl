apiVersion: v1
kind: Pod
metadata:
  name: etcd
  namespace: kube-system
spec:
  shareProcessNamespace: true
  containers:
    - name: etcd
      resources:
        requests:
          cpu: {{ .Etcd.Request.CPU }}
          ephemeral-storage: 0Mi
          memory: {{ .Etcd.Request.Memory }}
        limits:
          cpu: {{ .Etcd.Limit.CPU }}
          memory: {{ .Etcd.Limit.Memory }}
      env:
        - name: ETCDCTL_API
          value: "3"
        - name: ETCDCTL_ENDPOINTS
          value: https://127.0.0.1:2379
        - name: ETCDCTL_CERT
          value: /etc/kubernetes/pki/etcd/healthcheck-client.crt
        - name: ETCDCTL_KEY
          value: /etc/kubernetes/pki/etcd/healthcheck-client.key
        - name: ETCDCTL_CACERT
          value: /etc/kubernetes/pki/etcd/ca.crt
    - name: ionice
      image: docker.io/library/alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c
      command:
        - sh
        - -c
        - |
          set -euxo pipefail;
          while true; do
            for pid in $(pidof etcd); do
              ionice -c 1 -n 7 -p $pid;
              renice -n -20 -p $pid;
            done;
            sleep 60;
          done
      securityContext:
        capabilities:
          add: ["SYS_ADMIN", "SYS_NICE"]
      resources:
        requests:
          cpu: 10m
          memory: 30Mi
        limits:
          cpu: 10m
          memory: 30Mi
