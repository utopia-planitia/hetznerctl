apiVersion: kubeadm.k8s.io/v1beta3
kind: JoinConfiguration
nodeRegistration:
  taints: []
discovery:
  bootstrapToken:
    apiServerEndpoint: {{ .APIDomain }}:6443
    caCertHashes:
    - sha256:{{ .CaCertHashes }}
    token: {{ .Token }}
{{ if .CertificateKey }}
controlPlane:
  certificateKey: {{ .CertificateKey }}
{{ end }}
patches:
  directory: /root/kubeadm/
