apiVersion: v1
kind: Pod
metadata:
  name: kube-controller-manager
  namespace: kube-system
spec:
  containers:
    - name: kube-controller-manager
      resources:
        requests:
          cpu: {{ .ControllerManager.Request.CPU }}
          memory: {{ .ControllerManager.Request.Memory }}
        limits:
          cpu: {{ .ControllerManager.Limit.CPU }}
          memory: {{ .ControllerManager.Limit.Memory }}
