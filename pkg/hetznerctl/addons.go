package hetznerctl

// AwaitDNS deploys cilium connectivity check.
func (s Server) AwaitDNS(domain string) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer ssh.Close()

	return ssh.AwaitDNS(domain)
}
