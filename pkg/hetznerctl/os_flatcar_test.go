package hetznerctl

import (
	"os"
	"reflect"
	"testing"
)

func TestConvert(t *testing.T) {
	mustReadFile := func(filename string) []byte {
		b, err := os.ReadFile(filename)
		if err != nil {
			panic(err)
		}

		return b
	}

	tests := []struct {
		name    string
		arg     []byte
		want    []byte
		wantErr bool
	}{
		{
			name:    "default config creates a useful result",
			arg:     mustReadFile("testdata/default.conf"),
			want:    mustReadFile("testdata/default.golden"),
			wantErr: false,
		},
		{
			name:    "config can not be empty",
			arg:     []byte{},
			want:    []byte{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := convert(tt.arg)
			if (err != nil) != tt.wantErr {
				t.Errorf("Convert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Convert() = %v, want %v", string(got), string(tt.want))
			}
		})
	}
}
