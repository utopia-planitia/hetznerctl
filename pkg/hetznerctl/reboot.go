package hetznerctl

import (
	"fmt"

	"gitlab.com/utopia-planitia/hetznerctl/pkg/robot"
)

// Reboot defines who to reboot a server.
type Reboot string

const (
	// RebootKured reboots the node via kured.
	RebootKured = Reboot("kured")
	// RebootKured reboots the node via kured directly.
	RebootKuredRealTime = Reboot("kured-real-time")
	// RebootHardware reboots the node via hetzner robot API resetting the hardware.
	RebootHardware = Reboot(robot.RebootHardware)
	// RebootHardwareUngraceful reboots the node via hetzner robot API resetting the hardware without syncing the disk.
	RebootHardwareUngraceful = Reboot(robot.RebootHardware + "-no-sync")
	// RebootManually reboots the node via hetzner robot API sending a technician.
	RebootManually = Reboot(robot.RebootManually)
	// RebootRescueSystem reboots the node in rescue mode via 'reboot' command.
	RebootRescueSystem = Reboot("rescue-system")
	// RebootSystem reboots the node via 'reboot' command.
	RebootSystem = Reboot("system")

	KuredRebootSentinel = "/var/run/update_engine_autoupdate_completed"

	KuredRebootSentinelRealTime = "/tmp/kured-reboot-required"
)

// RebootToSSH reboots all nodes and waits until port 22 (ssh) opened.
func (ss Servers) RebootToSSH(rst Reboot) error {
	for _, s := range ss {
		err := s.RebootToSSH(rst)
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}
	}

	return nil
}

// RebootToSSH triggers a reboots and waits until port 22 (ssh) opened.
func (s Server) RebootToSSH(rst Reboot) error {
	err := s.Reboot(rst)
	if err != nil {
		return fmt.Errorf("%v: %v", s.Name, err)
	}

	err = s.waitSSHPort(portClosed)
	if err != nil {
		return err
	}

	err = s.waitSSHPort(portOpen)
	if err != nil {
		return err
	}

	s.log("ssh port opened")

	return nil
}

// Reboot reboots the node.
func (s Server) Reboot(rst Reboot) error {
	s.log("trigger reboot")

	ss := Servers{s}
	return ss.Reboot(rst)
}

// Reboot reboots all nodes.
func (ss Servers) Reboot(rst Reboot) error {
	switch rst {
	case RebootKured:
		return ss.RebootKured()
	case RebootKuredRealTime:
		return ss.RebootKuredRealTime()
	case RebootHardware:
		return ss.RebootHardware()
	case RebootHardwareUngraceful:
		return ss.RebootHardwareUngraceful()
	case RebootManually:
		return ss.RebootManually()
	case RebootRescueSystem:
		return ss.RebootRescueSystem()
	case RebootSystem:
		return ss.RebootSystem()
	}

	return fmt.Errorf("case %v is missing", rst)
}

func (ss Servers) RebootKured() error {
	for _, s := range ss {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return err
		}
		defer ssh.Close()

		err = ssh.Touch(KuredRebootSentinel)
		if err != nil {
			return err
		}
	}

	return nil
}

func (ss Servers) RebootKuredRealTime() error {
	for _, s := range ss {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return err
		}
		defer ssh.Close()

		err = ssh.Touch(KuredRebootSentinelRealTime)
		if err != nil {
			return err
		}
	}

	return nil
}

func (ss Servers) RebootManually() error {
	for _, s := range ss {
		err := s.RobotNode().Reboot(robot.RebootManually)
		if err != nil {
			return err
		}
	}

	return nil
}

func (ss Servers) RebootRescueSystem() error {
	for _, s := range ss {
		ssh, err := s.remoteRescueMode()
		if err != nil {
			return err
		}
		defer ssh.Close()

		err = ssh.SyncDisks()
		if err != nil {
			return err
		}

		err = ssh.Reboot()
		if err != nil {
			return err
		}
	}

	return nil
}

func (ss Servers) RebootHardwareUngraceful() error {
	for _, s := range ss {
		s.Log("triggering reboot")

		err := s.RobotNode().Reboot(robot.RebootHardware)
		if err != nil {
			return err
		}
	}

	return nil
}

func (ss Servers) RebootHardware() error {
	for _, s := range ss {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			s.Log("could not establish ssh connection: %v", err)
			continue
		}
		defer ssh.Close()

		err = ssh.PrepareGracefulReboot()
		if err != nil {
			s.Log("could not prepare graceful shutdown: %v", err)
			continue
		}
	}

	for _, s := range ss {
		s.Log("triggering reboot")

		err := s.RobotNode().Reboot(robot.RebootHardware)
		if err != nil {
			return err
		}
	}

	return nil
}

func (ss Servers) RebootSystem() error {
	for _, s := range ss {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return err
		}
		defer ssh.Close()

		err = ssh.PrepareGracefulReboot()
		if err != nil {
			return err
		}
	}

	for _, s := range ss {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return err
		}
		defer ssh.Close()

		err = ssh.Reboot()
		if err != nil {
			return err
		}
	}

	return nil
}
