package hetznerctl

import (
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/mikefarah/yq/v4/pkg/yqlib"
	"gopkg.in/op/go-logging.v1"
	"gopkg.in/yaml.v3"
)

// NodeRole defines the role of the node.
type NodeRole string

const (
	// PrimaryMaster is the node used to initilize and manage the cluster.
	PrimaryMaster = NodeRole("primary master")
	// BackupMaster nodes are equal to the PrimaryMaster to achieve high availability.
	BackupMaster = NodeRole("backup master")
	// Worker nodes scale the actual cluster.
	Worker = NodeRole("worker")

	// SelectAllNodes is the selector for all nodes.
	SelectAllNodes = "*"
)

// NodeConfig renders the container linux config template.
type NodeConfig struct {
	Config
	Node Server
}

func (c Config) nodeConfig(s Server) NodeConfig {
	return NodeConfig{
		Config: c,
		Node:   s,
	}
}

// JoinConfiguration renders the join config for kubeadm.
type JoinConfiguration struct {
	Config
	CaCertHashes   string
	Token          string
	CertificateKey string
}

// Config contains all configurations.
type Config struct {
	SSHAuthorizedKeys []string `yaml:"ssh_authorized_keys"`
	DNSServers        []string `yaml:"dns_servers"`
	NTPServers        []string `yaml:"ntp_servers"`
	VSwitch           struct {
		ID int
	}
	Rescue struct {
		OS   string
		Arch string
	}
	Disks struct {
		OS   string
		Data string
	}
	Flatcar struct {
		InstallScriptUrl string `yaml:"install_script_url"`
		Version          string
		DisableUpdates   bool   `yaml:"disable_updates"`
		UpdateURL        string `yaml:"update_url"`
		BaseURL          string `yaml:"base_url"`
		AppID            string `yaml:"app_id"`
		Group            string
		SignKey          struct {
			URL    string `yaml:"url"`
			SHA256 string `yaml:"sha256"`
		} `yaml:"sign_key"`
	}
	Cloudflare struct {
		Key        string
		Email      string
		Zone       string
		Subdomains struct {
			API  string
			HTTP string
		}
	}
	DockerHub struct {
		Username      string
		Password      string
		DisableViaDns bool `yaml:"disable_via_dns"`
	}
	RegistryMirror struct {
		Endpoint string
		Username string
		Password string
	}
	RegistryMirrorV2 struct {
		Host       string
		Path       string
		PathWithV2 string `yaml:"path_with_v2"`
		Username   string
		Password   string
	} `yaml:"registry_mirror_v2"`
	RegistryMirrors []struct {
		Registry   string
		Host       string
		Path       string
		PathWithV2 string `yaml:"path_with_v2"`
		Username   string
		Password   string
	} `yaml:"registry_mirrors"`
	Hardware struct {
		Disk1    string
		Disk2    string
		DiskType string `yaml:"diskType"`
	}
	Cluster struct {
		Name            string
		Domain          string
		Password        string
		BasicAuth       string `yaml:"basic_auth"`
		MasterNodeCount int    `yaml:"master_node_count"`
		PodsPerNode     int    `yaml:"pods_per_node"`
	}
	Services Services
	Users    []User
	Nodes    Servers
}

// Services configures the services via templates in .yaml.tpl files.
type Services struct {
	Minio struct {
		AccessKey string `yaml:"access_key"`
		SecretKey string `yaml:"secret_key"`
		Backup    struct {
			Cron      string
			Enabled   bool
			Host      string
			AccessKey string `yaml:"access_key"`
			SecretKey string `yaml:"secret_key"`
			Directory string
		}
	}
	Cassandra struct {
		Backups []struct {
			Keyspace string
			Cron     string
			Enabled  bool
		}
	}
	Kured struct {
		RebootSentinel string `yaml:"reboot_sentinel"`
		StartTime      string `yaml:"start_time"`
		EndTime        string `yaml:"end_time"`
		Period         string `yaml:"period"`
	}
	Registry struct {
		ServiceIP string `yaml:"service_ip"`
		Secret    string
	}
}

// User represents a kubectl user.
type User struct {
	Name      string
	Role      string
	Namespace string
	Active    bool
}

// Servers represents the nodes of the cluster.
type Servers []Server

// Server represents a node of the cluster.
type Server struct {
	Name    string
	Hetzner struct {
		Username string
		Password string
	}
	Role    NodeRole
	Network struct {
		IPv6       string
		PublicIP   string `yaml:"public_ip"`
		Gateway    string
		PrivateIP  string `yaml:"private_ip"`
		MAC        string
		DeviceName string `yaml:"device_name"`
	}
}

func (s Server) log(f string, a ...interface{}) {
	log.Println(s.error(f, a...))
}

// Log prints messages prefixed with the servers name.
func (s Server) Log(f string, a ...interface{}) {
	log.Printf("%s: %s", s.Name, fmt.Sprintf(f, a...))
}

func (s Server) error(f string, a ...interface{}) error {
	return fmt.Errorf("%s: %s", s.Name, fmt.Sprintf(f, a...))
}

// SanityCheck verifies the config does not contain obvious errors.
func (c Config) SanityCheck() error {
	if c.VSwitch.ID < 4000 || c.VSwitch.ID > 4091 {
		return fmt.Errorf("vswitch id %d not between 4000 and 4091", c.VSwitch.ID)
	}

	if !strings.Contains(c.Cluster.BasicAuth, ":$apr1$") {
		return fmt.Errorf("basic auth %s does not look like a valid hash, it should be md5 encoded '$apr1$' for nginx to accept it", c.Cluster.BasicAuth)
	}

	ipMap := make(map[string]int)
	for _, node := range c.Nodes {
		ipMap[node.Network.IPv6]++
		ipMap[node.Network.MAC]++
		ipMap[node.Network.PrivateIP]++
		ipMap[node.Network.PublicIP]++
	}

	for ip, ipCount := range ipMap {
		if ipCount > 1 {
			return fmt.Errorf("ip/mac address '%s' has to be unique, but was used %d times", ip, ipCount)
		}
	}

	// for S3 credential verification check https://aws.amazon.com/de/blogs/security/a-safer-way-to-distribute-aws-credentials-to-ec2/
	reAccessKey := regexp.MustCompile(`^[A-Za-z0-9]{20}$`)
	reSecretKey := regexp.MustCompile(`^[A-Za-z0-9+=]{40}$`)

	if !reAccessKey.MatchString(c.Services.Minio.AccessKey) {
		return fmt.Errorf("minio AccessKey is not valid, accepted regex: ^[A-Za-z0-9]{20}$")
	}

	if strings.ContainsRune(c.Services.Minio.SecretKey, '/') {
		return fmt.Errorf("loki does not accept '/' in a SecretKey, see https://github.com/grafana/loki/issues/1607")
	}

	if !reSecretKey.MatchString(c.Services.Minio.SecretKey) {
		return fmt.Errorf("minio SecretKey is not valid, accepted regex: ^[A-Za-z0-9+=]{40}$")
	}

	// password needs to be set
	if c.Cluster.Password == "" {
		return fmt.Errorf("password can not be empty")
	}

	// ensure master_node_count matches count of master nodes
	if c.Cluster.MasterNodeCount != c.Nodes.MastersCount() {
		return fmt.Errorf("cluster.master_node_count does not match count of master nodes: expected %d, found %d", c.Nodes.MastersCount(), c.Cluster.MasterNodeCount)
	}

	return nil
}

// PrintConfigTemplate prints the default config template.
func PrintConfigTemplate(w io.Writer) error {
	cfg := Config{
		SSHAuthorizedKeys: []string{"ssh-rsa AA... root@host"},
		DNSServers:        []string{"213.133.98.98", "213.133.99.99", "213.133.100.100"},
		NTPServers:        []string{"ntp1.hetzner.de", "ntp2.hetzner.com", "ntp3.hetzner.net"},
		VSwitch: struct {
			ID int
		}{
			ID: 1234,
		},
		Rescue: struct {
			OS   string
			Arch string
		}{
			OS:   "linux",
			Arch: "64",
		},
		Disks: struct {
			OS   string
			Data string
		}{Data: "/dev/nvme0n1", OS: "/dev/nvme1n1"},
		Flatcar: struct {
			InstallScriptUrl string `yaml:"install_script_url"`
			Version          string
			DisableUpdates   bool   `yaml:"disable_updates"`
			UpdateURL        string `yaml:"update_url"`
			BaseURL          string `yaml:"base_url"`
			AppID            string `yaml:"app_id"`
			Group            string
			SignKey          struct {
				URL    string `yaml:"url"`
				SHA256 string `yaml:"sha256"`
			} `yaml:"sign_key"`
		}{
			InstallScriptUrl: defaultFlatcarInstaller,
			Version:          "current",
			DisableUpdates:   false,
			UpdateURL:        "https://nebraska.flatcar.turbinekreuzberg.io/v1/update/",
			BaseURL:          "https://minio.flatcar.turbinekreuzberg.io/flatcar-images/",
			AppID:            "io.turbinekreuzberg.linux",
			Group:            "beta",
			SignKey: struct {
				URL    string `yaml:"url"`
				SHA256 string `yaml:"sha256"`
			}{
				URL:    "https://minio.flatcar.turbinekreuzberg.io/flatcar-images/sign.pub",
				SHA256: "862dbc7a43ba2783518d4dec1917b8c816bd2eaa2a59d1a7c1b2e35d37f1647d",
			},
		},
		Cloudflare: struct {
			Key        string
			Email      string
			Zone       string
			Subdomains struct {
				API  string
				HTTP string
			}
		}{
			Zone: "cluster-domain.tld",
			Subdomains: struct {
				API  string
				HTTP string
			}{API: "k8s", HTTP: "lb"},
		},
		DockerHub: struct {
			Username      string
			Password      string
			DisableViaDns bool "yaml:\"disable_via_dns\""
		}{
			Username:      "docker-hub-user",
			Password:      "docker-hub-password",
			DisableViaDns: false,
		},
		RegistryMirrors: []struct {
			Registry   string
			Host       string
			Path       string
			PathWithV2 string "yaml:\"path_with_v2\""
			Username   string
			Password   string
		}{
			{
				Registry:   "docker.io",
				Host:       "registry-domain.tld",
				Path:       "/ecr-or-harbor-project-path",
				PathWithV2: "/v2/ecr-or-harbor-project-path",
				Username:   "",
				Password:   "",
			},
		},
		Hardware: struct {
			Disk1    string
			Disk2    string
			DiskType string `yaml:"diskType"`
		}{Disk1: "/dev/nvme0", Disk2: "/dev/nvme1", DiskType: "nvme"},
		Cluster: struct {
			Name            string
			Domain          string
			Password        string
			BasicAuth       string "yaml:\"basic_auth\""
			MasterNodeCount int    `yaml:"master_node_count"`
			PodsPerNode     int    `yaml:"pods_per_node"`
		}{
			Domain:          "cluster-domain.tld",
			BasicAuth:       "username:$apr1$...",
			MasterNodeCount: 3,
		},
		Services: Services{
			Minio: struct {
				AccessKey string `yaml:"access_key"`
				SecretKey string `yaml:"secret_key"`
				Backup    struct {
					Cron      string
					Enabled   bool
					Host      string
					AccessKey string `yaml:"access_key"`
					SecretKey string `yaml:"secret_key"`
					Directory string
				}
			}{
				AccessKey: "12345678901234567890",
				SecretKey: "1234567890123456789012345678901234567890",
				Backup: struct {
					Cron      string
					Enabled   bool
					Host      string
					AccessKey string `yaml:"access_key"`
					SecretKey string `yaml:"secret_key"`
					Directory string
				}{
					Enabled:   false,
					Host:      "http://external-s3-host/",
					AccessKey: "12345678901234567890",
					SecretKey: "1234567890123456789012345678901234567890",
					Directory: "/archive",
				},
			},
			Cassandra: struct {
				Backups []struct {
					Keyspace string
					Cron     string
					Enabled  bool
				}
			}{
				Backups: []struct {
					Keyspace string
					Cron     string
					Enabled  bool
				}{
					{
						Keyspace: "reaper_db",
						Cron:     "0 7 * * *",
						Enabled:  false,
					},
				},
			},
			Kured: struct {
				RebootSentinel string `yaml:"reboot_sentinel"`
				StartTime      string `yaml:"start_time"`
				EndTime        string `yaml:"end_time"`
				Period         string `yaml:"period"`
			}{
				RebootSentinel: KuredRebootSentinel,
				StartTime:      "01:00:00",
				EndTime:        "04:00:00",
				Period:         "5m",
			},
			Registry: struct {
				ServiceIP string "yaml:\"service_ip\""
				Secret    string
			}{
				ServiceIP: "10.96.0.9",
			},
		},
		Users: []User{
			{
				Name:   "test",
				Role:   "cluster-admin",
				Active: true,
			},
		},
		Nodes: Servers{
			Server{
				Name: "node1",
				Role: "primary master",
				Network: struct {
					IPv6       string
					PublicIP   string `yaml:"public_ip"`
					Gateway    string
					PrivateIP  string `yaml:"private_ip"`
					MAC        string
					DeviceName string `yaml:"device_name"`
				}{PrivateIP: "192.168.100.1"},
			},
			Server{
				Name: "node2",
				Role: "backup master",
				Network: struct {
					IPv6       string
					PublicIP   string `yaml:"public_ip"`
					Gateway    string
					PrivateIP  string `yaml:"private_ip"`
					MAC        string
					DeviceName string `yaml:"device_name"`
				}{PrivateIP: "192.168.100.2"},
			},
			Server{
				Name: "node3",
				Role: "backup master",
				Network: struct {
					IPv6       string
					PublicIP   string `yaml:"public_ip"`
					Gateway    string
					PrivateIP  string `yaml:"private_ip"`
					MAC        string
					DeviceName string `yaml:"device_name"`
				}{PrivateIP: "192.168.100.3"},
			},
			Server{
				Name: "node4",
				Role: "worker",
				Network: struct {
					IPv6       string
					PublicIP   string `yaml:"public_ip"`
					Gateway    string
					PrivateIP  string `yaml:"private_ip"`
					MAC        string
					DeviceName string `yaml:"device_name"`
				}{PrivateIP: "192.168.100.4"},
			},
		},
	}

	return PrintConfig(w, cfg)
}

// APIDomain returns the API servers domain without port.
func (c Config) APIDomain() string {
	return c.Cloudflare.Subdomains.API + "." + c.Cloudflare.Zone
}

func (c Config) Write() error {
	return c.WriteTo(ConfigPath())
}

func (c Config) WriteTo(path string) error {
	f, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		return fmt.Errorf("open config file: %v", err)
	}
	defer f.Close()

	_, err = f.Write([]byte("# additional line break corrupts ssh keys @see https://github.com/go-yaml/yaml/issues/166\n"))
	if err != nil {
		return fmt.Errorf("write warning: %v", err)
	}

	err = yaml.NewEncoder(f).Encode(c)
	if err != nil {
		return fmt.Errorf("encode config: %v", err)
	}

	return nil
}

func (c Config) ToYaml() ([]byte, error) {
	yml, err := yaml.Marshal(c)
	if err != nil {
		return []byte(""), fmt.Errorf("encode config: %v", err)
	}

	return yml, nil
}

// PrintConfig prints the default config template.
func PrintConfig(w io.Writer, cfg Config) error {
	configWithDefaults, err := os.CreateTemp("", "cluster-*.yaml")
	if err != nil {
		return fmt.Errorf("create tempfile for config with default values: %v", err)
	}
	defer configWithDefaults.Close()
	defer os.Remove(configWithDefaults.Name())
	err = cfg.WriteTo(configWithDefaults.Name())
	if err != nil {
		return fmt.Errorf("write config files with default values: %v", err)
	}

	// Merge the cluster.yaml and the parsed configuration.
	yqlib.InitExpressionParser()
	expression := "select(fileIndex == 0) * select(filename == \"" + configWithDefaults.Name() + "\")"
	files := []string{}
	if fileExists(ConfigPath()) {
		files = append(files, ConfigPath())
	}
	files = append(files, configWithDefaults.Name())
	pref := yqlib.YamlPreferences{
		PrintDocSeparators: true,
		UnwrapScalar:       true,
	}
	encoder := yqlib.NewYamlEncoder(pref)
	writer := yqlib.NewSinglePrinterWriter(w)
	printer := yqlib.NewPrinter(encoder, writer)
	decoder := yqlib.NewYamlDecoder(pref)
	logging.SetLevel(logging.ERROR, "yq-lib") // change yqlib's default log level (DEBUG) to something less annoying
	err = yqlib.NewAllAtOnceEvaluator().EvaluateFiles(expression, files, printer, decoder)
	if err != nil {
		return fmt.Errorf("merge config files: %v", err)
	}

	return nil
}

func ConfigPath() string {
	cfgFile, found := os.LookupEnv("UTOPIACONFIG")
	if !found {
		cfgFile = "cluster.yaml"
	}

	return cfgFile
}

// LoadConfig load the config from the reader.
func LoadConfig(r io.Reader) (Config, error) {
	d := yaml.NewDecoder(r)

	cfg := Config{}

	err := d.Decode(&cfg)
	if err != nil {
		return Config{}, err
	}

	addDefaults(&cfg)

	return cfg, nil
}

func addDefaults(c *Config) {
	defaultCron := "0 0 31 2 0"

	if c.Cluster.PodsPerNode == 0 {
		c.Cluster.PodsPerNode = 110
	}

	if c.Services.Minio.Backup.Cron == "" {
		c.Services.Minio.Backup.Cron = defaultCron
	}

	if c.Flatcar.Version == "" {
		c.Flatcar.Version = "current"
	}

	for i := range c.Services.Cassandra.Backups {
		if c.Services.Cassandra.Backups[i].Cron == "" {
			c.Services.Cassandra.Backups[i].Cron = defaultCron
		}
	}

	if c.Services.Kured.StartTime == "" {
		c.Services.Kured.StartTime = "01:00:00"
	}
	if c.Services.Kured.EndTime == "" {
		c.Services.Kured.EndTime = "04:00:00"
	}
	if c.Services.Kured.Period == "" {
		c.Services.Kured.Period = "5m"
	}
	if c.Services.Kured.RebootSentinel == "" {
		c.Services.Kured.RebootSentinel = KuredRebootSentinel
	}
}

func (s Server) matches(match string) bool {
	return s.Name == match || s.Network.PublicIP == match || s.Network.PrivateIP == match
}

// Filter selects the nodes of a role type.
func (ss Servers) Filter(nodeSelection string) Servers {
	if nodeSelection == SelectAllNodes {
		return ss
	}

	ls := Servers{}

	for _, match := range regexp.MustCompile(`,`).Split(nodeSelection, -1) {
		for _, s := range ss {
			if s.matches(match) {
				ls = append(ls, s)
			}
		}
	}

	return ls
}

// MastersCount counts the masters.
func (ss Servers) MastersCount() int {
	c := 0

	for _, s := range ss {
		if s.Role == PrimaryMaster || s.Role == BackupMaster {
			c++
		}
	}

	return c
}

// Count counts the nodes.
func (ss Servers) Count() int {
	return len(ss)
}

// PrimaryMaster returns the primary master.
func (ss Servers) PrimaryMaster() (Server, error) {
	master := Server{}
	found := false

	for _, s := range ss {
		if s.Role == PrimaryMaster {
			if found {
				return Server{}, fmt.Errorf("more than one node is defined as primary master")
			}

			found = true
			master = s
		}
	}

	if !found {
		return Server{}, fmt.Errorf("no node is defined as primary master")
	}

	return master, nil
}

// NonePrimaryMaster selects backups master and worker nodes.
func (ss Servers) NonePrimaryMaster() Servers {
	ls := Servers{}

	for _, s := range ss {
		if s.Role != PrimaryMaster {
			ls = append(ls, s)
		}
	}

	return ls
}

// SelectPrimaryMasters selects the primary master nodes.
func (ss Servers) SelectPrimaryMasters() Servers {
	ls := Servers{}

	for _, s := range ss {
		if s.Role == PrimaryMaster {
			ls = append(ls, s)
		}
	}

	return ls
}

// SelectBackupMasters selects the backup master nodes.
func (ss Servers) SelectBackupMasters() Servers {
	ls := Servers{}

	for _, s := range ss {
		if s.Role == BackupMaster {
			ls = append(ls, s)
		}
	}

	return ls
}

// SelectMasters selects the master nodes.
func (ss Servers) SelectMasters() Servers {
	ls := Servers{}

	for _, s := range ss {
		if s.Role == PrimaryMaster || s.Role == BackupMaster {
			ls = append(ls, s)
		}
	}

	return ls
}

// SelectWorkers selects the worker nodes.
func (ss Servers) SelectWorkers() Servers {
	ls := Servers{}

	for _, s := range ss {
		if s.Role == Worker {
			ls = append(ls, s)
		}
	}

	return ls
}
