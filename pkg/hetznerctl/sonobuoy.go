package hetznerctl

func (s Server) Sonobuoy() error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}

	script := `
until [ $(sonobuoy status | grep passed | wc -l) -eq 2 ]; do
	sonobuoy delete --wait
	sonobuoy run --wait --plugin-env=e2e.E2E_PARALLEL=y --plugin-env=e2e.E2E_SKIP="\[Disruptive\]|NoExecuteTaintManager|\[Serial\]"
	sonobuoy status
done
sonobuoy delete --wait;
`

	err = ssh.RunStdout(script)
	if err != nil {
		return err
	}

	return nil
}
