package hetznerctl

import "fmt"

// Patches represents the changes applied to etcd, apiserver, controller-manager, and scheduler.
type Patches struct {
	Etcd struct {
		Request struct {
			CPU    string
			Memory string
		}
		Limit struct {
			CPU    string
			Memory string
		}
	}
	APIServer struct {
		Request struct {
			CPU    string
			Memory string
		}
		Limit struct {
			CPU    string
			Memory string
		}
	}
	ControllerManager struct {
		Request struct {
			CPU    string
			Memory string
		}
		Limit struct {
			CPU    string
			Memory string
		}
	}
	Scheduler struct {
		Request struct {
			CPU    string
			Memory string
		}
		Limit struct {
			CPU    string
			Memory string
		}
	}
}

// ApiserverMemory calculates the target memory use for the API Server.
func (c Config) ApiserverMemory() int {
	pods := len(c.Nodes) * c.Cluster.PodsPerNode
	return int(500 + (3.3 * float64(pods)))
}

// KubernetesVersion returns the version of Kubernetes.
func (c Config) KubernetesVersion() string {
	return KUBERNETES_VERSION
}

func generatePatches(cfg Config) Patches {
	// https://applatix.com/making-kubernetes-production-ready-part-2/
	pods := len(cfg.Nodes) * cfg.Cluster.PodsPerNode
	safetyMultiplier := 1.2

	etcdCPU := safetyMultiplier * (120 + (0.4 * float64(pods)))
	etcdMemory := safetyMultiplier * (120 + (1.8 * float64(pods)))
	apiserverCPU := safetyMultiplier * (300 + (0.33 * float64(pods)))
	apiserverMemory := safetyMultiplier * (500 + (3.3 * float64(pods)))
	controllerCPU := safetyMultiplier * (80 + (0.00 * float64(pods)))
	controllerMemory := safetyMultiplier * (50 + (0.67 * float64(pods)))
	schedulerCPU := safetyMultiplier * (200 + (0.00 * float64(pods)))
	schedulerMemory := safetyMultiplier * (30 + (0.16 * float64(pods)))

	patches := Patches{}
	patches.Etcd.Request.CPU = fmt.Sprintf("%dm", int(etcdCPU))
	patches.Etcd.Request.Memory = fmt.Sprintf("%dMi", int(etcdMemory))
	patches.Etcd.Limit.CPU = fmt.Sprintf("%dm", int(etcdCPU))
	patches.Etcd.Limit.Memory = fmt.Sprintf("%dMi", int(etcdMemory))
	patches.APIServer.Request.CPU = fmt.Sprintf("%dm", int(apiserverCPU))
	patches.APIServer.Request.Memory = fmt.Sprintf("%dMi", int(apiserverMemory))
	patches.APIServer.Limit.CPU = fmt.Sprintf("%dm", int(apiserverCPU))
	patches.APIServer.Limit.Memory = fmt.Sprintf("%dMi", int(apiserverMemory))
	patches.ControllerManager.Request.CPU = fmt.Sprintf("%dm", int(controllerCPU))
	patches.ControllerManager.Request.Memory = fmt.Sprintf("%dMi", int(controllerMemory))
	patches.ControllerManager.Limit.CPU = fmt.Sprintf("%dm", int(controllerCPU))
	patches.ControllerManager.Limit.Memory = fmt.Sprintf("%dMi", int(controllerMemory))
	patches.Scheduler.Request.CPU = fmt.Sprintf("%dm", int(schedulerCPU))
	patches.Scheduler.Request.Memory = fmt.Sprintf("%dMi", int(schedulerMemory))
	patches.Scheduler.Limit.CPU = fmt.Sprintf("%dm", int(schedulerCPU))
	patches.Scheduler.Limit.Memory = fmt.Sprintf("%dMi", int(schedulerMemory))

	return patches
}
