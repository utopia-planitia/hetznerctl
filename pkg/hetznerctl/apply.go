package hetznerctl

import (
	"bytes"
	"fmt"

	"gitlab.com/utopia-planitia/hetznerctl/pkg/remote"
)

// Apply applies manifests to the setup.
// Yaml strings are used as they are.
func (s Server) Apply(yaml []byte, delete bool, namespace string) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer ssh.Close()

	return apply(ssh, yaml, delete, namespace)
}

// ApplyTpl applies rendered manifests to the setup.
// Yaml strings are used as templates and rendered before being applied.
func (s Server) ApplyTpl(yaml []byte, delete bool, namespace string, cfg *Config) error {
	yaml, err := render("stdin", string(yaml), cfg)
	if err != nil {
		return fmt.Errorf("render content: %v", err)
	}

	return s.Apply(yaml, delete, namespace)
}

func apply(s remote.Remote, yml []byte, delete bool, namespace string) error {
	stdIn := &bytes.Buffer{}
	stdOut := &bytes.Buffer{}
	stdErr := &bytes.Buffer{}

	stdIn.Write(yml)

	err := s.Execute(applyCmd(delete, namespace), stdIn, stdOut, stdErr)
	if err != nil {
		return fmt.Errorf("%s%s%s", stdOut.Bytes(), stdErr.Bytes(), err)
	}

	return nil
}

func applyCmd(delete bool, namespace string) string {
	cmd := "apply"

	if delete {
		cmd = "delete"
	}

	if namespace == "" {
		return fmt.Sprintf("kubectl %s -f -", cmd)
	}

	return fmt.Sprintf("kubectl %s -n %s -f -", cmd, namespace)
}
