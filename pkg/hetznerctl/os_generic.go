package hetznerctl

import (
	"fmt"
	"path/filepath"
	"strings"
	"sync"

	"gitlab.com/utopia-planitia/hetznerctl/pkg/remote"
)

// SetupFlatcar installs the operating system on all nodes.
func (ss Servers) SetupFlatcar(cfg Config) error {
	waitgroup := sync.WaitGroup{}
	errors := make(chan error, len(cfg.Nodes))

	waitgroup.Add(len(ss))

	for _, s := range ss {
		go func(s Server) {
			defer waitgroup.Done()

			err := s.SetupFlatcar(cfg)
			if err != nil {
				s.log("%s", err.Error())
				errors <- fmt.Errorf("%v: %v", s.Name, err)
			}
		}(s)
	}

	waitgroup.Wait()
	close(errors)

	if len(errors) > 0 {
		err := fmt.Errorf("installing operating system: ")
		for e := range errors {
			err = fmt.Errorf("%s; %s", err, e)
		}

		return err
	}

	return nil
}

// BootRescueMode reboots all nodes into rescue mode.
func (ss Servers) BootRescueMode(cfg Config) error {
	for _, s := range ss {
		err := s.BootRescueMode(cfg)
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}
	}

	return nil
}

// BootRescueMode reboots into rescue mode and installs SSH keys.
func (s Server) BootRescueMode(cfg Config) error {
	s.log("enable rescue mode")

	rescueMode, err := s.RobotNode().EnableRescueMode(cfg.Rescue.OS, cfg.Rescue.Arch)
	if err != nil {
		return err
	}

	err = s.RebootToSSH(RebootHardware)
	if err != nil {
		return err
	}

	ssh, err := s.remoteRescueModePassword(rescueMode.Password)
	if err != nil {
		return err
	}
	defer ssh.Close()

	s.log("copy ssh authorized_keys")

	keys := strings.Join(cfg.SSHAuthorizedKeys, "\n")

	return ssh.WriteFile([]byte(keys), "/root/.ssh/authorized_keys", 0644)
}

// DeleteDataDisk empties the data disk on all nodes.
func (ss Servers) DeleteDataDisk(cfg Config) error {
	for _, s := range ss {
		ssh, err := s.remoteOperatingSystem()
		if err != nil {
			return fmt.Errorf("connect primary master: %v", err)
		}
		defer ssh.Close()

		err = ssh.DeleteDisk(cfg.Disks.Data)
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}
	}

	return nil
}

// Uncordon uncordons all nodes.
func (ss Servers) Uncordon(master Server) error {
	ssh, err := master.remoteOperatingSystem()
	if err != nil {
		return fmt.Errorf("connect primary master: %v", err)
	}
	defer ssh.Close()

	for _, s := range ss {
		err := ssh.Uncordon(s.Name, false)
		if err != nil {
			return err
		}
	}

	return nil
}

// ConfigureSSHKeys prepares all nodes to be accessible via ssh.
func (ss Servers) ConfigureSSHKeys(cfg Config) error {
	for _, s := range ss {
		err := s.ConfigureSSHKeys(cfg)
		if err != nil {
			return fmt.Errorf("%v: %v", s.Name, err)
		}
	}

	return nil
}

// ConfigureSSHKeys prepares node to be accessible via ssh.
func (s Server) ConfigureSSHKeys(cfg Config) error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer ssh.Close()

	err = s.authorizeKeys(ssh, cfg.SSHAuthorizedKeys)
	if err != nil {
		return fmt.Errorf("configure ssh authorized keys: %v", err)
	}

	return nil
}

func (s Server) authorizeKeys(ssh remote.Remote, keys []string) error {
	s.log("copy ssh authorized_keys")

	kk := strings.Join(keys, "\n")

	err := ssh.WriteFile([]byte(kk), ".ssh/authorized_keys", 0644)
	if err != nil {
		return fmt.Errorf("write updated ssh keys: %v", err)
	}

	return nil
}

func (s Server) downloadTooling() error {
	ssh, err := s.remoteOperatingSystem()
	if err != nil {
		return err
	}
	defer ssh.Close()

	return downloadTooling(ssh)
}

func downloadTooling(ssh remote.Remote) error {
	// k8status
	ssh.Log("install k8status")

	err := ssh.Run(fmt.Sprintf(`
set -eux
export PATH=/opt/bin:$PATH

k8status_is_expected_version(){
	k8status version | tee --append /dev/stderr | grep --fixed-strings --quiet '%s'
}

if k8status_is_expected_version; then
	exit 0
fi

mkdir -p /tmp/k8status
curl --fail --silent --show-error --location --max-time 120 \
"https://github.com/utopia-planitia/k8status/releases/download/%s/k8status_Linux_x86_64.tar.gz" \
| tar -C /tmp/k8status -xz
mv /tmp/k8status/k8status /opt/bin/k8status
rm -rf /tmp/k8status

if ! k8status_is_expected_version; then
	exit 1
fi
`, strings.TrimPrefix(K8STATUS_VERSION, "v"), K8STATUS_VERSION))
	if err != nil {
		return err
	}

	// helm
	ssh.Log("install helm")

	err = ssh.Run(fmt.Sprintf(`
set -eux
export PATH=/opt/bin:$PATH

helm_is_expected_version() {
	helm version | tee --append /dev/stderr | grep --fixed-strings --quiet '%s'
}

if helm_is_expected_version; then
	exit 0
fi
curl --fail --silent --show-error --location --max-time 120 --output /opt/bin/get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod +x /opt/bin/get_helm.sh
DESIRED_VERSION="%s" HELM_INSTALL_DIR=/opt/bin/ get_helm.sh
unlink /opt/bin/get_helm.sh

if ! helm_is_expected_version; then
	exit 1
fi
`, HELM_VERSION, HELM_VERSION))
	if err != nil {
		return err
	}

	// helmfile
	ssh.Log("install helmfile")

	err = ssh.Run(fmt.Sprintf(`
set -eux
export PATH=/opt/bin:$PATH

helmfile_is_expected_version() {
	helmfile version | tee --append /dev/stderr | grep --fixed-strings --quiet '%s'
}

if helmfile_is_expected_version; then
	exit 0
fi
curl --fail --silent --show-error --location --max-time 120 --output helmfile.tar.gz "https://github.com/helmfile/helmfile/releases/download/%s/helmfile_%s_linux_amd64.tar.gz"
tar --auto-compress --directory=/opt/bin --extract --file=helmfile.tar.gz --no-same-owner --no-same-permissions helmfile
rm --force helmfile.tar.gz

if ! helmfile_is_expected_version; then
	exit 1
fi

mkdir --parents /root/.local/share/bash-completion/completions
helmfile completion bash >/root/.local/share/bash-completion/completions/helmfile
`, strings.TrimPrefix(HELMFILE_VERSION, "v"), HELMFILE_VERSION, strings.TrimPrefix(HELMFILE_VERSION, "v")))
	if err != nil {
		return err
	}

	// kustomize
	ssh.Log("install kustomize")

	err = ssh.Run(fmt.Sprintf(`
set -eux
export PATH=/opt/bin:$PATH

kustomize_is_expected_version() {
	kustomize version | tee --append /dev/stderr | grep --fixed-strings --quiet 'v%s'
}

if kustomize_is_expected_version; then
	exit 0
fi
curl --fail --silent --show-error --location --max-time 120 --output /opt/bin/install_kustomize.sh https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh
chmod +x /opt/bin/install_kustomize.sh
rm -f /opt/bin/kustomize
install_kustomize.sh %s /opt/bin/
rm /opt/bin/install_kustomize.sh

if ! kustomize_is_expected_version; then
	exit 1
fi
`, KUSTOMIZE_VERSION, KUSTOMIZE_VERSION))
	if err != nil {
		return err
	}

	// sonobuoy
	ssh.Log("install sonobuoy")

	err = ssh.Run(fmt.Sprintf(`
set -eux
export PATH=/opt/bin:$PATH

sonobuoy_is_expected_version() {
	sonobuoy version --short | tee --append /dev/stderr | grep --fixed-strings --quiet '%s'
}

if sonobuoy_is_expected_version; then
	exit 0
fi

curl --fail --silent --show-error --location --max-time 120 \
"https://github.com/vmware-tanzu/sonobuoy/releases/download/%s/sonobuoy_%s_linux_amd64.tar.gz" \
| tar -C /opt/bin -xz
rm /opt/bin/LICENSE

if ! sonobuoy_is_expected_version; then
	exit 1
fi
`, SONOBUOY_VERSION, SONOBUOY_VERSION, strings.TrimPrefix(SONOBUOY_VERSION, "v")))
	if err != nil {
		return err
	}

	// crictl
	ssh.Log("install crictl")

	err = ssh.Run(`
set -eux
export PATH=/opt/bin:$PATH

cat > /etc/crictl.yaml <<EOF
runtime-endpoint: unix:///run/containerd/containerd.sock
image-endpoint: unix:///run/containerd/containerd.sock
timeout: 10
debug: false
EOF
	`)
	if err != nil {
		return err
	}

	err = ssh.Run(fmt.Sprintf(`
set -eux
export PATH=/opt/bin:$PATH

crictl_is_expected_version() {
	crictl --version | tee --append /dev/stderr | grep --fixed-strings --quiet '%s'
}

if crictl_is_expected_version; then
	exit 0
fi

curl --fail --silent --show-error --location --max-time 120 \
	"https://github.com/kubernetes-sigs/cri-tools/releases/download/%s/crictl-%s-linux-amd64.tar.gz" |
	tar --directory=/opt/bin -xz

if ! crictl_is_expected_version; then
	exit 1
fi
`, CRICTL_VERSION, CRICTL_VERSION, CRICTL_VERSION))
	if err != nil {
		return err
	}

	// velero
	ssh.Log("install velero")

	err = ssh.Run(fmt.Sprintf(`
set -eux
export PATH=/opt/bin:$PATH

velero_is_expected_version() {
	velero version --client-only | tee --append /dev/stderr | grep --fixed-strings --quiet '%s'
}

if velero_is_expected_version; then
	exit 0
fi

mkdir -p /tmp/velero
curl --fail --silent --show-error --location --max-time 120 \
"https://github.com/vmware-tanzu/velero/releases/download/%s/velero-%s-linux-amd64.tar.gz" \
| tar -C /tmp/velero -xz
mv /tmp/velero/velero-%s-linux-amd64/velero /opt/bin/velero
rm -rf /tmp/velero

if ! velero_is_expected_version; then
	exit 1
fi
`, VELERO_VERSION, VELERO_VERSION, VELERO_VERSION, VELERO_VERSION))
	if err != nil {
		return err
	}

	// Virtctl
	ssh.Log("install virtctl")

	err = ssh.Run(fmt.Sprintf(`
set -eux
export PATH=/opt/bin:$PATH

virtctl_is_expected_version() {
	virtctl version --client | tee --append /dev/stderr | grep --fixed-strings --quiet '%s'
}

if virtctl_is_expected_version; then
	exit 0
fi

mkdir -p /tmp/virtctl
curl --fail --silent --show-error --location --max-time 120 --output /tmp/virtctl/virtctl \
"https://github.com/kubevirt/kubevirt/releases/download/%s/virtctl-%s-linux-amd64"
chmod +x /tmp/virtctl/virtctl
mv /tmp/virtctl/virtctl /opt/bin/virtctl
rm -rf /tmp/virtctl

if ! virtctl_is_expected_version; then
	exit 1
fi
`, VIRTCTL_VERSION, VIRTCTL_VERSION, VIRTCTL_VERSION))
	if err != nil {
		return err
	}

	return nil
}

func modifyRenderedAsset(ssh remote.Remote, file, asset string, data interface{}) (bool, error) {
	directory := filepath.Dir(file)

	err := ssh.Run(fmt.Sprintf("mkdir -p \"%s\"", directory))
	if err != nil {
		return false, err
	}

	oldHash, _ := ssh.Md5(file)

	netcfg, err := renderAsset(asset, data)
	if err != nil {
		return false, err
	}

	err = ssh.WriteFile(netcfg, file, 0644)
	if err != nil {
		return false, err
	}

	newHash, err := ssh.Md5(file)
	if err != nil {
		return false, err
	}

	changed := oldHash != newHash

	return changed, nil
}
