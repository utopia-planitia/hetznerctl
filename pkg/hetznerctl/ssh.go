package hetznerctl

import (
	"fmt"
	"net"
	"strconv"
	"time"

	"gitlab.com/utopia-planitia/hetznerctl/pkg/remote"
)

type portState bool

const (
	portOpen   = portState(true)
	portClosed = portState(false)

	tcp     = "tcp"
	sshPort = 22

	rescueSSHUser = "root"
)

func (p portState) String() string {
	switch p {
	case portClosed:
		return "closed"
	case portOpen:
		return "open"
	default:
		panic(fmt.Sprintf("not covered case: %s", p.String()))
	}
}

func (s Server) remoteOperatingSystem() (remote.Remote, error) {
	addr := fmt.Sprintf("%s:%d", s.Network.PublicIP, sshPort)

	return remote.NewRemoteAuthSocket(s.Name, addr, s.sshUsername())
}

func (s Server) sshUsername() string {
	return "core"
}

func (s Server) remoteRescueMode() (remote.Remote, error) {
	addr := fmt.Sprintf("%s:%d", s.Network.PublicIP, sshPort)
	return remote.NewRemoteAuthSocket(s.Name, addr, rescueSSHUser)
}

func (s Server) remoteRescueModePassword(password string) (remote.Remote, error) {
	addr := fmt.Sprintf("%s:%d", s.Network.PublicIP, sshPort)
	return remote.NewRemoteUsernamePassword(s.Name, addr, rescueSSHUser, password)
}

func (s Server) sshPort() portState {
	conn, err := net.DialTimeout(tcp, net.JoinHostPort(s.Network.PublicIP, strconv.Itoa(sshPort)), time.Second)
	if err != nil {
		return portClosed
	}

	defer conn.Close()

	return portOpen
}

func (s Server) waitSSHPort(state portState) error {
	s.log("wait for ssh port to be %s", state)

	end := time.Now().Add(10 * time.Minute)

	for s.sshPort() != state {
		time.Sleep(time.Second)

		if end.Before(time.Now()) {
			return fmt.Errorf("timed out waiting for ssh port to close")
		}
	}

	return nil
}
