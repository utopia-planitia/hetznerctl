package hetznerctl

import (
	"bytes"
	"embed"
	"encoding/base64"
	"fmt"
	"io"
	"path/filepath"
	"text/template"

	"github.com/Masterminds/sprig/v3"
)

//go:embed assets
var assets embed.FS //nolint:gochecknoglobals

func loadAsset(path string) ([]byte, error) {
	f, err := assets.Open(filepath.Join("assets", path))
	if err != nil {
		return []byte{}, fmt.Errorf("read asset %v: %v", path, err)
	}
	defer f.Close()

	return io.ReadAll(f)
}

func renderAsset(path string, data interface{}) ([]byte, error) {
	tpl, err := loadAsset(path)
	if err != nil {
		return []byte{}, err
	}

	return render(path, string(tpl), data)
}

func b64enc(in string) string {
	return base64.StdEncoding.EncodeToString([]byte(in))
}

func render(name, tpl string, data interface{}) ([]byte, error) {
	funcs := template.FuncMap{
		"b64encode": b64enc,
		"b64enc":    b64enc,
		"iterate": func(count int) []int {
			var i int
			var items []int
			for i = 0; i < count; i++ {
				items = append(items, i)
			}
			return items
		},
	}

	tmpl, err := template.New(name).Funcs(sprig.TxtFuncMap()).Funcs(funcs).Parse(tpl)
	if err != nil {
		return []byte{}, fmt.Errorf("parse template: %v", err)
	}

	out := &bytes.Buffer{}

	err = tmpl.Execute(out, data)
	if err != nil {
		return []byte{}, fmt.Errorf("render template: %v", err)
	}

	return out.Bytes(), nil
}
