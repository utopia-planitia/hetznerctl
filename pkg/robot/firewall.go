package robot

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"

	"github.com/olekukonko/tablewriter"
)

const allInterfaces = "0.0.0.0"

func addRule(values *url.Values, offset int, r FirewallRule) {
	values.Add(fmt.Sprintf("rules[input][%d][ip_version]", offset), r.IPVersion)
	values.Add(fmt.Sprintf("rules[input][%d][name]", offset), r.Name)
	values.Add(fmt.Sprintf("rules[input][%d][dst_ip]", offset), r.DestinationIP)
	values.Add(fmt.Sprintf("rules[input][%d][src_ip]", offset), r.SourceIP)
	values.Add(fmt.Sprintf("rules[input][%d][dst_port]", offset), r.DestinationPort)
	values.Add(fmt.Sprintf("rules[input][%d][src_port]", offset), r.SourcePort)
	values.Add(fmt.Sprintf("rules[input][%d][protocol]", offset), r.Protocol)
	values.Add(fmt.Sprintf("rules[input][%d][tcp_flags]", offset), r.TCPFlags)
	values.Add(fmt.Sprintf("rules[input][%d][action]", offset), r.Action)
}

func addRules(values *url.Values, rr []FirewallRule) {
	for i, r := range rr {
		addRule(values, i, r)
	}
}

// EnableFirewall enables Hetzners IPv4 firewall.
func (s Server) EnableFirewall() error {
	rules := defineFirewallRules()
	values := &url.Values{}
	values.Add("status", "active")
	values.Add("whitelist_hos", "true")
	addRules(values, rules)

	resp, err := s.do(http.MethodPost, fmt.Sprintf("%s/firewall/%s", robotAPI, s.IP), *values)
	if err != nil {
		return fmt.Errorf("enabling firewall for server %v: %v", s.IP, err)
	}
	defer resp.Body.Close()

	return nil
}

func defineFirewallRules() []FirewallRule {
	return []FirewallRule{
		{
			Name:      "ping",
			IPVersion: "ipv4",
			Protocol:  "icmp",
			Action:    "accept",
		},
		{
			Name:            "established tcp",
			IPVersion:       "ipv4",
			Protocol:        "tcp",
			Action:          "accept",
			DestinationPort: "32768-65535",
			TCPFlags:        "ack",
		},
		{
			Name:            "established udp",
			IPVersion:       "ipv4",
			Protocol:        "udp",
			Action:          "accept",
			DestinationPort: "32768-65535",
		},
		{
			Name:            "ssh - ssh gitlab",
			IPVersion:       "ipv4",
			Protocol:        "tcp",
			Action:          "accept",
			DestinationPort: "22,2222",
		},
		{
			Name:            "http - https",
			IPVersion:       "ipv4",
			Protocol:        "tcp",
			Action:          "accept",
			DestinationPort: "80,443",
		},
		{
			Name:            "apiserver - etcd",
			IPVersion:       "ipv4",
			Protocol:        "tcp",
			Action:          "accept",
			DestinationPort: "6443,2380,2379",
		},
		{
			Name:            "service ports",
			IPVersion:       "ipv4",
			Action:          "accept",
			DestinationPort: "30000-32767",
		},
		{
			Name:      "class A",
			IPVersion: "ipv4",
			Action:    "accept",
			SourceIP:  "10.0.0.0/8",
		},
		{
			Name:      "class B",
			IPVersion: "ipv4",
			Action:    "accept",
			SourceIP:  "172.16.0.0/12",
		},
		{
			Name:      "class C",
			IPVersion: "ipv4",
			Action:    "accept",
			SourceIP:  "192.168.0.0/16",
		},
	}
}

// DisableFirewall disables Hetzners IPv4 firewall.
func (s Server) DisableFirewall() error {
	values := &url.Values{}
	values.Add("status", "disabled")
	values.Add("whitelist_hos", "true")

	resp, err := s.do(http.MethodPost, fmt.Sprintf("%s/firewall/%s", robotAPI, s.IP), *values)
	if err != nil {
		return fmt.Errorf("disabling firewall for server %v: %v", s.IP, err)
	}
	defer resp.Body.Close()

	return nil
}

func (fw Firewall) String() string {
	status := ""

	switch fw.Status {
	case "active":
		status = "firewall is enabled"
	case "disabled":
		status = "firewall is disabled"
	case "in process":
		if len(fw.Rules.Input) > 0 {
			status = "firewall is going to be activated"
		} else {
			status = "firewall is going to be disabled"
		}
	}

	b := bytes.Buffer{}

	_, err := b.WriteString(fmt.Sprintf("%s\n", status))
	if err != nil {
		panic(err) // writing to an in-memory buffer should never fail
	}

	if len(fw.Rules.Input) == 0 {
		return b.String()
	}

	table := tablewriter.NewWriter(&b)
	table.SetHeader([]string{"#", "Name", "Source", "Destination", "Protocol", "TCP Flags"})

	for i, r := range fw.Rules.Input {
		from := fmt.Sprintf("%s:%s", defaultTo(r.SourceIP, allInterfaces), defaultTo(r.SourcePort, "*"))
		to := fmt.Sprintf("%s:%s", defaultTo(r.DestinationIP, allInterfaces), defaultTo(r.DestinationPort, "*"))
		table.Append([]string{
			strconv.Itoa(i),
			r.Name,
			from,
			to,
			r.Protocol,
			r.TCPFlags,
		})
	}

	table.Render()

	return b.String()
}

func defaultTo(in, fallback string) string {
	if in != "" {
		return in
	}

	return fallback
}

// FirewallStatus prints the status of hetzners firewall.
func (s Server) FirewallStatus() (string, error) {
	fw, err := s.firewall()
	if err != nil {
		return "", fmt.Errorf("%v fetching firewall: %v", fw.ServerIP, err)
	}

	return fw.String(), nil
}

// FirewallResponse is returned by calling hetzners API.
type FirewallWrapper struct {
	Firewall Firewall
}

// Firewall combines firewall details and firewall rules.
type Firewall struct {
	ServerIP         string `json:"server_ip"`
	ServerNumber     int    `json:"server_number"`
	Status           string
	FilterIPv6       bool `json:"filter_ipv6"`
	WhiteListHetzner bool `json:"whitelist_hos"`
	Port             string
	Rules            struct {
		Input  []FirewallRule `json:"input"`
		Output []FirewallRule `json:"output"`
	}
}

// FirewallRule represents a single firewall rule.
type FirewallRule struct {
	IPVersion       string `json:"ip_version"`
	Name            string
	DestinationIP   string `json:"dst_ip"`
	SourceIP        string `json:"src_ip"`
	DestinationPort string `json:"dst_port"`
	SourcePort      string `json:"src_port"`
	Protocol        string
	TCPFlags        string `json:"tcp_flags"`
	Action          string
}

func (r FirewallRule) String() string {
	from := fmt.Sprintf("%s:%s", defaultTo(r.SourceIP, allInterfaces), defaultTo(r.SourcePort, "*"))
	to := fmt.Sprintf("%s:%s", defaultTo(r.DestinationIP, allInterfaces), defaultTo(r.DestinationPort, "*"))

	return fmt.Sprintf(
		"%25s [%s] %18s -> %-19s %-4s [%s]",
		r.Name,
		r.IPVersion,
		from,
		to,
		r.Protocol,
		r.TCPFlags)
}

func (s Server) firewall() (Firewall, error) {
	resp, err := s.do(http.MethodGet, fmt.Sprintf("%s/firewall/%s", robotAPI, s.IP), url.Values{})
	if err != nil {
		return Firewall{}, fmt.Errorf("fetching firewall status for server %v: %v", s.IP, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return Firewall{}, fmt.Errorf("firewall request returned status code %v, expected %v", resp.StatusCode, http.StatusOK)
	}

	return decodeFirewall(resp.Body)
}

func decodeFirewall(r io.Reader) (Firewall, error) {
	body, _ := io.ReadAll(r)
	d := json.NewDecoder(bytes.NewReader(body))
	d.DisallowUnknownFields()

	cfg := FirewallWrapper{}
	err := d.Decode(&cfg)

	if err != nil {
		d := json.NewDecoder(bytes.NewReader(body))
		d.DisallowUnknownFields()

		cfg := FirewallWrapper{}

		err2 := d.Decode(&cfg)
		if err2 != nil {
			return Firewall{}, fmt.Errorf("decoding firewall response from robot api: %v & %v", err, err2)
		}

		return cfg.Firewall, nil
	}

	return cfg.Firewall, nil
}
