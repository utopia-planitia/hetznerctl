package robot

import (
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

const robotAPI = "https://robot-ws.your-server.de"

// Server represents a server in hetzners API.
type Server struct {
	Username string
	Password string
	IP       string
}

func (s Server) log(f string, a ...interface{}) {
	log.Printf("%s: %s", s.IP, fmt.Sprintf(f, a...))
}

func (s Server) do(method, path string, params url.Values) (*http.Response, error) {
	req, err := http.NewRequest(method, path, strings.NewReader(params.Encode()))
	if err != nil {
		return &http.Response{}, err
	}

	req.SetBasicAuth(s.Username, s.Password)
	req.Header.Add("content-type", "application/x-www-form-urlencoded")

	client := httpClient()

	reties := 10

	for {
		resp, err := client.Do(req)

		if isRecoverable(resp, err) {
			reties--
			if reties == 0 {
				return &http.Response{}, fmt.Errorf("http call failed 10 times, aborting")
			}

			delay := time.Minute

			s.log("http call (%s %s) failed, waiting %s before retry", method, path, delay)
			time.Sleep(delay)

			continue
		}

		if err != nil {
			return &http.Response{}, err
		}

		if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusAccepted {
			defer resp.Body.Close()
			time.AfterFunc(5*time.Second, func() { resp.Body.Close() })

			body, _ := io.ReadAll(resp.Body)
			os.Stdout.Write(body)
			os.Stdout.WriteString("\n")

			return &http.Response{}, fmt.Errorf("robot api returned status %v, expected %v", resp.StatusCode, http.StatusOK)
		}

		return resp, nil
	}
}

func isRecoverable(resp *http.Response, err error) bool {
	nerr, ok := err.(net.Error)
	if ok && nerr.Timeout() {
		return true
	}

	if resp == nil && strings.Contains(err.Error(), "ContentLength=7 with Body length 0") {
		return true
	}

	if resp != nil && resp.StatusCode == http.StatusServiceUnavailable {
		return true
	}

	if resp != nil && resp.StatusCode == http.StatusInternalServerError {
		return true
	}

	if resp == nil {
		log.Printf("not retrying: resp=%v err=%v", resp, err)
		return false
	}

	if !isExpected(resp.StatusCode) {
		log.Printf("not retrying: status=%s status-code=%d resp=%v", resp.Status, resp.StatusCode, resp)
	}

	return false
}

func isExpected(statusCode int) bool {
	if statusCode == http.StatusOK {
		return true
	}

	if statusCode == http.StatusAccepted {
		return true
	}

	return false
}

// https://medium.com/@simonfrey/1360871cb72b
func httpClient() http.Client {
	return http.Client{
		Transport: &http.Transport{
			DialContext: (&net.Dialer{
				Timeout:   10 * time.Second,
				KeepAlive: 10 * time.Second,
			}).DialContext,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 10 * time.Second,
			ResponseHeaderTimeout: 10 * time.Second,
		},
		Timeout: 30 * time.Second,
	}
}
