package robot

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"
)

// EnableRescueMode enables rescue mode of a node.
func (s Server) EnableRescueMode(rescueOS, rescueArch string) (RescueMode, error) {
	values := url.Values{}

	values.Add("os", "linux")
	if rescueOS != "" {
		values.Set("os", rescueOS)
	}

	values.Add("arch", "64")
	if rescueArch != "" {
		values.Set("arch", rescueArch)
	}

	resp, err := s.do(http.MethodPost, fmt.Sprintf("%s/boot/%s/rescue", robotAPI, s.IP), values)
	if err != nil {
		return RescueMode{}, fmt.Errorf("activating rescue mode for server %v: %v", s.IP, err)
	}
	defer resp.Body.Close()

	return decodeRescueMode(resp.Body)
}

// DisableRescueMode disables rescue mode for a node.
func (s Server) DisableRescueMode() error {
	resp, err := s.do(http.MethodDelete, fmt.Sprintf("%s/boot/%s/rescue", robotAPI, s.IP), url.Values{})
	if err != nil {
		return fmt.Errorf("disable rescue mode for server %v: %v", s.IP, err)
	}
	defer resp.Body.Close()

	return nil
}

// RescueModeStatus returns the activation status of the rescue mode.
func (s Server) RescueModeStatus() (bool, error) {
	cfg, err := s.rescueMode()
	if err != nil {
		return false, fmt.Errorf("fetching rescue mode for node %v: %v", cfg.ServerIP, err)
	}

	return cfg.Active, nil
}

// RescueModeOptions returns the rescue mode options.
func (s Server) RescueModeOptions() (RescueModeOptions, error) {
	cfg, err := s.rescueModeOptions()
	if err != nil {
		return RescueModeOptions{}, fmt.Errorf("fetching rescue mode options for node %v: %v", cfg.ServerIP, err)
	}

	return cfg, nil
}

// RescueModePassword resturns the password and status of the rescue mode.
func (s Server) RescueModePassword() (string, bool, error) {
	cfg, err := s.rescueMode()
	if err != nil {
		return "", false, fmt.Errorf("fetching rescue mode status: %v", err)
	}

	if !cfg.Active {
		return "", false, nil
	}

	return cfg.Password, true, nil
}

// RescueModeResponse is returned by calling out to hetzners API.
type RescueModeResponse struct {
	RescueMode RescueMode `json:"rescue"`
}

// RescueMode are the rescue mode details.
type RescueMode struct {
	ServerIP     string `json:"server_ip"`
	ServerNumber int    `json:"server_number"`
	Active       bool
	Password     string
}

// RescueModeResponse is returned by calling out to hetzners API.
type RescueModeOptionsResponse struct {
	RescueMode RescueModeOptions `json:"rescue"`
}

// RescueModeOptions are the rescue mode options.
type RescueModeOptions struct {
	ServerIP     string `json:"server_ip"`
	ServerNumber int    `json:"server_number"`
	Active       bool
	Password     string
	Os           []string
	Arch         []int
}

func (s Server) rescueMode() (RescueMode, error) {
	resp, err := s.do(http.MethodGet, fmt.Sprintf("%s/boot/%s/rescue", robotAPI, s.IP), url.Values{})
	if err != nil {
		return RescueMode{}, fmt.Errorf("fetching rescue mode for server %v: %v", s.IP, err)
	}
	defer resp.Body.Close()

	time.AfterFunc(5*time.Second, func() { resp.Body.Close() })

	return decodeRescueMode(resp.Body)
}

func decodeRescueMode(r io.Reader) (RescueMode, error) {
	reader := io.LimitReader(r, 4096)
	bytes, err := io.ReadAll(reader)
	if err != nil {
		return RescueMode{}, fmt.Errorf("reading rescue response from robot api: %v", err)
	}

	cfg := RescueModeResponse{}
	err = json.Unmarshal(bytes, &cfg)
	if err != nil {
		return RescueMode{}, fmt.Errorf("decoding rescue response from robot api: %v", err)
	}

	return cfg.RescueMode, nil
}

func (s Server) rescueModeOptions() (RescueModeOptions, error) {
	resp, err := s.do(http.MethodGet, fmt.Sprintf("%s/boot/%s/rescue", robotAPI, s.IP), url.Values{})
	if err != nil {
		return RescueModeOptions{}, fmt.Errorf("fetching rescue mode for server %v: %v", s.IP, err)
	}
	defer resp.Body.Close()

	time.AfterFunc(5*time.Second, func() { resp.Body.Close() })

	return decodeRescueModeOptions(resp.Body)
}

func decodeRescueModeOptions(r io.Reader) (RescueModeOptions, error) {
	reader := io.LimitReader(r, 4096)
	bytes, err := io.ReadAll(reader)
	if err != nil {
		return RescueModeOptions{}, fmt.Errorf("reading rescue mode options response from robot api: %v", err)
	}

	cfg := RescueModeOptionsResponse{}

	err = json.Unmarshal(bytes, &cfg)
	if err != nil {
		return RescueModeOptions{}, fmt.Errorf("decoding rescue mode options response from robot api: %v", err)
	}

	return cfg.RescueMode, nil
}
