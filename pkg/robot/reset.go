package robot

import (
	"fmt"
	"net/http"
	"net/url"
)

// Reboot defines who to reboot a server.
type Reboot string

const (
	// RebootHardware signals the hardware to restart.
	RebootHardware = Reboot("hw")
	// RebootManually tells a technician to reboot the server.
	RebootManually = Reboot("man")
)

// Reboot sends a reboot command to hetzners API.
func (s Server) Reboot(rst Reboot) error {
	values := url.Values{}
	values.Add("type", string(rst))

	resp, err := s.do(http.MethodPost, fmt.Sprintf("%s/reset/%s", robotAPI, s.IP), values)
	if err != nil {
		return fmt.Errorf("resetting server %v: %v", s.IP, err)
	}
	defer resp.Body.Close()

	return nil
}
