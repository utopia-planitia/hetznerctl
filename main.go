package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"

	"gitlab.com/utopia-planitia/hetznerctl/pkg/hetznerctl"

	cli "github.com/urfave/cli/v2"
)

func main() {
	cfg, err := loadConfig()
	if err != nil {
		log.Fatal(err)
	}

	nodeSelection := ""

	app := &cli.App{
		Name:                 "hetznerctl",
		Usage:                "run kubenetes on bare metal hetzner nodes",
		EnableBashCompletion: true,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "nodes",
				Aliases:     []string{"limit", "node"},
				Value:       "*",
				Usage:       "select nodes to work on",
				Destination: &nodeSelection,
			},
		},
		Commands: []*cli.Command{
			configurationCommands(cfg),
			environmentCommands(cfg, &nodeSelection),
			rebootCommands(cfg, &nodeSelection),
			restartCommands(cfg, &nodeSelection),
			operatingSystemCommands(cfg, &nodeSelection),
			ignitionCommands(cfg, &nodeSelection),
			firewallCommands(cfg, &nodeSelection),
			rescueModeCommands(cfg, &nodeSelection),
			kubeAdmCommands(cfg, &nodeSelection),
			statusCommands(cfg, &nodeSelection),
			maintenanceCommands(cfg, &nodeSelection),
			applyCommand(cfg),
			servicesCommand(cfg),
			e2eCommand(cfg),
			testsCommand(cfg),
			execCommand(cfg),
			foreachCommand(cfg, &nodeSelection),
			dependenciesCommand(),
			autocompletionsCommand(),
			versionCommands(),
		},
	}

	bufferLength := 2 // signal.Notify requires a buffered channel
	stop := make(chan os.Signal, bufferLength)

	go runApp(app, stop)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)
	<-stop
}

func loadConfig() (*hetznerctl.Config, error) {
	cfgFile := hetznerctl.ConfigPath()

	f, err := os.Open(cfgFile)
	if errors.Is(err, os.ErrNotExist) {
		if !contains(os.Args, "--generate-bash-completion") {
			// do not print errors into autocompletion
			log.Printf("config file %s missing", cfgFile)
		}

		return &hetznerctl.Config{}, nil
	}

	if err != nil {
		return nil, fmt.Errorf("open config: %v", err)
	}
	defer f.Close()

	fi, err := f.Stat()
	if err != nil {
		return nil, fmt.Errorf("stat config: %v", err)
	}

	if fi.Size() == 0 {
		return &hetznerctl.Config{}, nil
	}

	cfg, err := hetznerctl.LoadConfig(f)
	if err != nil {
		return nil, fmt.Errorf("parse config: %v", err)
	}

	err = cfg.SanityCheck()
	if err != nil {
		return nil, fmt.Errorf("config error: %v", err)
	}

	return &cfg, nil
}

func contains(haystack []string, needle string) bool {
	for _, hay := range haystack {
		if hay == needle {
			return true
		}
	}

	return false
}

func runApp(app *cli.App, stop chan os.Signal) {
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
	stop <- os.Interrupt
}

func configurationCommands(cfg *hetznerctl.Config) *cli.Command {
	return &cli.Command{
		Name:    "configuration",
		Aliases: []string{"cfg"},
		Usage:   "define cluster settings",
		Subcommands: []*cli.Command{
			{
				Name:    "template",
				Usage:   "print the default configuration template",
				Aliases: []string{"tpl"},
				Action: func(c *cli.Context) error {
					err := hetznerctl.PrintConfigTemplate(os.Stdout)
					if err != nil {
						return fmt.Errorf("print config template: %v", err)
					}
					return nil
				},
			},
			{
				Name:    "print",
				Usage:   "print currently used configuration",
				Aliases: []string{"show"},
				Action: func(c *cli.Context) error {
					err := hetznerctl.PrintConfig(os.Stdout, *cfg)
					if err != nil {
						return fmt.Errorf("print config: %v", err)
					}
					return nil
				},
			},
		},
	}
}

func environmentCommands(cfg *hetznerctl.Config, nodeSelection *string) *cli.Command {
	return &cli.Command{
		Name:    "environment",
		Aliases: []string{"env"},
		Usage:   "setup local environment to access the cluster",
		Subcommands: []*cli.Command{
			{
				Name:      "ssh-keyscan",
				Usage:     "print command to rescan ssh fingerprints",
				UsageText: "example: . <(hetznerctl env ssh-keyscan)",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.SSHKeyscan(os.Stdout)
				},
			},
			{
				Name:      "ssh-config",
				Usage:     "print ssh configuration",
				UsageText: "example: hetznerctl env ssh-config >> ~/.ssh/config",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:    "disable-hostkey-checking",
						Aliases: []string{"insecure"},
					},
				},
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					disableHostkeyChecking := c.Bool("disable-hostkey-checking")

					return nodes.SSHConfig(os.Stdout, disableHostkeyChecking)
				},
			},
			{
				Name:      "etc-hosts",
				Usage:     "print etc hosts entries",
				UsageText: "example: hetznerctl env etc-hosts >> /etc/hosts",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.EtcHosts(os.Stdout)
				},
			},
			{
				Name:        "kubectl",
				Aliases:     []string{"kubeconfig"},
				Usage:       "print kubectl configuration",
				UsageText:   "example: hetznerctl env kubectl 1> cluster-kube-config.yaml",
				Description: "Hint to merge multiple config files: KUBECONFIG=file1:file2:file3 kubectl config view --merge --flatten",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}

					return node.KubeConfig(os.Stdout, *cfg)
				},
			},
			{
				Name:      "user-kubectl",
				Aliases:   []string{"user"},
				Usage:     "print user kubectl configuration",
				UsageText: "example: hetznerctl env user --user username 1> username-kube-config.yaml",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "user",
						Aliases:  []string{"u"},
						Required: true,
					},
				},
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}

					return node.UserKubeConfig(os.Stdout, *cfg, c.String("user"))
				},
				BashComplete: func(c *cli.Context) {
					if c.NArg() > 0 {
						return
					}

					for _, user := range cfg.Users {
						fmt.Println(user.Name)
					}
				},
			},
		},
	}
}

func rebootCommands(cfg *hetznerctl.Config, nodeSelection *string) *cli.Command {
	dangerousFeatures := false
	if os.Getenv("ENABLE_DANGEROUS_FEATURES") == cfg.Cluster.Name {
		dangerousFeatures = true
	}

	disabledError := fmt.Errorf("disabled until ENABLE_DANGEROUS_FEATURES is set to '%s'", cfg.Cluster.Name)

	return &cli.Command{
		Name:  "reboot",
		Usage: "reboot nodes",
		Subcommands: []*cli.Command{
			{
				Name:    "hardware",
				Aliases: []string{"hw", "hard"},
				Usage:   "trigger a hardware restart",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.Reboot(hetznerctl.RebootHardware)
				},
			},
			{
				Name:  "hardware-no-sync",
				Usage: "trigger a hardware restart without disk sync",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.Reboot(hetznerctl.RebootHardwareUngraceful)
				},
			},
			{
				Name:    "manually",
				Aliases: []string{"man"},
				Usage:   "send a technician to restart node manually",
				Action: func(c *cli.Context) error {
					if !dangerousFeatures {
						return disabledError
					}

					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.Reboot(hetznerctl.RebootManually)
				},
			},
			{
				Name:    "kured",
				Aliases: []string{"k"},
				Usage:   "drain and reboot one node at a time (by placing a reboot sentinel file, which kured picks up at a set schedule)",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.Reboot(hetznerctl.RebootKured)
				},
			},
			{
				Name:    "kured-real-time",
				Aliases: []string{"kured-rt", "k-rt"},
				Usage:   "drain and reboot one node at a time now",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.Reboot(hetznerctl.RebootKuredRealTime)
				},
			},
			{
				Name:    "systemctl",
				Aliases: []string{"sys", "system", "os"},
				Usage:   "sync disk and reboot",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.Reboot(hetznerctl.RebootSystem)
				},
			},
		},
	}
}

func statusCommands(cfg *hetznerctl.Config, nodeSelection *string) *cli.Command {
	return &cli.Command{
		Name:  "status",
		Usage: "show the status components",
		Subcommands: []*cli.Command{
			{
				Name:  "kured",
				Usage: "show if nodes want to reboot",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					needsReboot, err := nodes.KuredStatus(hetznerctl.KuredRebootSentinel)

					errMsg := ""
					if err != nil {
						errMsg = err.Error()
					}

					exitCode := 0
					if needsReboot || err != nil {
						exitCode = 1
					}

					return cli.Exit(errMsg, exitCode)
				},
			},
			{
				Name:    "kured-real-time",
				Aliases: []string{"kured-rt"},
				Usage:   "show if nodes want to reboot now",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					needsReboot, err := nodes.KuredStatus(hetznerctl.KuredRebootSentinelRealTime)

					errMsg := ""
					if err != nil {
						errMsg = err.Error()
					}

					exitCode := 0
					if needsReboot || err != nil {
						exitCode = 1
					}

					return cli.Exit(errMsg, exitCode)
				},
			},
			{
				Name:  "uptime",
				Usage: "show the uptime of nodes",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.UptimeStatus()
				},
			},
			{
				Name:  "nodes",
				Usage: "list kubernetes nodes",
				Action: func(c *cli.Context) error {
					primaryMaster, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return primaryMaster.NodesStatus()
				},
			},
			{
				Name:  "k8s",
				Usage: "show the status of kubernetes (control plane, nodes, pods, ...)",
				Action: func(c *cli.Context) error {
					primaryMaster, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return primaryMaster.KubernetesStatus()
				},
			},
		},
	}
}

func restartCommands(cfg *hetznerctl.Config, nodeSelection *string) *cli.Command {
	return &cli.Command{
		Name:  "restart",
		Usage: "restart services on running nodes",
		Subcommands: []*cli.Command{
			{
				Name:  "all",
				Usage: "restart containerd and kubelet",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)

					err := nodes.RestartService("kubelet")
					if err != nil {
						return err
					}

					return nodes.RestartService("containerd")
				},
			},
			{
				Name:  "containerd",
				Usage: "restart containerd",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.RestartService("containerd")
				},
			},
			{
				Name:  "kubelet",
				Usage: "restart kubelet",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.RestartService("kubelet")
				},
			},
		},
	}
}

func operatingSystemCommands(cfg *hetznerctl.Config, nodeSelection *string) *cli.Command {
	dangerousFeatures := false
	if os.Getenv("ENABLE_DANGEROUS_FEATURES") == cfg.Cluster.Name {
		dangerousFeatures = true
	}

	disabledError := fmt.Errorf("disabled until ENABLE_DANGEROUS_FEATURES is set to '%s'", cfg.Cluster.Name)

	return &cli.Command{
		Name:    "operating-system",
		Aliases: []string{"os"},
		Usage:   "install operating system on machines",
		Subcommands: []*cli.Command{
			{
				Name:  "all",
				Usage: "sets up the operating system from bare metal",
				Action: func(c *cli.Context) error {
					if !dangerousFeatures {
						return disabledError
					}
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.SetupFlatcar(*cfg)
				},
			},
			{
				Name:  "firewall",
				Usage: "disable firewall",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.DisableFirewall()
				},
			},
			{
				Name:  "rescue",
				Usage: "reboot to rescue mode & install ssh keys",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.BootRescueMode(*cfg)
				},
			},
			{
				Name:  "os",
				Usage: "install operating system",
				Action: func(c *cli.Context) error {
					if !dangerousFeatures {
						return disabledError
					}
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.InstallFlatcar(*cfg)
				},
			},
			{
				Name:  "data",
				Usage: "reset ceph storage disk",
				Action: func(c *cli.Context) error {
					if !dangerousFeatures {
						return disabledError
					}
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.DeleteDataDisk(*cfg)
				},
			},
		},
	}
}

func ignitionCommands(cfg *hetznerctl.Config, nodeSelection *string) *cli.Command {
	return &cli.Command{
		Name:    "ignition",
		Aliases: []string{"ign"},
		Usage:   "update node configuration",
		Subcommands: []*cli.Command{
			{
				Name:    "print-ignition-config",
				Aliases: []string{"ign"},
				Usage:   "print ignition config",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.RenderIgnitionConfig(*cfg, os.Stdout)
				},
			},
			{
				Name:    "print-container-linux-config",
				Aliases: []string{"cl"},
				Usage:   "print container linux config",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.RenderContainerLinuxConfig(*cfg, os.Stdout)
				},
			},
			{
				Name:    "print-container-linux-template",
				Aliases: []string{"tpl"},
				Usage:   "print container linux config template",
				Action: func(c *cli.Context) error {
					return hetznerctl.RenderContainerLinuxConfigTemplate(os.Stdout)
				},
			},
			{
				Name:  "update",
				Usage: "upload ignition config and notify kured",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.ConfigureFlatcar(*cfg)
				},
			},
		},
	}
}

func firewallCommands(cfg *hetznerctl.Config, nodeSelection *string) *cli.Command {
	return &cli.Command{
		Name:    "firewall",
		Aliases: []string{"fw"},
		Usage:   "configure firewall",
		Subcommands: []*cli.Command{
			{
				Name:  "status",
				Usage: "show firewall status",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.FirewallStatus()
				},
			},
			{
				Name:  "enable",
				Usage: "enable firewall",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.EnableFirewall()
				},
			},
			{
				Name:  "disable",
				Usage: "disable firewall",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.DisableFirewall()
				},
			},
		},
	}
}

func rescueModeCommands(cfg *hetznerctl.Config, nodeSelection *string) *cli.Command {
	return &cli.Command{
		Name:    "rescuemode",
		Aliases: []string{"rescue"},
		Usage:   "manage rescue mode for hetzner bare metal machines",
		Subcommands: []*cli.Command{
			{
				Name:  "status",
				Usage: "show rescue mode status",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.RescueModeStatus()
				},
			},
			{
				Name:  "options",
				Usage: "show rescue mode options",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.RescueModeOptions()
				},
			},
			{
				Name:  "password",
				Usage: "show rescue mode password",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.RescueModePassword()
				},
			},
			{
				Name:  "enable",
				Usage: "enable rescue mode",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.EnableRescueMode(*cfg)
				},
			},
			{
				Name:  "disable",
				Usage: "disable rescue mode",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.DisableRescueMode()
				},
			},
		},
	}
}

func kubeAdmCommands(cfg *hetznerctl.Config, nodeSelection *string) *cli.Command {
	dangerousFeatures := false
	if os.Getenv("ENABLE_DANGEROUS_FEATURES") == cfg.Cluster.Name {
		dangerousFeatures = true
	}

	disabledError := fmt.Errorf("disabled until ENABLE_DANGEROUS_FEATURES is set to '%s'", cfg.Cluster.Name)

	return &cli.Command{
		Name:    "kubernetes",
		Aliases: []string{"k8s", "kubeadm"},
		Usage:   "install kubernetes",
		Subcommands: []*cli.Command{
			{
				Name:  "all",
				Usage: "install all components",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:    "verbose",
						Aliases: []string{"v"},
						Usage:   "show output",
					},
					&cli.IntFlag{
						Name:  "helmfileSyncTries",
						Usage: "number of (re)tries for helmfile sync",
						Value: 5,
					},
				},
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					verbose := c.Bool("verbose")
					helmfileSyncTries := c.Int("helmfileSyncTries")
					return nodes.SetupKubeadm(*cfg, verbose, helmfileSyncTries)
				},
			},
			{
				Name:    "initialize",
				Aliases: []string{"init"},
				Usage:   "initialize first master node",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return node.InitCluster(*cfg)
				},
			},
			{
				Name:  "users",
				Usage: "create users roles",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return node.CreateUsers(*cfg)
				},
			},
			{
				Name:  "node-annotations",
				Usage: "annotate nodes",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return node.AnnotateNodes(*cfg)
				},
			},
			{
				Name:  "patch-apiservice",
				Usage: "add sessionAffinity=ClientIP to internal kubernetes service",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return node.PatchAPIService(*cfg)
				},
			},
			{
				Name:    "masters",
				Aliases: []string{"ha"},
				Usage:   "join master nodes",
				Action: func(c *cli.Context) error {
					primaryMaster, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					nodes := cfg.Nodes.SelectBackupMasters().Filter(*nodeSelection)
					return nodes.JoinMasters(primaryMaster, *cfg)
				},
			},
			{
				Name:  "workers",
				Usage: "join worker nodes",
				Action: func(c *cli.Context) error {
					primaryMaster, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					nodes := cfg.Nodes.SelectWorkers().Filter(*nodeSelection)
					return nodes.JoinWorker(primaryMaster, *cfg)
				},
			},
			{
				Name:  "update",
				Usage: "update kubernetes version or update cluster configuration",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:  "skipAwaitReady",
						Value: false,
						Usage: "do not wait for status.sh",
					},
					&cli.BoolFlag{
						Name:  "skipDrain",
						Value: false,
						Usage: "do not drain nodes",
					},
					&cli.BoolFlag{
						Name:    "verbose",
						Aliases: []string{"v"},
						Usage:   "show output",
					},
				},
				Action: func(c *cli.Context) error {
					skipAwaitReady := c.Bool("skipAwaitReady")
					skipDrain := c.Bool("skipDrain")
					verbose := c.Bool("verbose")

					return cfg.Nodes.KubeadmUpdate(*cfg, skipDrain, skipAwaitReady, verbose)
				},
			},
			{
				Name:  "reset",
				Usage: "remove all traces of kubernetes from the node",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:  "dataDisks",
						Value: true,
						Usage: "reset ceph data disks",
					},
				},
				Action: func(c *cli.Context) error {
					if !dangerousFeatures {
						return disabledError
					}
					nodes := cfg.Nodes.Filter(*nodeSelection)

					dataDisks := c.Bool("dataDisks")
					if dataDisks {
						err := nodes.DeleteDataDisk(*cfg)
						if err != nil {
							return err
						}
					}

					return nodes.ResetKubeadm()
				},
			},
		},
	}
}

func versionCommands() *cli.Command {
	return &cli.Command{
		Name:  "version",
		Usage: "print component versions",
		Subcommands: []*cli.Command{
			{
				Name:    "container-network-interface",
				Usage:   "print cni version",
				Aliases: []string{"cni"},
				Action: func(c *cli.Context) error {
					fmt.Println(hetznerctl.CNI_VERSION)
					return nil
				},
			},
			{
				Name:    "kubernetes",
				Usage:   "print kubernetes version",
				Aliases: []string{"k8s"},
				Action: func(c *cli.Context) error {
					fmt.Println(hetznerctl.KUBERNETES_VERSION)
					return nil
				},
			},
			{
				Name:  "kubernetes-release",
				Usage: "print kubernetes release version",
				Action: func(c *cli.Context) error {
					fmt.Println(hetznerctl.RELEASE_VERSION)
					return nil
				},
			},
			{
				Name:  "helm",
				Usage: "print helm version",
				Action: func(c *cli.Context) error {
					fmt.Println(hetznerctl.HELM_VERSION)
					return nil
				},
			},
			{
				Name:  "helmfile",
				Usage: "print helmfile version",
				Action: func(c *cli.Context) error {
					fmt.Println(hetznerctl.HELMFILE_VERSION)
					return nil
				},
			},
			{
				Name:  "kustomize",
				Usage: "print kustomize version",
				Action: func(c *cli.Context) error {
					fmt.Println(hetznerctl.KUSTOMIZE_VERSION)
					return nil
				},
			},
			{
				Name:  "sonobuoy",
				Usage: "print sonobuoy version",
				Action: func(c *cli.Context) error {
					fmt.Println(hetznerctl.SONOBUOY_VERSION)
					return nil
				},
			},
			{
				Name:  "crictl",
				Usage: "print crictl version",
				Action: func(c *cli.Context) error {
					fmt.Println(hetznerctl.CRICTL_VERSION)
					return nil
				},
			},
			{
				Name:  "velero",
				Usage: "print velero version",
				Action: func(c *cli.Context) error {
					fmt.Println(hetznerctl.VELERO_VERSION)
					return nil
				},
			},
		},
	}
}

func maintenanceCommands(cfg *hetznerctl.Config, nodeSelection *string) *cli.Command {
	return &cli.Command{
		Name:  "maintain",
		Usage: "maintain kubernetes, users, and opering system",
		Subcommands: []*cli.Command{
			{
				Name:  "all",
				Usage: "maintain all components",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:  "skipAwaitReady",
						Value: false,
						Usage: "do not wait for status.sh",
					},
					&cli.BoolFlag{
						Name:  "skipDrain",
						Value: false,
						Usage: "do not drain nodes",
					},
					&cli.BoolFlag{
						Name:    "verbose",
						Aliases: []string{"v"},
						Usage:   "show output",
					},
					&cli.IntFlag{
						Name:  "helmfileSyncTries",
						Usage: "number of (re)tries for helmfile sync",
						Value: 5,
					},
				},
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					skipAwaitReady := c.Bool("skipAwaitReady")
					skipDrain := c.Bool("skipDrain")
					verbose := c.Bool("verbose")
					helmfileSyncTries := c.Int("helmfileSyncTries")

					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}

					err = nodes.Uncordon(node)
					if err != nil {
						return err
					}

					err = node.PrepareMaintenance(*cfg)
					if err != nil {
						return fmt.Errorf("prepare maintenance: %v", err)
					}

					err = nodes.KubeadmUpdate(*cfg, skipDrain, skipAwaitReady, verbose)
					if err != nil {
						return fmt.Errorf("update kubernetes: %v", err)
					}

					err = nodes.ReconfigureCluster(*cfg, skipAwaitReady, verbose)
					if err != nil {
						return fmt.Errorf("reconfigure cluster: %v", err)
					}

					err = node.CreateUsers(*cfg)
					if err != nil {
						return err
					}

					services, err := listServices()
					if err != nil {
						return err
					}

					err = node.DeployServices(*cfg, services, false, verbose, helmfileSyncTries)
					if err != nil {
						return fmt.Errorf("update services: %v", err)
					}

					err = node.FinalizeMaintenance(*cfg)
					if err != nil {
						return fmt.Errorf("finalize maintenance: %v", err)
					}

					err = nodes.ConfigureSSHKeys(*cfg)
					if err != nil {
						return fmt.Errorf("update ssh keys: %v", err)
					}

					err = nodes.ConfigureFlatcar(*cfg)
					if err != nil {
						return fmt.Errorf("update operating system: %v", err)
					}

					return nil
				},
			},
			{
				Name:  "uncordon",
				Usage: "uncordon all nodes",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return cfg.Nodes.Uncordon(node)
				},
			},
			{
				Name:  "prepare",
				Usage: "prepare cluster maintenance",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return node.PrepareMaintenance(*cfg)
				},
			},
			{
				Name:    "kubernetes",
				Aliases: []string{"k8s"},
				Usage:   "update kubernetes control plane and nodes",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:  "skipAwaitReady",
						Value: false,
						Usage: "do not wait for status.sh",
					},
					&cli.BoolFlag{
						Name:  "skipDrain",
						Value: false,
						Usage: "do not drain nodes",
					},
					&cli.BoolFlag{
						Name:    "verbose",
						Aliases: []string{"v"},
						Usage:   "show output",
					},
				},
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					skipAwaitReady := c.Bool("skipAwaitReady")
					skipDrain := c.Bool("skipDrain")
					verbose := c.Bool("verbose")

					return nodes.KubeadmUpdate(*cfg, skipDrain, skipAwaitReady, verbose)
				},
			},
			{
				Name:    "configuration",
				Aliases: []string{"config", "cfg"},
				Usage:   "reconfigure kubernetes cluster",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:  "skipAwaitReady",
						Value: false,
						Usage: "do not wait for status.sh",
					},
					&cli.BoolFlag{
						Name:    "verbose",
						Aliases: []string{"v"},
						Usage:   "show output",
					},
				},
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					skipAwaitReady := c.Bool("skipAwaitReady")
					verbose := c.Bool("verbose")

					return nodes.ReconfigureCluster(*cfg, skipAwaitReady, verbose)
				},
			},
			{
				Name:  "users",
				Usage: "deploy user accounts",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return node.CreateUsers(*cfg)
				},
			},
			{
				Name:  "node-annotations",
				Usage: "annotate nodes",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return node.AnnotateNodes(*cfg)
				},
			},
			{
				Name:  "patch-apiservice",
				Usage: "add sessionAffinity=ClientIP to internal kubernetes service",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return node.PatchAPIService(*cfg)
				},
			},
			{
				Name:    "services",
				Aliases: []string{"svc"},
				Usage:   "update service deployments",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:    "verbose",
						Aliases: []string{"v"},
						Usage:   "show output",
					},
					&cli.IntFlag{
						Name:  "helmfileSyncTries",
						Usage: "number of (re)tries for helmfile sync",
						Value: 5,
					},
				},
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)

					node, err := nodes.PrimaryMaster()
					if err != nil {
						return err
					}

					services, err := listServices()
					if err != nil {
						return err
					}

					verbose := c.Bool("verbose")
					helmfileSyncTries := c.Int("helmfileSyncTries")

					return node.DeployServices(*cfg, services, false, verbose, helmfileSyncTries)
				},
			},
			{
				Name:  "addons",
				Usage: "deploy setup addons",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:    "verbose",
						Aliases: []string{"v"},
						Usage:   "show output",
					},
					&cli.IntFlag{
						Name:  "helmfileSyncTries",
						Usage: "number of (re)tries for helmfile sync",
						Value: 5,
					},
				},
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}

					verbose := c.Bool("verbose")
					helmfileSyncTries := c.Int("helmfileSyncTries")

					return node.SetupAddons(*cfg, verbose, false, helmfileSyncTries)
				},
			},
			{
				Name:  "finalize",
				Usage: "finalize cluster maintenance",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}
					return node.FinalizeMaintenance(*cfg)
				},
			},
			{
				Name:    "ssh-keys",
				Aliases: []string{"ssh"},
				Usage:   "update ssh authorized keys",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.ConfigureSSHKeys(*cfg)
				},
			},
			{
				Name:    "operating-system",
				Aliases: []string{"os"},
				Usage:   "update operating system",
				Action: func(c *cli.Context) error {
					nodes := cfg.Nodes.Filter(*nodeSelection)
					return nodes.ConfigureFlatcar(*cfg)
				},
			},
		},
	}
}

func applyCommand(cfg *hetznerctl.Config) *cli.Command {
	return &cli.Command{
		Name:  "apply",
		Usage: "apply manifests",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "delete",
				Aliases: []string{"d"},
				Usage:   "delete the components",
			},
			&cli.StringFlag{
				Name:    "namespace",
				Aliases: []string{"n"},
				Usage:   "select a namespace",
			},
			&cli.BoolFlag{
				Name:    "tpl",
				Aliases: []string{"t"},
				Usage:   "render input as templates",
			},
		},
		Action: func(c *cli.Context) error {
			node, err := cfg.Nodes.PrimaryMaster()
			if err != nil {
				return err
			}

			delete := c.Bool("delete")
			namespace := c.String("namespace")
			tpl := c.Bool("tpl")

			for _, file := range c.Args().Slice() {
				yaml, err := os.ReadFile(file)
				if err != nil {
					return err
				}

				if tpl {
					err = node.ApplyTpl(yaml, delete, namespace, cfg)
				} else {
					err = node.Apply(yaml, delete, namespace)
				}
				if err != nil {
					return err
				}
			}

			return nil
		},
		BashComplete: func(c *cli.Context) {
			fmt.Println("/dev/stdin")
			matches, err := filepath.Glob("*")
			if err != nil {
				log.Fatalln(err)
			}

			for _, t := range matches {
				fmt.Println(t)
			}
		},
	}
}

func e2eCommand(cfg *hetznerctl.Config) *cli.Command {
	return &cli.Command{
		Name:  "e2e",
		Usage: "run e2e for kubernetes",
		Subcommands: []*cli.Command{
			{
				Name:  "run",
				Usage: "run sonobuoy",
				Action: func(c *cli.Context) error {
					node, err := cfg.Nodes.PrimaryMaster()
					if err != nil {
						return err
					}

					return node.Sonobuoy()
				},
			},
		},
	}
}

func testsCommand(cfg *hetznerctl.Config) *cli.Command {
	return &cli.Command{
		Name:      "tests",
		Usage:     "run tests for services",
		ArgsUsage: "list of services to test",
		Aliases:   []string{"test"},
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "verbose",
				Aliases: []string{"v"},
				Usage:   "show output",
			},
			&cli.BoolFlag{
				Name:  "sync",
				Value: true,
				Usage: "sync helmfile before testing",
			},
			&cli.IntFlag{
				Name:  "helmfileSyncTries",
				Usage: "number of (re)tries for helmfile sync",
				Value: hetznerctl.HELM_RETRIES,
			},
			&cli.StringFlag{
				Name:  "filter",
				Usage: "filter passed to helm test via helmfile tests",
				Value: "",
			},
		},
		Action: func(c *cli.Context) error {
			node, err := cfg.Nodes.PrimaryMaster()
			if err != nil {
				return err
			}

			if c.Args().Len() == 0 {
				return cli.ShowSubcommandHelp(c)
			}

			services := c.Args().Slice()
			if c.Args().First() == "all" {
				services, err = listServices()
				if err != nil {
					return err
				}
			}

			verbose := c.Bool("verbose")
			sync := c.Bool("sync")
			helmfileSyncTries := c.Int("helmfileSyncTries")
			filter := c.String("filter")

			return node.TestServices(*cfg, services, verbose, sync, helmfileSyncTries, filter)
		},
		BashComplete: func(c *cli.Context) {
			services, err := listServices()
			if err != nil {
				log.Fatalln(err)
			}

			fmt.Println("all")
			for _, svc := range services {
				fmt.Println(svc)
			}
		},
	}
}

func execCommand(cfg *hetznerctl.Config) *cli.Command {
	return &cli.Command{
		Name:      "exec",
		Usage:     "run command on primary node",
		ArgsUsage: "command to execute",
		Action: func(c *cli.Context) error {
			node, err := cfg.Nodes.PrimaryMaster()
			if err != nil {
				return err
			}

			return node.Exec(c.Args().Slice())
		},
	}
}

func foreachCommand(cfg *hetznerctl.Config, nodeSelection *string) *cli.Command {
	return &cli.Command{
		Name:      "foreach",
		Usage:     "run command on each node",
		ArgsUsage: "command to execute",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "abort",
				Aliases: []string{"a"},
				Value:   false,
				Usage:   "abort executions after first failure",
			},
		},
		Action: func(c *cli.Context) error {
			nodes := cfg.Nodes.Filter(*nodeSelection)
			return nodes.Exec(c.Args().Slice(), c.Bool("abort"))
		},
	}
}

func servicesCommand(cfg *hetznerctl.Config) *cli.Command {
	return &cli.Command{
		Name:      "services",
		Usage:     "deploy services",
		ArgsUsage: "list of services to deploy",
		Aliases:   []string{"svc"},
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "verbose",
				Aliases: []string{"v"},
				Usage:   "show output",
			},
			&cli.BoolFlag{
				Name:    "delete",
				Aliases: []string{"d"},
				Usage:   "delete service",
			},
			&cli.IntFlag{
				Name:  "helmfileSyncTries",
				Usage: "number of (re)tries for helmfile sync",
				Value: hetznerctl.HELM_RETRIES,
			},
		},
		Action: func(c *cli.Context) error {
			node, err := cfg.Nodes.PrimaryMaster()
			if err != nil {
				return err
			}

			if c.Args().Len() == 0 {
				return cli.ShowSubcommandHelp(c)
			}

			services := c.Args().Slice()
			if c.Args().First() == "all" {
				services, err = listServices()
				if err != nil {
					return err
				}
			}

			verbose := c.Bool("verbose")
			delete := c.Bool("delete")
			helmfileSyncTries := c.Int("helmfileSyncTries")

			return node.DeployServices(*cfg, services, delete, verbose, helmfileSyncTries)
		},
		BashComplete: func(c *cli.Context) {
			services, err := listServices()
			if err != nil {
				log.Fatalln(err)
			}

			fmt.Println("all")
			for _, svc := range services {
				fmt.Println(svc)
			}
		},
	}
}

func dependenciesCommand() *cli.Command {
	return &cli.Command{
		Name:      "dependencies",
		Usage:     "generate dot graph of dependencies between services",
		UsageText: "example: hetznerctl deps all | m4 | dot -Tpng > dependencies.png",
		ArgsUsage: "list of services",
		Aliases:   []string{"deps"},
		Action: func(c *cli.Context) error {
			if c.Args().Len() == 0 {
				return cli.ShowSubcommandHelp(c)
			}

			var err error
			services := c.Args().Slice()
			if c.Args().First() == "all" {
				services, err = listServices()
				if err != nil {
					return err
				}
			}

			return hetznerctl.Dependencies(os.Stdout, services)
		},
		BashComplete: func(c *cli.Context) {
			services, err := listServices()
			if err != nil {
				log.Fatalln(err)
			}

			fmt.Println("all")
			for _, svc := range services {
				fmt.Println(svc)
			}
		},
	}
}

func listServices() ([]string, error) {
	services := []string{}

	files, err := os.ReadDir("services")
	if err != nil {
		return []string{}, err
	}

	for _, file := range files {
		keep, err := isDirOrSymlinkToDir(filepath.Join("services", file.Name()))
		if err != nil {
			return []string{}, err
		}

		if !keep {
			continue
		}

		if file.Name() == "hetznerctl" {
			continue
		}

		if strings.HasPrefix(file.Name(), "_") {
			continue
		}

		services = append(services, file.Name())
	}

	return services, nil
}

func isDirOrSymlinkToDir(path string) (bool, error) {
	fi, err := os.Stat(path)
	if err != nil {
		return false, err
	}

	// is symlink
	if fi.Mode()&os.ModeSymlink == os.ModeSymlink {
		path, err = filepath.EvalSymlinks(path)
		if err != nil {
			return false, err
		}

		fi, err = os.Stat(path)
		if err != nil {
			return false, err
		}
	}

	// is directory
	if fi.IsDir() {
		return true, nil
	}

	return false, nil
}

const bashCompletion = `
: ${PROG:=$(basename ${BASH_SOURCE})}

_cli_bash_autocomplete() {
  if [[ "${COMP_WORDS[0]}" != "source" ]]; then
    local cur opts base
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    if [[ "$cur" == "-"* ]]; then
      opts=$( ${COMP_WORDS[@]:0:$COMP_CWORD} ${cur} --generate-bash-completion )
    else
      opts=$( ${COMP_WORDS[@]:0:$COMP_CWORD} --generate-bash-completion )
    fi
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    return 0
  fi
}

complete -o bashdefault -o default -o nospace -F _cli_bash_autocomplete $PROG
unset PROG
`

const zshCompletion = `
#compdef $PROG

_cli_zsh_autocomplete() {

  local -a opts
  local cur
  cur=${words[-1]}
  if [[ "$cur" == "-"* ]]; then
    opts=("${(@f)$(_CLI_ZSH_AUTOCOMPLETE_HACK=1 ${words[@]:0:#words[@]-1} ${cur} --generate-bash-completion)}")
  else
    opts=("${(@f)$(_CLI_ZSH_AUTOCOMPLETE_HACK=1 ${words[@]:0:#words[@]-1} --generate-bash-completion)}")
  fi

  if [[ "${opts[1]}" != "" ]]; then
    _describe 'values' opts
  fi

  return
}

compdef _cli_zsh_autocomplete $PROG
`

func autocompletionsCommand() *cli.Command {
	return &cli.Command{
		Name:  "completion",
		Usage: "autocomplete bash and zsh commands",
		Subcommands: []*cli.Command{
			{
				Name:  "bash",
				Usage: "autocomplete bash. example: . <(hetznerctl completion bash)",
				Action: func(c *cli.Context) error {
					_, err := os.Stdout.WriteString(fmt.Sprintf("PROG=%s", os.Args[0]))
					if err != nil {
						return err
					}

					_, err = os.Stdout.WriteString(bashCompletion)
					return err
				},
			},
			{
				Name:  "zsh",
				Usage: "autocomplete zsh. example: . <(hetznerctl completion zsh)",
				Action: func(c *cli.Context) error {
					_, err := os.Stdout.WriteString(fmt.Sprintf("PROG=%s", os.Args[0]))
					if err != nil {
						return err
					}

					_, err = os.Stdout.WriteString(zshCompletion)
					return err
				},
			},
		},
	}
}
