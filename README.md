# Hetznerctl

## Why

This tool is created to setup a Kubernetes cluster on Hetzner bare metal nodes.

## Features

- configure clusters in a single yaml
- install flatcar
- reboot machines via kured
- update nodes
- manage kubernetes via kubeadm
- install preconfigured services

## Install

Install via the go get command

```shell
go get -u -v gitlab.com/utopia-planitia/hetznerctl
```

Compile inside docker

```shell
docker run --rm -v $PWD:/app -w /app golang:latest go build .
sudo mv ./hetznerctl /usr/local/bin
```

## Setup

Create base configuration

```shell
hetznerctl cfg tpl > cluster.yaml
```

Cloudflare is used as a DNS based load balancer. Services use subdomains of `.cluster.domain`. Nginx Ingress configures `cloudflare.subdomains.http`.`.cloudflare.zone` and Kubernetes Apiserver configures `cloudflare.subdomains.api`.`.cloudflare.zone` to add its nodes ip to DNS. A Wildcard from `*.project-domain.tld` should be used to forward to `lb.cluster-domain.tld`.

Password protection for cluster services need hashed credentials. `htpasswd -c /dev/stdout <username>` creates a salt with the required prefix `$apr1$`.

Hetzner provides an internal network between bare metal nodes. The service is called Vswitch and can be configured at <https://robot.your-server.de/vswitch/index>.

## Help

To show the help run

```shell
hetznerctl help
```
